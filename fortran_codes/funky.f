      FUNCTION FULLFUNK(IC)
************************************************************************
* The purpose of this routine is for use in the ameoba optimization.   *
* The H2O2 internal coordinates are put into the array IC and the      *
* function FULLFUNK returns the potential energy at the corresponding  * 
* geometry in hartrees.  All inputs should be in radians and atomic    *
* units (bohr for length).  The IC array is filled as:                 *
*      IC(1) =  (O3-H1) bond length.                                   *
*      IC(2) =  (O4-H2) bond length.                                   *
*      IC(3) =  (O3--O4) bond length.                                  *
*      IC(4) =  Angle between (O3-H1) bond and (O3--O4) bond.          *
*      IC(5) =  Angle between (O4-H2) bond and (O3--O4) bond.          *
*      IC(6) =  Dihedral angle of H2O2.                                *
************************************************************************
      IMPLICIT NONE
      DIMENSION IC(1:6), XX(1:4,1:3)
      DOUBLE PRECISION FULLFUNK, IC, PottyOut, XX
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DOUBLE PRECISION cmi2har
      INTEGER iflag
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* Take out the internal coordinates.
      ROH1 = IC(1)
      ROH2 = IC(2)
      ROO  = IC(3)
      AHOO1= IC(4)
      AHOO2= IC(5)
      TAU  = IC(6)
* Convert to the cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* Find the potential energy
      iflag = 0
      CALL hoohpot(XX,PottyOut,iflag)
      FULLFUNK = PottyOut * cmi2har
      RETURN
      END

* Separate ROO
      FUNCTION ROOFUNK(IC,ROO)
************************************************************************
* The purpose of this routine is for use in the ameoba optimization.   *
* The H2O2 internal coordinates are put into the array IC and the      *
* function ROOFUNK returns the potential energy at the corresponding   * 
* geometry in hartrees.  All inputs should be in radians and atomic    *
* units (bohr for length).  The ROO coordinate is put in separately so *
* that the amoeba will not optimize it.                                *
* The IC array is filled as:                                           *
*      IC(1) =  (O3-H1) bond length.                                   *
*      IC(2) =  (O4-H2) bond length.                                   *
*      IC(3) =  Angle between (O3-H1) bond and (O3--O4) bond.          *
*      IC(4) =  Angle between (O4-H2) bond and (O3--O4) bond.          *
*      IC(5) =  Dihedral angle of H2O2.                                *
************************************************************************
      IMPLICIT NONE
      DIMENSION IC(1:5), XX(1:4,1:3)
      DOUBLE PRECISION ROOFUNK, IC, PottyOut, XX
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DOUBLE PRECISION cmi2har
      INTEGER iflag
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* Take out the internal coordinates.
      ROH1 = IC(1)
      ROH2 = IC(2)
      AHOO1= IC(3)
      AHOO2= IC(4)
      TAU  = IC(5)
* Convert to the cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* Find the potential energy
      iflag = 0
      CALL hoohpot(XX,PottyOut,iflag)
      ROOFUNK = PottyOut * cmi2har
      RETURN
      END

* Holds TAU constant
      FUNCTION TAUFUNK(IC,TAU)
************************************************************************
* The purpose of this routine is for use in the ameoba optimization.   *
* The H2O2 internal coordinates are put into the array IC and the      *
* function TAUFUNK returns the potential energy at the corresponding   * 
* geometry in hartrees.  The dihedral angle is put in separately so    *
* the ameoba will not optimize it.                                     * 
*    All inputs should be in radians and atomic                        *
*    units (bohr for length).  The IC array is filled as:              *
*      IC(1) =  (O3-H1) bond length.                                   *
*      IC(2) =  (O4-H2) bond length.                                   *
*      IC(3) =  (O3--O4) bond length.                                  *
*      IC(4) =  Angle between (O3-H1) bond and (O3--O4) bond.          *
*      IC(5) =  Angle between (O4-H2) bond and (O3--O4) bond.          *
************************************************************************
      IMPLICIT NONE
      DIMENSION IC(1:5), XX(1:4,1:3)
      DOUBLE PRECISION TAUFUNK, IC, PottyOut, XX
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DOUBLE PRECISION cmi2har
      INTEGER iflag
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* Take out the internal coordinates.
      ROH1 = IC(1)
      ROH2 = IC(2)
      ROO  = IC(3)
      AHOO1= IC(4)
      AHOO2= IC(5)
* Convert to the cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* Find the potential energy
      iflag = 0
      CALL hoohpot(XX,PottyOut,iflag)
      TAUFUNK = PottyOut * cmi2har
      RETURN
      END


* For holding TAU and ROO constant.
      FUNCTION ROOTAUFUNK(IC,ROO,TAU)
************************************************************************
* The purpose of this routine is for use in the ameoba optimization.   *
* The H2O2 internal coordinates are put into the array IC and the      *
* function ROOTAUFUNK returns the potential energy at the corresponding* 
* geometry in hartrees.  The dihedral angle and the O-O bondlength is  *
* put in separately so the ameoba will not optimize it.                *
*                                                                      *
*    All inputs should be in radians and atomic                        *
*    units (bohr for length).  The IC array is filled as:              *
*      IC(1) =  (O3-H1) bond length.                                   *
*      IC(2) =  (O4-H2) bond length.                                   *
*      IC(3) =  Angle between (O3-H1) bond and (O3--O4) bond.          *
*      IC(4) =  Angle between (O4-H2) bond and (O3--O4) bond.          *
************************************************************************
      IMPLICIT NONE
      DIMENSION IC(1:4), XX(1:4,1:3)
      DOUBLE PRECISION ROOTAUFUNK, IC, PottyOut, XX
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DOUBLE PRECISION cmi2har
      INTEGER iflag
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* Take out the internal coordinates.
      ROH1 = IC(1)
      ROH2 = IC(2)
      AHOO1= IC(3)
      AHOO2= IC(4)
* Convert to the cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* Find the potential energy
      iflag = 0
      CALL hoohpot(XX,PottyOut,iflag)
      ROOTAUFUNK = PottyOut * cmi2har
      RETURN
      END
