      SUBROUTINE TANMW(Natm,mass,TANG)
************************************************************************
* The purpose of this routine is to mass-weight a cartesian            *
* displace tangent vector (s_ta in the notation of Wilson, Decius, and *
* Cross).                                                              *
* The input for the routine is:                                        *
*         -> Natm is the number of atoms in the molecule.              *
*         -> mass is an array containing each of the masses of the     *
*            atoms (a 1D array of size Natm).                          *
*         -> TANG is a double array containing the cartesian tangent   *
*            vector upon input and the mass-weighted cartesian tangent *
*            vector upon output.  It is organized as (atom#,XYZ#).     *
************************************************************************
      IMPLICIT NONE
      INTEGER Natm, i, j, flag
      DIMENSION mass(1:Natm), TANG(1:Natm,1:3)
      DOUBLE PRECISION mass, TANG, Normy
* Divide by the sqrt of the masses of the atoms to convert
* tangents from cartesian to centers of mass.
      Normy = 0.0D0
      DO i=1, Natm
         DO j=1, 3
           TANG(i,j) = TANG(i,j) / DSQRT( mass(i) )
           Normy = Normy + TANG(i,j)**2
           END DO
         END DO
* Normalize the new tangent vector.
      Normy = DSQRT( Normy )
      DO i=1, Natm
         DO j=1, 3
            TANG(i,j) = TANG(i,j) / Normy
            END DO
         END DO
      RETURN
      END
