************************************************************************
* THESE ARE THE 2D SPLINE ROUTINES.  A FIRST CALL OF splie2 IS NEEDED  *
* TO PARAMETERIZE THE SPLINE.  THE splin2 CAN BE USED AS MANY TIMES AS *
* NEEDED FOR THE INTERPOLATION.  SEE NUMERICAL RECIPES FOR PROPER      *
* USAGE.                                                               *
************************************************************************
* INTERPOLATION ROUTINE
      SUBROUTINE splin2(x1a,x2a,ya,y2a,m,n,x1,x2,y)
      INTEGER m,n
      DOUBLE PRECISION x1,x2,y,x1a(m),x2a(n),y2a(m,n),ya(m,n)
CU    USES spliner,splinter
      INTEGER j,k
      DOUBLE PRECISION y2tmp(1:m+n),ytmp(1:m+n),yytmp(1:m+n)
      do 12 j=1,m
        do 11 k=1,n
          ytmp(k)=ya(j,k)
          y2tmp(k)=y2a(j,k)
11      continue
        call splinter(x2a,ytmp,y2tmp,n,x2,yytmp(j))
12    continue
      call spliner(x1a,yytmp,m,1.0D30,1.0D30,y2tmp)
      call splinter(x1a,yytmp,y2tmp,m,x1,y)
      return
      END




* PARAMETERIZATION ROUTINE
      SUBROUTINE splie2(x1a,x2a,ya,m,n,y2a)
      INTEGER m,n,NN
      DOUBLE PRECISION x1a(m),x2a(n),y2a(m,n),ya(m,n)
CU    USES spliner
      INTEGER j,k
      DOUBLE PRECISION y2tmp(1:m+n),ytmp(1:m+n)
      do 13 j=1,m
        do 11 k=1,n
          ytmp(k)=ya(j,k)
11      continue
        call spliner(x2a,ytmp,n,1.0D30,1.0D30,y2tmp)
        do 12 k=1,n
          y2a(j,k)=y2tmp(k)
12      continue
13    continue
      return
      END



************************************************************************
*==============ROUTINES USED BY 2D SPLINE ROUTINES ABOVE===============*
*==========================NEED NOT USE THEM UNLESS A==================*
*===============================1D SPLINE IS NEEDED====================*
************************************************************************


      SUBROUTINE spliner(x,y,n,yp1,ypn,y2)
      INTEGER n
      DOUBLE PRECISION yp1,ypn,x(n),y(n),y2(n)
      INTEGER i,k
      DOUBLE PRECISION p,qn,sig,un,u(n)
      if (yp1.gt.0.99D30) then
        y2(1)=0.0D0
        u(1)=0.0D0
      else
        y2(1)=-0.50D0
        u(1)=(3.0D0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.0D0
        y2(i)=(sig-1.0D0)/p
        u(i)=(6.0D0*((y(i+1)-y(i))/(x(i+
     *1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*
     *u(i-1))/p
11    continue
      if (ypn.gt.0.99D30) then
        qn=0.0D0
        un=0.0D0
      else
        qn=0.50D0
        un=(3.0D0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0D0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      END





      SUBROUTINE splinter(xa,ya,y2a,n,x,y)
      INTEGER n
      DOUBLE PRECISION x,y,xa(n),y2a(n),ya(n)
      INTEGER k,khi,klo
      DOUBLE PRECISION a,b,h
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.0D0) stop 'bad xa input in splinter'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**
     *2)/6.0D0
      return
      END
