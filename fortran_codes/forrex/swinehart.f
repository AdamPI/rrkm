      SUBROUTINE BSWIN(NMax,Ndof,DGen,Rdof)
************************************************************************
* This is the subroutine that folds the states for a particular        *
* degree of freedom into the number of state count.                    *
* The input to the program is:                                         *
*     -> NMax giving the total size of the energy grid.                *
*     -> Ndof is the number of quanta of the degrees of freedom        *
*        possible given the size of the energy grid.                   *
*     -> DGen is the number of states array to be modified.            *
*     -> Rdof is an array specifying the positions on the energy grid  *
*        that the individual degree of freedom states lie.             *
************************************************************************
      IMPLICIT NONE
      INTEGER NMax, i, j, k, Ndof
      DIMENSION DGen(1:NMax), DGTemp(1:NMax), Rdof(1:Ndof)
      INTEGER Rdof
      DOUBLE PRECISION DGen, DGTemp
* We initialize the temporary degeneracy array.
      DO i=1, NMax
         DGTemp(i) = DGen(i) 
         END DO             
* Now we fold in the states of the degrees of freedom into
* the temporary degeneracy array.
      DO k=1, Ndof
         DO i=1, Nmax-Rdof(k)
            DGTemp(Rdof(k)+i) = DGTemp(Rdof(k)+i) + DGen(i)
            END DO
         END DO
* The states for the degree of freedom are now folded in. We
* transfer the results to the permanent array.
      DO k=1, NMax
         DGen(k) = DGTemp(k)
         END DO
      RETURN
      END

