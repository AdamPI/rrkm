      PROGRAM CRAP
      IMPLICIT NONE 
      INTEGER NT, i, NR,j
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2
      DOUBLE PRECISION TAU, Tmax, Tmin, Pi, Rmin, Rmax
      DOUBLE PRECISION Bohr2A, Deg2Rad, EOpt, TOL
      CHARACTER*80 FileNam
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(Deg2Rad=Pi/180.0D0)
      PARAMETER(Bohr2A=0.5291772109217D0)
      ROH1 = 1.0D0/Bohr2A
      ROH2 = 1.0D0/Bohr2A
      AHOO1 = 99.76 * Deg2Rad
      AHOO2 = 80.24 * Deg2Rad
      NT=11
      NR = 75
      Tmin = 0.0D0
      Tmax = Pi
      TOL = 1.0D-13
      Rmin = 1.1D0/Bohr2A
      Rmax = 3.5D0/Bohr2A
999   FORMAT('ROOPES',I2.2,'.OUT')
998   FORMAT(1X,F12.5,4X,E15.6)
      DO i=1, NT
         WRITE(FileNam,999) i
         OPEN(UNIT=1,STATUS='NEW',FILE=FileNam)
         TAU = Tmin+(Tmax-Tmin)/DBLE(NT-1) * DBLE(i-1)
         DO j=1, NR
            ROO = Rmin+(Rmax-Rmin)/DBLE(NR-1)*DBLE(j-1)
            CALL ROOTAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,EOpt,TOL)
            EOpt = (EOpt + (19039.20038266396D0/
     &                         219474.631370515D0) )*627.509469
            WRITE(1,998) ROO*Bohr2A, EOpt
            END DO
         CLOSE(UNIT=1,STATUS='KEEP')
         END DO
      END
************************************************************************
*===================AMOEBA FOR THE FULL OPTIMIZATION===================*
************************************************************************
************************************************************************
*=====================AMOEBA FOR FROZEN TAU & ROO======================*
************************************************************************
      SUBROUTINE amoebart(p,y,mp,np,ndim,ftol,funk,roo,
     &                  tau,iter)
      INTEGER iter,mp,ndim,np,NMAX,ITMAX
      DOUBLE PRECISION ftol,p(mp,np),y(mp),funk,TINY
      PARAMETER (NMAX=20,ITMAX=80000,TINY=1.0D-17)
      EXTERNAL funk
CU    USES amotry,funk
      INTEGER i,ihi,ilo,inhi,j,m,n
      DOUBLE PRECISION rtol,sum,swap,ysave,ytry,psum(NMAX),amotryrt
      DOUBLE PRECISION roo, tau
      iter=0
1     do 12 n=1,ndim
        sum=0.0D0
        do 11 m=1,ndim+1
          sum=sum+p(m,n)
11      continue
        psum(n)=sum
12    continue
2     ilo=1
      if (y(1).gt.y(2)) then
        ihi=1
        inhi=2
      else
        ihi=2
        inhi=1
      endif
      do 13 i=1,ndim+1
        if(y(i).le.y(ilo)) ilo=i
        if(y(i).gt.y(ihi)) then
          inhi=ihi
          ihi=i
        else if(y(i).gt.y(inhi)) then
          if(i.ne.ihi) inhi=i
        endif
13    continue
      rtol=2.0D0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo))+TINY)
      if (rtol.lt.ftol) then
        swap=y(1)
        y(1)=y(ilo)
        y(ilo)=swap
        do 14 n=1,ndim
          swap=p(1,n)
          p(1,n)=p(ilo,n)
          p(ilo,n)=swap
14      continue
        return
      endif
      if (iter.ge.ITMAX) stop 'ITMAX exceeded in amoeba'
      iter=iter+2
      ytry=amotryrt(p,y,psum,mp,np,ndim,funk,roo,tau,ihi,-1.0D0)
      if (ytry.le.y(ilo)) then
        ytry=amotryrt(p,y,psum,mp,np,ndim,funk,roo,tau,ihi,2.0D0)
      else if (ytry.ge.y(inhi)) then
        ysave=y(ihi)
        ytry=amotryrt(p,y,psum,mp,np,ndim,funk,roo,tau,ihi,0.50D0)
        if (ytry.ge.ysave) then
          do 16 i=1,ndim+1
            if(i.ne.ilo)then
              do 15 j=1,ndim
                psum(j)=0.50D0*(p(i,j)+p(ilo,j))
                p(i,j)=psum(j)
15            continue
              y(i)=funk(psum,roo,tau)
            endif
16        continue
          iter=iter+ndim
          goto 1
        endif
      else
        iter=iter-1
      endif
      goto 2
      END


* FUNCTION USED BY THE AMEOBA ABOVE.
      FUNCTION amotryrt(p,y,psum,mp,np,ndim,funk,roo,tau,ihi,fac)
      INTEGER ihi,mp,ndim,np,NMAX
      DOUBLE PRECISION amotryrt,fac,p(mp,np),psum(np),y(mp),funk
      PARAMETER (NMAX=20)
      EXTERNAL funk
CU    USES funk
      INTEGER j
      DOUBLE PRECISION fac1,fac2,ytry,ptry(NMAX), roo, tau
      fac1=(1.0D0-fac)/ndim
      fac2=fac1-fac
      do 11 j=1,ndim
        ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
11    continue
      ytry=funk(ptry,roo,tau)
      if (ytry.lt.y(ihi)) then
        y(ihi)=ytry
        do 12 j=1,ndim
          psum(j)=psum(j)-p(ihi,j)+ptry(j)
          p(ihi,j)=ptry(j)
12      continue
      endif
      amotryrt=ytry
      return
      END

* For holding TAU and ROO constant.
      FUNCTION ROOTAUFUNK(IC,ROO,TAU)
************************************************************************
* The purpose of this routine is for use in the ameoba optimization.   *
* The H2O2 internal coordinates are put into the array IC and the      *
* function ROOTAUFUNK returns the potential energy at the corresponding* 
* geometry in hartrees.  The dihedral angle and the O-O bondlength is  *
* put in separately so the ameoba will not optimize it.                *
*                                                                      *
*    All inputs should be in radians and atomic                        *
*    units (bohr for length).  The IC array is filled as:              *
*      IC(1) =  (O3-H1) bond length.                                   *
*      IC(2) =  (O4-H2) bond length.                                   *
*      IC(3) =  Angle between (O3-H1) bond and (O3--O4) bond.          *
*      IC(4) =  Angle between (O4-H2) bond and (O3--O4) bond.          *
************************************************************************
      IMPLICIT NONE
      DIMENSION IC(1:4), XX(1:4,1:3)
      DOUBLE PRECISION ROOTAUFUNK, IC, PottyOut, XX
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DOUBLE PRECISION cmi2har
      INTEGER iflag
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* Take out the internal coordinates.
      ROH1 = IC(1)
      ROH2 = IC(2)
      AHOO1= IC(3)
      AHOO2= IC(4)
* Convert to the cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* Find the potential energy
      iflag = 0
      CALL hoohpot(XX,PottyOut,iflag)
      ROOTAUFUNK = PottyOut * cmi2har
      RETURN
      END

      SUBROUTINE INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
************************************************************************
* The purpose of this routine is to compute a cartesian geometry for   *
* H2O2 system given the internal coordinates.  The H2O2 system is:     *
*                           H1-O3-----O4-H2                            *
* and the internal coordinates are given according to those from the   *
* paper:  B. Kuhn, T.R. Rizzo, D. Luckhaus, M. Quack, M.A. Suhm, J.    *
*         Chem. Phys. 111, 2566 specifically on pg. 2569.              *
* The cartesian coordinates are output into the XX array in order of   *
* the indices shown above.                                             *
* * ALL ANGLES SHOULD BE INPUT IN RADIANS.                             *
************************************************************************
      IMPLICIT NONE
      DIMENSION XX(1:4,1:3)
      DOUBLE PRECISION XX, ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
* Define the oxygen coordinates.
      XX(3,1) = 0.0D0
      XX(3,2) = 0.0D0
      XX(3,3) = 0.0D0
      XX(4,1) = ROO
      XX(4,2) = 0.0D0
      XX(4,3) = 0.0D0
* Define the first hydrogen coordinate.
      XX(1,1) = ROH1 * DCOS( AHOO1 )
      XX(1,2) = ROH1 * DSIN( AHOO1 )
      XX(1,3) = 0.0D0
* Define the second hydrogen coordinate.
      XX(2,1) = ROO + ROH2 * DCOS( AHOO2 )
      XX(2,2) = ROH2 * DCOS( TAU ) * DSIN(AHOO2 )
      XX(2,3) = ROH2 * DSIN( TAU ) * DSIN(AHOO2 )
      RETURN
      END


* Frozen dihedral angle AND O--O bond length.
      SUBROUTINE ROOTAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,
     &                     TAU,EOpt,TOL)
************************************************************************
* The purpose of this routine is to minimize the H2O2 molecule with    *
* the amoeba routine.  Some liberties have been taken to design the    *
* initial simplex points for the ameoba (images have been generated    *
* by taking the initial guess and perturbing each of the coordinates   *
* by an amount specified by the Step array.                            *
* All input and output should be in radians and atomic units!!!        *
* This routine DOES NOT OPTIMIZE the O-O bond length ROO and the       *
* dihedral angle TAU which are left as the values input and are frozen.*
* The input into this routine is:                                      *
*     -> ROH1 is the first OH bond length in H2O2.                     *
*     -> ROH2 is the second OH bond length in H2O2.                    *
*     -> AHOO1 is the angle between the first OH bond and the OO bond. *
*     -> AHOO2 is the angle between the second OH bond and the OO bond.*
*     -> TAU is the H2O2 dihedral angle.                               *
*     **** The coordinates above are output as their optimized values. *
*     -> TOL is the tolerance for the energy change that is used to    *
*        determine if the potential is optimized.                      *
* The output from this routine:                                        *
*     -> The coordinates are returned in their optimized values,       *
*        except TAU and ROO which are frozen.                          *
*     -> EOpt is the potential energy of the output structure.         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag
      DIMENSION IC(1:5,1:4), PEn(1:5), XX(1:4,1:3), Step(1:4)
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU, Step
      DOUBLE PRECISION IC, TOL, XX, PEn, PottyOut, EOpt
      DOUBLE PRECISION cmi2har, deg2rad, Pi, ROOTAUFUNK
      EXTERNAL ROOTAUFUNK
      INTEGER Ncoor, Nimage, Niter, Ndim
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(deg2rad=Pi/180.0D0)
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we define initial step-sizes for each coordinate.
      Step(1) = 0.150D0 
      Step(2) = 0.13D0
      Step(3) = 3.2D0 * deg2rad
      Step(4) = 3.0D0 * deg2rad
* First, we put the initial coordinates into the optimation array.
      IC(1,1) = ROH1
      IC(1,2) = ROH2
      IC(1,3) = AHOO1
      IC(1,4) = AHOO2
* We find the energy of this geometry.
      iflag=0
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      PEn(1) = PottyOut * cmi2har
* Now, we generate six other images needed by alterning each coordinate,
* by a step-size.
      DO i=1, 4
         DO j=1, 4
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,IC(i+1,3),
     &                 IC(i+1,4),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 5
      Ncoor = 4
      Ndim = 4
      CALL amoebart(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &              ROOTAUFUNK,ROO,TAU,Niter)
* We restart the optimization in order to make sure
* the amoeba didn't behave badly.
      DO i=1, 4
         DO j=1, 4
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,IC(i+1,3),
     &                 IC(i+1,4),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 5
      Ncoor = 4
      Ndim = 4
      CALL amoebart(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &              ROOTAUFUNK,ROO,TAU,Niter)
* Now, we retrieve the optimized geometry and energy.
      ROH1 = IC(1,1)
      ROH2 = IC(1,2)
      AHOO1= IC(1,3)
      AHOO2= IC(1,4)
      EOpt = PEn(1)
      RETURN
      END 
