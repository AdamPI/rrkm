      PROGRAM TEST
      IMPLICIT NONE
      INTEGER NTAU, NEmax, NEtot, NPTAU, NESPL
      DOUBLE PRECISION ROH1, ROH2, ROO, TAU
      DOUBLE PRECISION AHOO1, AHOO2, Etotmin, Etotmax
      DOUBLE PRECISION XX, Deg2Rad, Pi, Bohr2A, Emin
      DOUBLE PRECISION Emax, DelE, TOL
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(Deg2Rad=Pi/180.0D0)
      PARAMETER(Bohr2A=0.5291772109217D0)
      DIMENSION II(1:3)
      DOUBLE PRECISION II
      DOUBLE PRECISION cmi2har, kcal2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      PARAMETER(kcal2har=1.0D0/627.509469D0)
* Set the initial guess for the equilibrium geometry.
      ROH1 = 1.818
      ROH2 = 1.818
      ROO = 2.745
      AHOO1 = 99.76 * Deg2Rad
      AHOO2 = 80.24 * Deg2Rad
      TAU = 114.30 * Deg2Rad
      TOL=1.0D-12
      CALL FULLMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Emin,TOL)
* Change ROO to a distance where you want to find the number of states.
* Leave the other coordinates as guesses to real geometry.
      ROO = ROO * 1.75D0
* Now do the state count.
      Emax = 100.0D0
      DelE = 10.0D0
      NEmax = INT( (Emax/DelE) * (kcal2har/cmi2har) ) + 1 
      NTAU = 50
      CALL NOSROO(ROO,ROH1,ROH2,AHOO1,AHOO2,Emin,Emax,DelE,NEmax,NTAU)
* Now we do the integration.
      CALL ROOMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Etotmin,TOL)
      Etotmin = Etotmin - Emin
      Etotmax = Etotmin + 50.0D0*kcal2har
      NPTAU = 30
      NEtot = 51
      NESPL = 200
      CALL TAUINTEG(NEmax,NTAU,NPTAU,Etotmax,Etotmin,NEtot,NESPL)
      END
