      SUBROUTINE TAUINTEG(NEmax,NTAU,NPTAU,Etotmax,Etotmin,NEtot,
     &                    NESPL)
************************************************************************
* This routine does the classical integral over the torsion angle TAU  *
* in order to find the complete sum of states.                         *
* The input to this program is:                                        *
*     -> NEmax is the number of points in the energy grid.             *
*     -> NTAU is the number of points in the torsion angle grid.       *
*     -> NPTAU is the number of torsion momentum quadrature points to  *
*        be used in the phase space integration.                       *
*     -> Etotmax is the maximum total energy to be considered.         *
*     -> Etotmin is the minimum total energy to be considered.         *
*     -> NEtot is the number of energy points to generate the N(E,J)   *
*        at.                                                           *
*                                                                      *
* This program is used with the following caveats:                     *
*  -> Right now molecule is ASSUMED TO BE A PROLATE TOP.  This should  *
*     have a test written for it so program can determine at each      *
*     value of tau whether molecule is prolate, oblate, or symmetric.  *
*  -> The value of the energy is defined by the counting routine run   *
*     before this.                                                     *
*  -> Torsion zero point energy is not accounted for (though the       *
*     zero point of every other oscillator is).                        *
*  -> Energy reference defined by the program before this.             *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, k, NEmax, NTAU, NPTAU, NJJMAX, NEtot
      INTEGER kkk, jjj, JMAX, JMAXMAX, JMAXMAX2, KKMAX
      INTEGER NESPL
      PARAMETER(NJJMAX=10000)   ! Maximum J angular momentum number considered.
      DIMENSION TAUpt(1:NTAU), TAUwght(1:NTAU)  
      DIMENSION SoS(1:NEmax,1:NTAU), Egrid(1:NEmax)
      DIMENSION PotTau(1:NTAU), SPLARR(1:NEmax,1:NTAU)
      DIMENSION II(1:NTAU,1:3), IITAU(1:NTAU), PTAUwgt(1:NPTAU)
      DIMENSION PTAU(1:NPTAU), ZPEosc(1:NTAU), SoSSPL(1:NESPL,1:NTAU)
      DIMENSION NJtot(0:NJJMAX), SPLARR2(1:NESPL,1:NTAU)
      DIMENSION Espl(1:NESPL)
      DOUBLE PRECISION SoS, PotTau, Egrid, TAUpt, TAUwght, Pi
      DOUBLE PRECISION TauMax, TauMin, SPLARR, II, IITAU
      DOUBLE PRECISION NJ, Eavail, ZPEosc, Nosc, SoSSPL
      DOUBLE PRECISION Etotmax, Etotmin, Etot, Espl
      DOUBLE PRECISION PTAU, PTAUmax, PTAUmin, PTAUwgt, IIB
      DOUBLE PRECISION PTAUMAXER, Eosc, SPLARR2
      DOUBLE PRECISION SYMTPE, NJtot
      INTEGER JMAXER, KMAXER
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      CHARACTER*80 FileNam
      INCLUDE './incfile.inc'
************************************************************************
*++++++++++++++++++DEFINE ANGULAR INTEGRATION GRID+++++++++++++++++++++*
************************************************************************
* Define the minimum and maximum values of the torsion angle.
      TauMax = 2.0D0 * Pi
      TauMin = 0.0D0
* We use the legendre quadrature points as the torsion points.
      CALL gauleg(TAUmin,TAUmax,TAUpt,TAUwght,NTAU)
************************************************************************
*++++++++++++++++READ IN NECESSARY QUANTITIES FROM FILE++++++++++++++++*
************************************************************************
* Here we read in the SoS grid and the energy grid for the
* 2D spline.
      WRITE(FileNam,996)
      OPEN(UNIT=1,STATUS='OLD',FILE=FileNam)
      WRITE(FileNam,994)
      OPEN(UNIT=2,STATUS='OLD',FILE=FileNam)
      DO i=1, NTAU
         DO j=1, NEmax
            READ(1,*) SoS(j,i)
            END DO
         END DO
      DO i=1, NEmax
         READ(2,*) Egrid(i)
         END DO
      CLOSE(UNIT=1,STATUS='KEEP')    ! These can be changed to delete
      CLOSE(UNIT=2,STATUS='KEEP')    ! after testing.
* Now we read in the moments of inertia.
      WRITE(FileNam,998)
      OPEN(UNIT=1,STATUS='OLD',FILE=FileNam)
      WRITE(FileNam,997)
      OPEN(UNIT=2,STATUS='OLD',FILE=FileNam)
      WRITE(FileNam,999)
      OPEN(UNIT=3,STATUS='OLD',FILE=FileNam)
      WRITE(FileNam,993)
      OPEN(UNIT=4,STATUS='OLD',FILE=FileNam)
      DO i=1, NTAU
         READ(1,*) II(i,1), II(i,2), II(i,3)
         READ(2,*) IITAU(i)
         READ(3,*) PotTau(i)
         READ(4,*) ZPEosc(i)
         END DO
      CLOSE(UNIT=1,STATUS='KEEP')   ! Again, these files can be
      CLOSE(UNIT=2,STATUS='KEEP')   ! deleted after test.
      CLOSE(UNIT=3,STATUS='KEEP')
      CLOSE(UNIT=4,STATUS='KEEP')
************************************************************************
*++++++++++++++++READY SPLINE OF SUM OF STATES+++++++++++++++++++++++++*
************************************************************************
* Ready the pre-spline spline.
      CALL splie2(Egrid,TAUpt,SoS,NEmax,NTAU,SPLARR)
* Now we make points, because likely the spline above probably much too
* big to be of much use.
      DO i=1, NESPL
         Espl(i) = Egrid(1) + 
     &            (Egrid(NEmax)-Egrid(1))/DBLE(NESPL-1)*DBLE(i-1)
         DO j=1, NTAU
            CALL splin2(Egrid,TAUpt,SoS,SPLARR,NEmax,NTAU,Espl(i),
     &                  TAUpt(j),SoSSPL(i,j))            
            END DO
         END DO
* The reduced spline points have been defined, now we can make the small
* spline.
      CALL splie2(Espl,TAUpt,SoSSPL,NESPL,NTAU,SPLARR2)
************************************************************************
*++++++++++++++++INTEGRATION OVER TAU & PTAU+++++++++++++++++++++++++++*
************************************************************************
* Start the integration
      JMAXMAX = 0
*     * 1 BEGIN OUTER LOOP FOR EACH ENERGY.
      DO i=1, NEtot       
         JMAXMAX2 = 0
* Determine the total energy relative to the ground state
* energy of the reactants (...right now absolute energy)...
         Etot=Etotmin+(Etotmax-Etotmin)/DBLE(NEtot-1)*DBLE(i-1)
* Initialize array that will contain each J sum of states.
         DO j=0, NJJMAX
            NJtot(j) = 0.0D0
            END DO
*        * 2 BEGIN LOOP OVER ANGLE QUADRATURE.
         DO j=1, NTAU
* Calculate the maximum J value at this TAU.  We will assume
* H2O2 remains a PROLATE TOP THROUGHOUT THIS!!! Need to change
* this to be smarter for another molecule!!!
            Eavail = Etot-PotTau(j)-ZPEosc(j)
            JMAX = JMAXER(II(j,1),Eavail)
            IF ( JMAX .GT. JMAXMAX ) JMAXMAX = JMAX
            IF ( JMAX .GT. JMAXMAX2 ) JMAXMAX2 = JMAX
            IF ( JMAX .GT. NJJMAX ) STOP 'FAIL-UP NJJMAX IN INTEG'
*           * 3 BEGIN LOOP LOOP OVER ROTATIONAL QUANTUM NUMBERS.
            DO jjj=0, JMAX
               NJ = 0.0D0
* FIRST WE DO THE K=0 PIECE.  WE CREATE THE MOMENTUM POINTS TO
* FIT ITS POSSIBLE RANGE GIVEN THE ROTATIONAL ENERGY
               Eavail = Etot-ZPEosc(j)-
     &                  (1.0D0/(2*II(j,1)))*DBLE(jjj*(jjj+1)) -
     &                  PotTau(j)
              IF ( Eavail .GT. 0.0D0 ) THEN
* Define the momentum quadrature points.
               PTAUmax = PTAUMAXER(Eavail,IITAU(j))
               PTAUmin = - PTAUmax
               CALL gauleg(PTAUmin,PTAUmax,PTAU,PTAUwgt,NPTAU)
*              * 4 BEGIN LOOP OVER MOMENTUM QUADRATURE POINTS.
               DO k=1, NPTAU
                  Eosc = Etot - ZPEosc(j) - 
     &                   (1.0D0/(2*II(j,1)))*DBLE(jjj*(jjj+1)) -
     &                   PotTau(j) - PTAU(k)**2/(2.0D0*IITAU(j))
                  IF ( Eosc .GT. 0.0D0 ) THEN
                     CALL splin2(Espl,TAUpt,SoSSPL,SPLARR2,
     &                           NESPL,NTAU,Eosc,TAUpt(j),Nosc)
                     NJ = NJ + Nosc * PTAUwgt(k)
                  ELSE
                     NJ = NJ
                     END IF
                  END DO
*              * 4 END LOOP OVER MOMENTUM QUADRATURE POINTS.
                 END IF
* NOW WE ADD THE K>0 POINTS.
               IF ( jjj .GT. 0) THEN
               Eavail = Etot - ZPEosc(j) - PotTau(j)
               IIB = DSQRT( II(j,2) * II(j,3) )       ! Compute geometric average
               KKmax = KMAXER(II(j,1),IIB,jjj,Eavail) ! for prolt top degerate II.
*              * 5 BEGIN LOOP OVER HELICITY QUANTUM NUMBERS.
               DO kkk=1, KKmax
                  Eavail = Etot-ZPEosc(j)-PotTau(j)-
     &                     SYMTPE(jjj,kkk,II(j,1),IIB)
                 IF ( Eavail .GT. 0.0D0 ) THEN
* Define the momentum quadrature points.
                  PTAUmax = PTAUMAXER(Eavail,IITAU(j))
                  PTAUmin = -PTAUmax
                  CALL gauleg(PTAUmin,PTAUmax,PTAU,PTAUwgt,NPTAU)
*                 * 6 BEGIN LOOP OVER MOMENTUM QUADRATURE POINTS.
                  DO k=1, NPTAU
                     Eosc = Etot-ZPEosc(j)-PotTau(j)-
     &                      SYMTPE(jjj,kkk,II(j,1),IIB)-
     &                      PTAU(k)**2 / (2.0D0 * IITAU(j) )
                     IF ( Eosc .GT. 0.0D0 ) THEN
                        CALL splin2(Espl,TAUpt,SoSSPL,SPLARR2,
     &                              NESPL,NTAU,Eosc,TAUpt(j),Nosc)
                        NJ = NJ + 2.0D0 * Nosc * PTAUwgt(k)
                     ELSE
                        NJ = NJ
                        END IF
                     END DO
*                 * 6 END LOOP OVER MOMENTUM QUADRATURE POINTS.
                   END IF
                  END DO
*             * 5 END LOOP OVER MOMENTUM QUADRATURE POINTS.
               END IF
* Perform the Tau integral.  Factor of 2*Pi is Planck's constant.
               NJtot(jjj)=NJtot(jjj)+NJ*TAUwght(j)/(2.0D0*Pi)
               END DO
*           * 3 END LOOP OVER ANGULAR MOMENTUM QUANTUM NUMBER.
             END DO   
*         * 2 END LOOP OVER ANGLE QUADRATURE POINTS.
          DO j=0, JMAXMAX2
* Open file for correct E and J.
* First, determine file name.
             IF( (j .GE. 0) .AND. (j .LT. 10) ) THEN
                 WRITE(FileNam,981) j
             ELSE IF( (j .GE. 10) .AND. (j .LT. 100) ) THEN
                 WRITE(FileNam,982) j
             ELSE IF( (j .GE. 100) .AND. (j .LT. 1000)) THEN
                 WRITE(FileNam,983) j
             ELSE IF( (j .GE. 1000) .AND. (j .LT. 10000)) THEN
                 WRITE(FileNam,984) j
             ELSE IF( (j .GE. 10000) .AND. (j .LT. 100000))THEN
                 WRITE(FileNam,985) j
             ELSE 
                 STOP 'JMAX TOO BIG IN INTEG!'
                 END IF
* Now open the file.  First if tests for file existence for the 
* first J's, but doesn't test for higher J's generated by higer energies.
* The IF is a safety measure, but not a good one.
             IF ( i .EQ. 1)  THEN
                OPEN(UNIT=1,STATUS='NEW',FILE=FileNam)
             ELSE         
                OPEN(UNIT=1,STATUS='UNKNOWN',FILE=FileNam,
     &               ACCESS='APPEND')
                END IF
* Write to file and close.
             WRITE(1,896) Etot, NJtot(j)
             CLOSE(UNIT=1,STATUS='KEEP')
             END DO
         END DO 
*     * 1 END LOOP OVER ENERGY.
* Write the total number of J's considered to a file.
      WRITE(FileNam,992)
      OPEN(UNIT=1,STATUS='NEW',FILE=FileNam)
      WRITE(1,*) JMAXMAX
      CLOSE(UNIT=1,STATUS='KEEP')
      RETURN
      END         


                  
************************************************************************
*===================FUNCTIONS USED IN ROUTINE ABOVE====================*
************************************************************************
      FUNCTION SYMTPE(JJ,KK,IIA,IIB)
************************************************************************
* This function gives the energy of a symmetric top in atomic units.   *
* The input for this routine is:                                       *
*     -> JJ is the overall rotational quantum number.                  *
*     -> KK is the helicity quantum number.                            *
*     -> IIA is the unique moment of inertia in atomic units.          *
*     -> IIB is the degenerate moment of inertia in atomic units.      *
************************************************************************
      IMPLICIT NONE
      INTEGER JJ, KK
      DOUBLE PRECISION IIA, IIB, SYMTPE
      DOUBLE PRECISION Aconst, Bconst
      Aconst = 1.0D0/(2.0D0*IIA)
      Bconst = 1.0D0/(2.0D0*IIB) - ( 1.0D0/(2.0D0*IIA) )
      SYMTPE = Aconst * DBLE(JJ*(JJ+1)) + Bconst * DBLE(KK**2)
      RETURN
      END



      FUNCTION PTAUMAXER(Eavail,IITAU)
************************************************************************
* This routine calculates the maximum rotational momentum that can be  *
* achieved with the available energy.  The input to this program is:   *
*    -> Eavail is the available energy for the kinetic rotational      *
*       energy.                                                        *
*    -> IITAU is the rotational moment of inertia for the rotation.    *
************************************************************************
      IMPLICIT NONE
      DOUBLE PRECISION PTAUMAXER, Eavail, IITAU
      IF( Eavail .GT. 0.0D0 ) THEN
         PTAUMAXER = DSQRT( 2.0D0 * IITAU * Eavail )
      ELSE
         PTAUMAXER = 0.0D0
      END IF
      RETURN
      END



      FUNCTION JMAXER(IIA,EAble)
************************************************************************
* This routine gives the maximum J quantum number for a symmetric top  *
* molecule when the available kinetic energy for rotation is EAble.    *
*     -> EAble is the available kinetic energy for rotation in hartree.*
*     -> IIA is the unique moment of inertia in atomic units.          *
************************************************************************
      IMPLICIT NONE
      INTEGER JMAXER
      DOUBLE PRECISION IIA, EAble
      IF( EAble .GT. 0.0D0 ) THEN
      JMAXER=INT(-0.50D0+0.50D0*DSQRT(1.0D0+8.0D0*IIA*EAble))
      ELSE
      JMAXER = 0
      END IF
      RETURN
      END 


      FUNCTION KMAXER(IIA,IIB,J,EAble)
***********************************************************************
* This function gives the maximum possible K helicity quantum number  *
* for a symmetric top at a given value of J, the angular momentum     *
* quantum number, and at a given value of available kinetic energy    *
* for rotation.  The input to this function is:                       *
*    -> IIA is the unique moment of rotationl inertia in atomic units.*
*    -> IIB is the degenerate moment of rotational inertia in atomic  *
*       units.                                                        *
*    -> J is the overall angular momentum quantum number.             *
*    -> EAble is the available kinetic energy for rotation in hartree.*
***********************************************************************
      IMPLICIT NONE
      INTEGER KMAXER, J
      DOUBLE PRECISION EAble, IIA, IIB
      DOUBLE PRECISION Aconst, Bconst
      IF ( EAble .GT. 0.0D0 ) THEN 
      Aconst = 1.0D0/(2.0D0*IIA)
      Bconst = (1.0D0/(2.0D0*IIB)-(1.0D0/(2.0D0*IIA)))
      KMAXER=INT(DSQRT(EAble/Bconst-Aconst/Bconst*DBLE(J*(J+1))))
         IF ( J .GT. KMAXER) KMAXER = J
      ELSE
      KMAXER = 0
      END IF
      RETURN
      END
