      SUBROUTINE HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,
     &                XX,HESSIAN,III)
************************************************************************
* This routine calculates the Hessian using a 5 point divided          *
* difference method. The input geometry is in the hydrogen peroxide    *
* internal coordinates of the Quack PES.  The input parameters are:    *
*                       H1-O3----O4-H2     # Gives indexing system     *
*                                            for cartesian arrays.     *
*   -> ROH1 and ROH2 are the OH bond lengths of the H2O2.              *
*   -> AHOO1 and AHOO2 are the angles the OH bonds make with the OO    *
*      OO bond.                                                        *
*   -> TAU is the dihedral angle.                                      *
*   -> Step is the divided difference stepsize for the 5 point formula.*
*      Step should be (a small value) in bohr.                         *
* The output is:                                                       *
*   -> XX is the cartesian coordinates corresponding to the input      *
*      internal coordinates.  The frame is the COM and principle axes. *
*   -> III is an array containing the 3 moments of rotational inertia. *
*   -> HESSIAN is an array containing the cartesian (not MW'ed)        *
*      hessian matrix.  It is organized in the same way as the         *
*      cartesian array XX rather than as a 2D array.  It is a 4D array *
*      (atom#,XYZ#,atom#,XYZ#) where a coordinate is dilineated by     *
*      the atom index and the XYZ index.                               *
*                                                                      *
*  *** INPUTS AND OUTPUTS SHOULD BE IN ATOMIC UNITS AND RADIANS ***    *
************************************************************************
      IMPLICIT NONE
      INTEGER Natom, i, j, k, ii, jj, flagCOM, flagROT
      DIMENSION HESSIAN(1:4,1:3,1:4,1:3), XX(1:4,1:3)
      DIMENSION XXtemp(1:4,1:3)
      DOUBLE PRECISION HESSIAN, XX, ROH1, ROH2, ROO, AHOO1
      DOUBLE PRECISION AHOO2, TAU, XXtemp
      DIMENSION FUNKGUY(1:4,1:4), III(1:3), NQmax(1:2)
      DIMENSION NQi(1:2), NQj(1:2), FUNK2(-4:4)
      DOUBLE PRECISION FUNKGUY, FUNK2
      INTEGER NQi, NQj
      INTEGER NMax, iflag, NumNums, NQmax, counti, countj
      DOUBLE PRECISION PottyOut, Step, III
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we set iflag = 0 to initialize the potential.
      iflag = 0
* Now, we convert the internal coordinates to cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
* And now to COM frame in the principle axes.
      flagCOM =1  
      flagROT =1
      CALL INERT(XX,III,flagCOM,flagROT)
* Now we define the parameters needed by the INTMAP routine to manage
* matrix indices.
      NumNums=2
      NMax = 12
      NQmax(1)=4
      NQmax(2)=3
* The Hessian matrix is now constructed.  We first construct the 
* off-diagonal elements.
* Set the temporary geometry to the input geometry.  The temporary
* geometry is used for the finite differences.
      DO i=1, NQmax(1)
         DO j=1, NQmax(2)
            XXtemp(i,j) = XX(i,j)
            END DO
         END DO
      DO i=1, NMax-1
         CALL INTMAP(NumNums,NQmax,NQi,i,-1)
         DO j=i+1, NMax
            CALL INTMAP(NumNums,NQmax,NQj,j,-1)
            counti=0
            DO ii=-2, 2, 1
               IF ( ii .NE. 0) THEN
                  counti=counti+1
                  countj=0
                  END IF
               DO jj=-2, 2, 1
                  IF( ( ii .NE. 0 ) .AND. (jj .NE. 0 ) ) THEN
                     countj=countj+1
* Vary the cartesian coordinates for the potential calculation.
                     XXtemp(NQi(1),NQi(2)) = XX(NQi(1),NQi(2)) + 
     &                                       DBLE(ii)*Step
                     XXtemp(NQj(1),NQj(2)) = XX(NQj(1),NQj(2)) + 
     &                                       DBLE(jj)*Step
* Calculate and store the potential points for the divided difference.
                     CALL hoohpot(XXtemp,PottyOut,iflag)
                     FUNKGUY(counti,countj) = PottyOut *cmi2har
* Reset the temporary geometry for future calculations.
                     XXtemp(NQi(1),NQi(2)) = XX(NQi(1),NQi(2))
                     XXtemp(NQj(1),NQj(2)) = XX(NQj(1),NQj(2))
                     END IF
                  END DO
               END DO
* Use the 5 Point Divided Difference to comput the Hessian matrix
* element.
              HESSIAN(NQi(1),NQi(2),NQj(1),NQj(2)) = 
     &      (1.0D0/(12.0D0*Step))**2 *    (
     &             (FUNKGUY(1,1)-8.0D0*FUNKGUY(1,2)+8.0D0*FUNKGUY(1,3)-
     &              FUNKGUY(1,4)) -
     &      8.0D0* (FUNKGUY(2,1)-8.0D0*FUNKGUY(2,2)+8.0D0*FUNKGUY(2,3)-
     &              FUNKGUY(2,4)) +
     &      8.0D0* (FUNKGUY(3,1)-8.0D0*FUNKGUY(3,2)+8.0D0*FUNKGUY(3,3)-
     &              FUNKGUY(3,4)) - 
     &             (FUNKGUY(4,1)-8.0D0*FUNKGUY(4,2)+8.0D0*FUNKGUY(4,3)-
     &              FUNKGUY(4,4))     
     &                                )
* Use the fact of symmetric Hessian to determine the other half of the 
* matrix.
              HESSIAN(NQj(1),NQj(2),NQi(1),NQi(2)) = 
     &                     HESSIAN(NQi(1),NQi(2),NQj(1),NQj(2))
              END DO
          END DO
* Now we need to calculate the diagonal elements which takes fewer 
* functional evaluations.
          DO i=1, NQmax(1)
             DO j=1, NQmax(2)
                DO k=-4, 4, 1
                   XXtemp(i,j) = XX(i,j) + DBLE(k) * Step
                   CALL hoohpot(XXtemp,PottyOut,iflag)
                   FUNK2(k) = PottyOut * cmi2har
                   XXtemp(i,j) = XX(i,j)
                   END DO
* Organize potential energies for divided difference calculations.
               FUNKGUY(1,1) = FUNK2(-4)
               FUNKGUY(2,2) = FUNK2(-2)
               FUNKGUY(3,3) = FUNK2(2)
               FUNKGUY(4,4) = FUNK2(4)
               FUNKGUY(1,2) = FUNK2(-3)
               FUNKGUY(1,3) = FUNK2(-1)
               FUNKGUY(1,4) = FUNK2(0)
               FUNKGUY(2,3) = FUNK2(0)
               FUNKGUY(2,4) = FUNK2(1)
               FUNKGUY(3,4) = FUNK2(3)
               DO ii=1, 3
                  DO jj=ii+1, 4
                     FUNKGUY(jj,ii) = FUNKGUY(ii,jj)
                     END DO
                  END DO
* The 5 point divided difference calculation is performed.
               HESSIAN(i,j,i,j) =
     &           (1.0D0/(12.0D0*Step))**2 *  (
     &              (FUNKGUY(1,1)-8.0D0*FUNKGUY(2,1)+
     &                            8.0D0*FUNKGUY(3,1)-FUNKGUY(4,1)) -
     &          8.0D0*(FUNKGUY(1,2)-8.0D0*FUNKGUY(2,2)+
     &                            8.0D0*FUNKGUY(3,2)-FUNKGUY(4,2)) +
     &          8.0D0*(FUNKGUY(1,3)-8.0D0*FUNKGUY(2,3)+
     &                            8.0D0*FUNKGUY(3,3)-FUNKGUY(4,3)) -
     &              (FUNKGUY(1,4)-8.0D0*FUNKGUY(2,4)+
     &                            8.0D0*FUNKGUY(3,4)-FUNKGUY(4,4)) 
     &                                     )
               END DO 
           END DO
      RETURN
      END
