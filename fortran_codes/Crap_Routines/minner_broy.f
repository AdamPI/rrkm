      SUBROUTINE MINFULLB(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,
     &           PotOut,TOL)
      IMPLICIT NONE
      INTEGER i, Ncoord, Niter, NStep, iflag
      DOUBLE PRECISION ROH1, ROH2, AHOO1, AHOO2, TAU, TOL
      DIMENSION IC(1:6), Step(1:6), XX(1:4,1:3)
      EXTERNAL GRADFULL
      DOUBLE PRECISION IC, Step, GTest, RTest, XX
      DOUBLE PRECISION PotOut, ROO, cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      Step(1) = 0.0010D0
      Step(2) = 0.0010D0
      Step(3) = 0.00100D0
      Step(4) = 0.00100D0
      Step(5) = 0.0010D0
      Step(6) = 0.0010D0
      IC(1) = ROH1
      IC(2) = ROH2
      IC(3) = ROO
      IC(4) = AHOO1
      IC(5) = AHOO2
      IC(6) = TAU
      Ncoord = 6 
      Niter=0
      NStep=80
      CALL BRYDRV(Ncoord,IC,GRADFULL,Step,ROO,TAU,TOL,GTest,
     &            RTest,Niter,NStep)
      ROH1  = IC(1)
      ROH2  = IC(2)
      ROO   = IC(3)
      AHOO1 = IC(4)
      AHOO2 = IC(5)
      TAU   = IC(6)
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      iflag = 0
      CALL hoohpot(XX,PotOut,iflag)
      PotOut = PotOut * cmi2har
      RETURN
      END
