      SUBROUTINE BRYDRV(N,Xini,Grad,Step,ROO,TAU,TOL,
     &                  Gtest,Rtest,Niter,NStep)
************************************************************************
* This is the driver for Broyden's extremum finding method (the multi- *
* dimensional analogue of the Secant Method for finding roots).  The   *
* input into this method is:                                           *
*    -> The number of dimensions N.                                    *
*    -> An initial guess array for the extremum Xini. This vector is   *
*       DESTROYED in the routine.  Upon successful output, this array  *
*       contains the converged values.                                 *
*    -> Grad, the routine  that returns the gradient in a 1D array     *
*       which must have the form Grad(N,X,Del).                        *
*    -> Step is the step-size for the 5 point divided difference used  *
*       to generate a numerical Hessian matrix in the first Broyden    *
*       step.                                                          *
*    -> TOL is the tolerance (the stopping criteria).  If the          *
*       magnitude of the gradient and the magnitude of the displacement*
*       are simultaneously less than TOL, the routine exits and the    *
*       system is considered to have converged.                        *
*    -> Niter is an integer.  Upon output, it will contain the number  *
*       of steps the optimization has made.                            *
*    -> NStep is the MAXIMUM number of Broyden steps to be taken.      *
*    -> ROO is the O--O bond length in H2O2 in bohr.  If the gradient  *
*       does not include this coordinate (if the coordinate is froze)  *
*       then the ROO value input is used in the gradient.  Otherwise   *
*       ROO is just a placeholder.                                     *
*    -> TAU is the H2O2 dihedral in radians.
* If this routine fails, the program execution will be terminated and  *
* and error message printed to ERROR.OUT in the directory where the    *
* program is executed.                                                 *
* The routine BRYDN1 will require the routine gaussj.                  *
************************************************************************   
      IMPLICIT NONE
      EXTERNAL Grad
      INTEGER N, i, NStep, Niter
      DIMENSION Xini(1:N), Xout(1:N), HssnInv(1:N,1:N)
      DIMENSION Del(1:N), Step(1:N)
      DOUBLE PRECISION Xini, Step, TOL, Xout, Rtest
      DOUBLE PRECISION Gtest, Xtest, Del, HssnInv, ROO, TAU
* We first perform a BRYDN1 run to set up the initial guesses.
      Niter = 1
      CALL BRYDN1(N,Xini,Xout,ROO,TAU,Grad,HssnInv,Step)
* We now test to see if our guess is already converged.
      CALL Grad(N,Xout,ROO,TAU,Step,Del)
      Gtest = 0.0D0
      Rtest = 0.0D0
      DO i=1, N
         Gtest = Gtest + Del(i)**2
         Rtest = Rtest + (Xout(i) - Xini(i))**2
         END DO
      Gtest = DSQRT(Gtest)
      Rtest = DSQRT(Rtest)
      IF( (Gtest .LT. TOL) .AND. (Rtest .LT. TOL) ) THEN
         DO i=1, N
            Xini(i) = Xout(i)
            END DO
         RETURN
         END IF
* If our first Broyden approximation was not good enough, we now use 
* the main Broyden iteration to optimize.
      DO Niter = 2, NStep   
         CALL BRYDN2(N,Xout,Xini,ROO,TAU,HssnInv,Grad,Step)
         CALL Grad(N,Xout,ROO,TAU,Step,Del)
         Gtest = 0.0D0
         Rtest = 0.0D0
         DO i=1, N
            Gtest = Gtest + Del(i)**2
            Rtest = Rtest + (Xout(i) - Xini(i))**2
            END DO
         Gtest = DSQRT(Gtest)
         Rtest = DSQRT(Rtest)
******        
         WRITE(*,*) 'BROYDEN ITERATION ', Niter
         WRITE(*,*) Xini(1),Xini(2),Xini(3),Xini(4),Xini(5),Xini(6)
         WRITE(*,*) ''
         WRITE(*,*) 'Rtest= ', Rtest,' Gtest= ', Gtest
******
         IF( (Gtest .LT. TOL) .AND. (Rtest .LT. TOL) ) THEN
            DO i=1, N
               Xini(i) = Xout(i)
               END DO
            RETURN
            END IF
         END DO
* If the extremum was located, the routine should have exitted above.
* If it doesn't we must print an error message to the file ERROR.OUT
* and stop the execution of the program.
999   FORMAT(D25.18)
      OPEN(UNIT=1,STATUS='UNKNOWN',FILE='ERROR.OUT')
      WRITE(1,*) 'Broyden convergence error.  Convergence not '
      WRITE(1,*) 'achieved after ', Niter, ' Broyden iterations.'
      WRITE(1,*) 'The magnitude of the gradient was:'
      WRITE(1,999) Gtest
      WRITE(1,*) 'The magnitude of the displacement DeltaR was:'
      WRITE(1,999) Rtest
      CLOSE(UNIT=1,STATUS='KEEP')
      STOP
      RETURN
      END

************************************************************************
*======================================================================*
************************************************************************


      SUBROUTINE BRYDN1(N,Xini,Xfin,ROO,TAU,Grad,HssInv,Step)
************************************************************************
* The purpose of this routine is to perform the first iteration of the *
* Broyden optimization scheme.  Xini is the initial point (N dimension *
* vector).  Grad is the routine that returns the gradient with input   *
* N,Xini,"1D Array".  Step is the step-size used in the 5-point        *
* divided difference scheme used to find the Hessian matrix:           *
*               HssInv(j,i) = d (f_j) / dx_i                            *
*                       f_j = d (V) / dx_j                             *
* Xini is kept in this routine and Xfin is returned as the guess for   *
* continuation of the Broyden scheme (routine BRYDN2).                 *
* HssInv is a N x N double array that will contain the initial estimate* 
* of the INVERSE Hessian matrix.                                       * 
************************************************************************
      IMPLICIT NONE
      INTEGER N, i, j, k, ll
      EXTERNAL Grad
      DIMENSION Xini(1:N), Del(1:N), HssInv(1:N,1:N)
      DIMENSION Xhold(1:4), Xnew(1:N), GNew(1:N,1:4)
      DIMENSION DeltaX(1:4), Crap(1:N,1:1), Xfin(1:N)
      DIMENSION Step(1:N)
      DOUBLE PRECISION Xini, Del, Step, HssInv, Xfin
      DOUBLE PRECISION Xhold, Xnew, GNew, DeltaX
      DOUBLE PRECISION Pre, Crap, ROO, TAU
      DeltaX(1) = -2.0D0             ! Here we define some parameters
      DeltaX(2) = -1.0D0                  ! used in the 5 point divided difference.
      DeltaX(3) =  1.0D0
      DeltaX(4) =  2.0D0 
* Now we must construct an approximation to the Hessian
* matrix using a finite difference method.  We shall use the 
* five-point derivative method to find each matrix element.
      DO j=1, N
         Pre = (1.0D0/(12.0D0*Step(j)))
         DO k=1, 4
            Xhold(k) = Xini(j) + DeltaX(k) * Step(j)
            DO ll=1, N
               Xnew(ll) = Xini(ll)
               IF( ll .EQ. j) Xnew(ll) = Xhold(k)  ! Vary x_j to determine D_j,i.
               END DO
            CALL Grad(N,Xnew,ROO,TAU,Step,Del)  ! Here we find the gradient values 
            DO ll=1, N             ! used in the 5-point divided diff.
               GNew(ll,k) = Del(ll)
               END DO
            END DO
         DO i=1, N
            HssInv(j,i) = Pre * (                   ! Here we construct the Hessian
     &                   GNew(i,1) -               ! using the 5 point divided difference
     &                   8.0D0 * GNew(i,2) +       ! Method.
     &                   8.0D0 * GNew(i,3) - 
     &                   GNew(i,4) )
            END DO
         END DO
* The initial numerical Hessian has been constructed.
* We now INVERT the Hessian matrix.
      DO i=1, N
         Crap(i,1) = 0.0D0
         END DO
      CALL gaussj(HssInv,N,N,Crap,1,1) 
* HssInv now contains the INVERSE of the Hessian matrix.            
* We now start the initial Broyden step.
      CALL Grad(N,Xini,ROO,TAU,Step,Del)
      DO i=1, N
         Crap(i,1) = 0.0D0
         DO j=1, N
            Crap(i,1) = Crap(i,1) + HssInv(i,j) * Del(j)
            END DO
         Xfin(i) = Xini(i) - Crap(i,1)  ! We find the new point. 
         END DO
      RETURN
      END

************************************************************************
*======================================================================*
************************************************************************

      SUBROUTINE BRYDN2(N,Xnew,X0,ROO,TAU,HssInv,Grad,Step)
************************************************************************
* This routine performs one step of the Broyden extremum finding       *
* routine (analogous to the Secant Method in 1D, superlinear           *
* convergence).  Note that one step of the BRYDN1 routine must be run  *
* before calling this routine.                                         *
* The variables input into the routine are:                            *
*    -> N is the number of dimensions of the problem.                  *
*    -> Xnew is the previous estimate of the extremum.  Upon output    *
*       the new estimate of the extremum is put in this array.         *
*    -> X0 is the second previous routine.  Upon output, the old value *
*       of Xnew is placed in this array.                               *
*    -> HssInv is the guess of the inverse Hessian matrix corresponding*
*       to X0.  Upon output, this matrix is updated to correspond with *
*       Xnew.                                                          *
*    -> Grad is the routine that produces the gradient values via      *
*       Grad(N,X,DX).                                                  *
************************************************************************ 
      IMPLICIT NONE
      EXTERNAL Grad
      INTEGER N, i, j, k, ll
      DIMENSION HssInv(1:N,1:N), X0(1:N), Del0(1:N), Step(1:6)
      DIMENSION SS(1:N), YY(1:N), Delnew(1:N), Crap(1:N) 
      DIMENSION Crap2(1:N), HssInew(1:N,1:N), Xnew(1:N)
      DOUBLE PRECISION HssInv, X0, Del0, SS, YY, Delnew, Step
      DOUBLE PRECISION Crap, Crap2, HssInew, val, DFac, Xnew, ROO, TAU
* We first compute the gradient at the given guess points.
      CALL Grad(N,Xnew,ROO,TAU,Step,Delnew)
      CALL Grad(N,X0,ROO,TAU,Step,Del0)
* We create several vectors we shall use later.
      DO i=1, N
         YY(i) = Delnew(i) - Del0(i)
         SS(i) = Xnew(i) - X0(i)
         END DO
* Now we create the denominator for the secant method.
      DO j=1, N
         Crap(j) = 0.0D0
         DO k=1, N
            Crap(j) = Crap(j) - HssInv(j,k) * YY(k)
            END DO
         END DO
      DFac = 0.0D0
      DO j=1, N
         DFac = DFac - SS(j) * Crap(j)
         END DO
* Now we create another vector.
      DO j=1, N
         Crap2(j) = 0.0D0
         DO k=1, N 
            Crap2(j) = Crap2(j) + SS(k) * HssInv(k,j)
            END DO
         END DO
* We create the new inverse Hessian from the old inverse Hessian.
      DO i=1, N
         DO j=1, N
            val = (1.0D0/DFac) * (SS(j) + Crap(j)) * Crap2(i)
            HssInew(j,i) = HssInv(j,i) + val
            END DO
         END DO
* We now use the new matrix to determine the next guess for our 
* vector.
      DO i=1, N
         X0(i) = Xnew(i)
         DO j=1, N
            X0(i) = X0(i) - HssInew(i,j) * Delnew(j)
            END DO
         END DO
* Now we end the routine by putting the new vector in 
* Xnew and the old vector in X0.
      DO i=1, N
         val = X0(i)
         X0(i) = Xnew(i)
         Xnew(i) = val
         DO j=1, N
            HssInv(j,i) = HssInew(j,i)
            END DO
         END DO
      RETURN
      END


      SUBROUTINE gaussj(a,n,np,b,m,mp)
************************************************************************
* The purpose of this routine is to solve the linear equation:         *
*                    a * x = b                                         *
* Upon input, a is the n x n matrix, b is the n x m vector.            *
* np and mp are the physical dimensions of the arrays (set them equal  *
* to n and m unless you need them to be larger).                       *
* Upon output, a contains the inverse matrix of a and b contains the   *
* solution to the equation.                                            *
* This routine is useful for solving a set of linear equations OR      *
* inverting a matrix.                                                  *
* This routine was taken from NUMERICAL RECIPES by Press et al.        *
************************************************************************
      INTEGER m,mp,n,np,NMAX
      DOUBLE PRECISION a(np,np),b(np,mp)
      PARAMETER (NMAX=1000)
      INTEGER i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
      DOUBLE PRECISION big,dum,pivinv
      do 11 j=1,n
        ipiv(j)=0
11    continue
      do 22 i=1,n
        big=0.0D0
        do 13 j=1,n
          if(ipiv(j).ne.1)then
            do 12 k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big)then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              endif
12          continue
          endif
13      continue
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do 14 l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
14        continue
          do 15 l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
15        continue
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.0D0) stop 'singular matrix in gaussj'
        pivinv=1./a(icol,icol)
        a(icol,icol)=1.0D0
        do 16 l=1,n
          a(icol,l)=a(icol,l)*pivinv
16      continue
        do 17 l=1,m
          b(icol,l)=b(icol,l)*pivinv
17      continue
        do 21 ll=1,n
          if(ll.ne.icol)then
            dum=a(ll,icol)
            a(ll,icol)=0.0D0
            do 18 l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
18          continue
            do 19 l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
19          continue
          endif
21      continue
22    continue
      do 24 l=n,1,-1
        if(indxr(l).ne.indxc(l))then
          do 23 k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
23        continue
        endif
24    continue
      return
      END
