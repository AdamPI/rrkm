      SUBROUTINE GRADFULL(N,IC,Crap1,Crap2,Step,GRAD)
**************************************************************
* The purpose of this routine is to calculate the potential  *
* gradient of H2O2 in internal coordinates.  The input for   *
* this routine is:                                           *
*       -> IC is an array of the six internal coordinates.   *
*              IC(1)=ROH1, IC(2)=ROH2, IC(3)=ROO             *
*              IC(4)=AHOO1, IC(5)=AHOO2, IC(6)=TAU           *
*          Internal coordinates are defined according to     *
*          those in the Quack PES paper (J Chem Phys).       *
*       -> Step contains a step size for each individual     *
*          degree of freedom (since some of the coordinates  *
*          are lengths and some are angles).                 *
*       -> Crap is just some double precision number.  It    *
*          is not used.  It is a placeholder.                *
*       -> GRAD is an output array containing the gradient.  *
**************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag, N
      DIMENSION IC(1:6), Step(1:6), GRAD(1:6)
      DIMENSION ICHold(1:6), XX(1:4,1:3), Pot(1:4)
      DOUBLE PRECISION IC, Step, ROH1, ROH2
      DOUBLE PRECISION ICHold, Crap1, Crap2
      DOUBLE PRECISION XX, Pot, GRAD
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      iflag = 0
      DO i=1, 6
         ICHold(i) = IC(i)
         END DO
      DO i=1, 6
         ICHold(i) = IC(i) - 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4)
     &                ,ICHold(5),ICHold(6),XX)
         CALL hoohpot(XX,Pot(1),iflag)
         ICHold(i) = IC(i) - Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),ICHold(6),XX)
         CALL hoohpot(XX,Pot(2),iflag)
         ICHold(i) = IC(i) + Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),ICHold(6),XX)
         CALL hoohpot(XX,Pot(3),iflag)
         ICHold(i) = IC(i) + 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),ICHold(6),XX)
         CALL hoohpot(XX,Pot(4),iflag)
         DO j=1, 4
            Pot(j) = Pot(j) * cmi2har
            END DO
         GRAD(i) = (1.0D0/(12.0D0*Step(i))) * 
     &             ( Pot(1)-8.0D0*Pot(2)+8.0D0*Pot(3)-Pot(4) )
         ICHold(i) = IC(i)
         END DO
      RETURN
      END 
         
      
      SUBROUTINE GRADTAU(N,IC,TAU,Crap,Step,GRAD)
**************************************************************
* The purpose of this routine is to calculate the potential  *
* gradient of H2O2 in internal coordinates.  The input for   *
* this routine is:                                           *
*       -> IC is an array of the six internal coordinates.   *
*              IC(1)=ROH1, IC(2)=ROH2, IC(3)=ROO             *
*              IC(4)=AHOO1, IC(5)=AHOO2                      *
*          Internal coordinates are defined according to     *
*          those in the Quack PES paper (J Chem Phys).       *
*       -> Step contains a step size for each individual     *
*          degree of freedom (since some of the coordinates  *
*          are lengths and some are angles).                 *
*       -> Crap is just some double precision number.  It    *
*          is not used.  It is a placeholder.                *
* The torsion angle TAU is held frozen and is input          *
* separately from the other coordinates.                     *
*       -> GRAD is an output array containing the gradient.  *
**************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag, N
      DIMENSION IC(1:5), Step(1:5), GRAD(1:5)
      DIMENSION ICHold(1:5), XX(1:4,1:3), Pot(1:4)
      DOUBLE PRECISION IC, Step
      DOUBLE PRECISION TAU, ICHold
      DOUBLE PRECISION XX, Pot, Crap, GRAD
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      iflag = 0
      DO i=1, 5 
         ICHold(i) = IC(i)
         END DO
      DO i=1, 5
         ICHold(i) = IC(i) - 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),TAU,XX)
         CALL hoohpot(XX,Pot(1),iflag)
         ICHold(i) = IC(i) - Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),TAU,XX)
         CALL hoohpot(XX,Pot(2),iflag)
         ICHold(i) = IC(i) + Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),TAU,XX)
         CALL hoohpot(XX,Pot(3),iflag)
         ICHold(i) = IC(i) + 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ICHold(3),ICHold(4),
     &                 ICHold(5),TAU,XX)
         CALL hoohpot(XX,Pot(4),iflag)
         DO j=1, 4
            Pot(j) = Pot(j) * cmi2har
            END DO
         GRAD(i) = (1.0D0/(12.0D0*Step(i))) * 
     &             ( Pot(1)-8.0D0*Pot(2)+8.0D0*Pot(3)-Pot(4) )
         ICHold(i) = IC(i)
         END DO
      RETURN
      END 


      SUBROUTINE GRADROO(N,IC,Crap,ROO,Step,GRAD)
**************************************************************
* The purpose of this routine is to calculate the potential  *
* gradient of H2O2 in internal coordinates.  The input for   *
* this routine is:                                           *
*       -> IC is an array of the six internal coordinates.   *
*              IC(1)=ROH1, IC(2)=ROH2                        *
*              IC(4)=AHOO1, IC(5)=AHOO2, IC(6)=TAU           *
*          Internal coordinates are defined according to     *
*          those in the Quack PES paper (J Chem Phys).       *
*       -> Step contains a step size for each individual     *
*          degree of freedom (since some of the coordinates  *
*          are lengths and some are angles).                 *
*       -> Crap is just some double precision number.  It    *
*          is not used.  It is a placeholder.                *
* The O-O bond length ROO is held frozen and is input        *
* separately from the other coordinates.                     *
*       -> GRAD is an output array containing the gradient.  *
**************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag, N
      DIMENSION IC(1:5), Step(1:5), GRAD(1:5)
      DIMENSION ICHold(1:5), XX(1:4,1:3), Pot(1:4)
      DOUBLE PRECISION IC, Step, GRAD
      DOUBLE PRECISION ROO, ICHold, Crap
      DOUBLE PRECISION XX, Pot
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      iflag = 0
      DO i=1, 5
         ICHold(i) = IC(i)
         END DO
      DO i=1, 5
         ICHold(i) = IC(i) - 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),ICHold(5),XX)
         CALL hoohpot(XX,Pot(1),iflag)
         ICHold(i) = IC(i) - Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),ICHold(5),XX)
         CALL hoohpot(XX,Pot(2),iflag)
         ICHold(i) = IC(i) + Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),ICHold(5),XX)
         CALL hoohpot(XX,Pot(3),iflag)
         ICHold(i) = IC(i) + 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),ICHold(5),XX)
         CALL hoohpot(XX,Pot(4),iflag)
         DO j=1, 4
            Pot(j) = Pot(j) * cmi2har
            END DO
         GRAD(i) = (1.0D0/(12.0D0*Step(i))) * 
     &             ( Pot(1)-8.0D0*Pot(2)+8.0D0*Pot(3)-Pot(4) )
         ICHold(i) = IC(i)
         END DO
      RETURN
      END 



      SUBROUTINE GRADTAUROO(N,IC,TAU,ROO,Step,GRAD)
**************************************************************
* The purpose of this routine is to calculate the potential  *
* gradient of H2O2 in internal coordinates.  The input for   *
* this routine is:                                           *
*       -> IC is an array of the six internal coordinates.   *
*              IC(1)=ROH1, IC(2)=ROH2                        *
*              IC(3)=AHOO1, IC(4)=AHOO2                      *
*          Internal coordinates are defined according to     *
*          those in the Quack PES paper (J Chem Phys).       *
*       -> Step contains a step size for each individual     *
*          degree of freedom (since some of the coordinates  *
*          are lengths and some are angles).                 *
* The O-O bond length ROO is held frozen and is input        *
* separately from the other coordinates.                     *
*       -> GRAD is an output array containing the gradient.  *
**************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag, N
      DIMENSION IC(1:4), Step(1:4), GRAD(1:4)
      DIMENSION ICHold(1:4), XX(1:4,1:3), Pot(1:4)
      DOUBLE PRECISION IC, Step, GRAD
      DOUBLE PRECISION ROO, ICHold
      DOUBLE PRECISION XX, Pot, TAU
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      iflag = 0
      DO i=1, 4
         ICHold(i) = IC(i)
         END DO
      DO i=1, 4
         ICHold(i) = IC(i) - 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),TAU,XX)
         CALL hoohpot(XX,Pot(1),iflag)
         ICHold(i) = IC(i) - Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),TAU,XX)
         CALL hoohpot(XX,Pot(2),iflag)
         ICHold(i) = IC(i) + Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),TAU,XX)
         CALL hoohpot(XX,Pot(3),iflag)
         ICHold(i) = IC(i) + 2.0D0 * Step(i)
         CALL INT2CART(ICHold(1),ICHold(2),ROO,ICHold(3),
     &                 ICHold(4),TAU,XX)
         CALL hoohpot(XX,Pot(4),iflag)
         DO j=1, 4
            Pot(j) = Pot(j) * cmi2har
            END DO
         GRAD(i) = (1.0D0/(12.0D0*Step(i))) * 
     &             ( Pot(1)-8.0D0*Pot(2)+8.0D0*Pot(3)-Pot(4) )
         ICHold(i) = IC(i)
         END DO
      RETURN
      END 
