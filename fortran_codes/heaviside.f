      FUNCTION HEAVISIDE(XX)
************************************************************************
* This is the heaviside function that returns a value of 1 if the input*
* is positive and 0 if it is negative (returned as double precision).  *
************************************************************************
      IMPLICIT NONE
      DOUBLE PRECISION XX, HEAVISIDE
      IF( XX .GT. 0.0D0) THEN
          HEAVISIDE = 1.0D0
      ELSE 
          HEAVISIDE = 0.0D0
      END IF
      RETURN
      END
