* Mass weighting of the hessian.
      SUBROUTINE HESSMW(HESS)
************************************************************************
* This routine takes the cartesian hessian matrix for H2O2 and converts*
* it to the mass weighted hessian matrix.                              *
* The mass weighting is done in atomic units.                          *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, ii, jj
      DIMENSION HESS(1:4,1:3,1:4,1:3), mass(1:4)
      DOUBLE PRECISION HESS, mass
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
      DO i=1, 4
         DO j=1, 3
            DO ii=1, 4
               DO jj=1, 3
                  HESS(i,j,ii,jj) = HESS(i,j,ii,jj) / 
     &                  DSQRT(mass(i)*mass(ii))
                  END DO
               END DO
            END DO
         END DO
      RETURN 
      END 



* Mass weighting of cartesian.
      SUBROUTINE XMW(XX,XXmw)
************************************************************************
* This routine converts the cartesian coordinates of H2O2 to the mass  *
* weighted coordinates.  The cartesian coordinates are contained in the*
* input array XX(atom#,XYZ#) and the mass weighted coordinates are     *
* placed in the array XXmw(atom#,XYZ#).  The atom indexing is:         *
*                          H1-O3----O4-H2                              *
* The mass weights are in atomic units.                                *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j
      DIMENSION XX(1:4,1:3), mass(1:4), XXmw(1:4,1:3)
      DOUBLE PRECISION XX, mass, XXmw
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
      DO i=1, 4
         DO j=1, 3
            XXmw(i,j) = XX(i,j) * DSQRT(mass(i))
            END DO
         END DO
      RETURN
      END
