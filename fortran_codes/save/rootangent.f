      SUBROUTINE ROOTAN(XX,TANG)
************************************************************************
* The purpose of this routine is to calculate the tangent vector in    *
* the H2O2 molecule to the dissociation of the two (OH) fragments.     *
* The mode for which the tangent is calculated is the stretch          *
* between the two centers of masss of the two OH fragments.            *
* The input for this program is:                                       *
*         -> XX is the cartesian geometry of the H2O2 molecule where   *
*            the array is organized in the order (atom#,XYZ#) and      *
*            the atom indices are H1-O3--O4-H2.                        *
* The output is:                                                       *
*         -> TANG is the tangent vector to the fragment stretching     *
*            mode and is organized similarly to XX.                    *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j
      DIMENSION XX(1:4,1:3), TANG(1:4,1:3), mass(1:4)
      DIMENSION COM1(1:3), COM2(1:3), DCOM(1:3)
      DOUBLE PRECISION XX, TANG, COM1, COM2, DCOM
      DOUBLE PRECISION RDCOM, Normy, mass
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
* We initialize the tangent vector.
      DO i=1, 4
         DO j=1, 3
            TANG(i,j) = 0.0D0
            END DO
         END DO
* We compute the centers of mass for each fragment.
      RDCOM = 0.0D0
      DO i=1, 3
         COM1(i) = 0.0D0
         COM2(i) = 0.0D0
         COM1(i) = (mass(1)/(mass(1)+mass(3))) * XX(1,i) +
     &             (mass(3)/(mass(1)+mass(3))) * XX(3,i) 
         COM2(i) = (mass(2)/(mass(2)+mass(4))) * XX(2,i) +
     &             (mass(4)/(mass(2)+mass(4))) * XX(4,i)
         DCOM(i) = COM2(i)-COM1(i)
         RDCOM = RDCOM + DCOM(i)**2
         END DO
* Now we compute the unit vector in the direction of the stretch
* of the two OH fragments.
      RDCOM = DSQRT( RDCOM )
      DO i=1, 3
         DCOM(i) = DCOM(i) / RDCOM
         END DO
* Now we make the tangent vector.
      Normy = 0.0D0
      DO i=1, 3
         TANG(1,i) = -DCOM(i)
         TANG(3,i) = -DCOM(i)
         TANG(2,i) =  DCOM(i)
         TANG(4,i) =  DCOM(i)
         Normy=Normy+TANG(1,i)**2+TANG(2,i)**2+TANG(3,i)**2+TANG(4,i)**2
         END DO
      Normy = DSQRT( Normy )
* Now we normalize the tangent vector.
      DO i=1, 4
         DO j=1, 3
            TANG(i,j) = TANG(i,j) / Normy
            END DO
         END DO
      RETURN
      END 
