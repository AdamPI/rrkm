      SUBROUTINE INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
************************************************************************
* The purpose of this routine is to compute a cartesian geometry for   *
* H2O2 system given the internal coordinates.  The H2O2 system is:     *
*                           H1-O3-----O4-H2                            *
* and the internal coordinates are given according to those from the   *
* paper:  B. Kuhn, T.R. Rizzo, D. Luckhaus, M. Quack, M.A. Suhm, J.    *
*         Chem. Phys. 111, 2566 specifically on pg. 2569.              *
* The cartesian coordinates are output into the XX array in order of   *
* the indices shown above.                                             *
* * ALL ANGLES SHOULD BE INPUT IN RADIANS.                             *
************************************************************************
      IMPLICIT NONE
      DIMENSION XX(1:4,1:3)
      DOUBLE PRECISION XX, ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
* Define the oxygen coordinates.
      XX(3,1) = 0.0D0
      XX(3,2) = 0.0D0
      XX(3,3) = 0.0D0
      XX(4,1) = ROO
      XX(4,2) = 0.0D0
      XX(4,3) = 0.0D0
* Define the first hydrogen coordinate.
      XX(1,1) = ROH1 * DCOS( AHOO1 )
      XX(1,2) = ROH1 * DSIN( AHOO1 )
      XX(1,3) = 0.0D0
* Define the second hydrogen coordinate.
      XX(2,1) = ROO + ROH2 * DCOS( AHOO2 )
      XX(2,2) = ROH2 * DCOS( TAU ) * DSIN(AHOO2 )
      XX(2,3) = ROH2 * DSIN( TAU ) * DSIN(AHOO2 )
      RETURN
      END

      SUBROUTINE H2O2COM(XX)
************************************************************************
* The purpose of this routine is to put the H2O2 molecule in its       * 
* center of mass frame.  The input array XX contains the coordinates   *
* (cartesian) of the molecule calculated in the routine INT2CART.      *
* The input array (and where the output is placed) XX is given as      *
* (atom#,XYZ#), H1-O3---O4-H2.                                         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j
      DIMENSION XX(1:4,1:3), mass(1:4), COM(1:3)
      DOUBLE PRECISION XX, mass, COM, Mtot
* Below we give the masses of the H and O atoms in amu.
      DATA mass/1.007825035D0,1.007825035D0,15.994914635D0,
     &          15.994914635D0/
* Find the total mass.
      Mtot = 0.0D0
      DO i=1, 4
         Mtot = mass(i) + Mtot
         END DO
* We find the COM coordinates and set it as the origin.
      DO i=1, 3
         COM(i) = 0.0D0
         DO j=1, 4
            COM(i) = COM(i) + (mass(j)/Mtot) * XX(j,i)
            END DO
         DO j=1, 4
            XX(j,i) = XX(j,i) - COM(i)
            END DO
         END DO
      RETURN
      END   

      SUBROUTINE INERT(XX,II,flagCOM,flagROT)
************************************************************************
* The purpose of this routine is to compute the principle moments of   *
* inertia, and, if desired, rotate the geometry into the frame of the  *
* principle axes.                                                      *
* The input for this routine is:                                       *
*    -> XX contains the H2O2 geometry in an array (atom#,XYZ#).  If    *
*      flagCOM is set the geometry is set relative to the center of    *
*      mass.  If flagROT is set then it is set in the frame of the     *
*      principle axes.                                                 *
*    -> II are the principle moments of inertia, an array (1:3).       *
*    -> flagCOM=1 will set the H2O2 geometry relative to the center    *
*       of mass.                                                       *
*    -> flagROT=1 will set the H2O2 geometry to the principle axes     *
*       frame.                                                         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, k, NMAT1, NMAT2, MatZ, IErr, flagCOM, flagROT
      DIMENSION XX(1:4,1:3), II(1:3), mass(1:4), XXnew(1:4,1:3)
      DIMENSION IMAT(1:3,1:3), IAX(1:3,1:3), Crap1(1:3), Crap2(1:3)
      DOUBLE PRECISION XX, II, mass, IMAT, IAX, Crap1, Crap2, XXnew
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
      IF ( flagCOM .EQ. 1 )  CALL H2O2COM(XX)
* First, we initialize the arrays.
      DO i=1, 3
         II(i) = 0.0D0
         Crap1(i) = 0.0D0
         Crap2(i) = 0.0D0
         DO j=i, 3
            IMAT(i,j) = 0.0D0
            IMAT(j,i) = IMAT(i,j)
            IAX(i,j) = 0.0D0
            IAX(j,i) = IAX(i,j)
            END DO
         END DO
* We construct the inertial matrix taking advantage of symmetry.
      DO i=1, 4
         IMAT(1,1) = IMAT(1,1) + mass(i) * (XX(i,2)**2 + XX(i,3)**2)
         IMAT(2,2) = IMAT(2,2) + mass(i) * (XX(i,1)**2 + XX(i,3)**2)
         IMAT(3,3) = IMAT(3,3) + mass(i) * (XX(i,1)**2 + XX(i,2)**2)
         IMAT(2,1) = IMAT(2,1) - mass(i) * XX(i,2) * XX(i,1)
         IMAT(3,1) = IMAT(3,1) - mass(i) * XX(i,3) * XX(i,1)
         IMAT(3,2) = IMAT(3,2) - mass(i) * XX(i,3) * XX(i,2)
         END DO
      IMAT(1,2) = IMAT(2,1)
      IMAT(1,3) = IMAT(3,1)
      IMAT(2,3) = IMAT(3,2)
* We diagonalize the inertia tensor to find the principle axes 
* and moments.
      NMAT1 = 3
      NMAT2 = 3
      MatZ=1
      IErr = 0
      CALL rs(NMAT1,NMAT2,IMAT,II,MatZ,IAX,Crap1,Crap2,IErr)
* Now we rotate the geometry into the principle axis frame, if
* we set flagROT=1.
      IF ( flagROT .EQ. 1 ) THEN
          DO i=1, 4
             DO j=1, 3
                XXnew(i,j) = 0.0D0
                DO k=1, 3
                   XXnew(i,j) = XXnew(i,j) + IAX(k,j)*XX(i,k)
                   END DO
                END DO
             DO j=1, 3
                XX(i,j) = XXnew(i,j)
                END DO
             END DO
      END IF
      RETURN
      END
