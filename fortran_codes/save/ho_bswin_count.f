* Routine for calling.
      SUBROUTINE BSCOUNTER(Emax,NEmax,Nfreq,Freq,DeltaE,
     &                     EGrid,SoS,DoS)
************************************************************************
* This routine is the Beyer-Swinehart algorithm method as extended by  *
* Stein and Rabinovitch in:                                            *
*   S.E. Stein and B.S. Rabinovitch, J. Chem. Phys., 58, 2438 (1973).  *
*                                                                      *
* IMPORTANT:                                                           *
*     ** THIS ROUTINE IS ONLY FOR HARMONIC OSCILLATOR DEGREES OF       *
*        FREEDOM, BUT CAN EASILY BE EXTENDED.                          *
*     ** INPUT NEmax MUST MUST MUST SATISFY CONDITION:                 *
*             NEMax=INT(Emax/DeltaE) + 1                               *
*        USER SHOULD SET THIS IN THE CALLING ROUTINE.                  *
*                                                                      *
* Argument types:                                                      *
*     INTEGERS :  NEmax, Nfreq                                         *
*     DOUBLE PRECISION  : Emax, DeltaE                                 *
*     1D DOUBLE PRECISION ARRAYS: Freq*(Nfreq), EGrid(NEmax),          *
*                                 SoS(NEmax), DoS(NEmax)               *
* The input to this routine is:                                        *
*    -> Energy value Emax in kcal/mol defining maximum energy          *
*       considered.                                                    *
*    -> NEmax, SEE NOTE ABOVE, number of energy points in grid.        *
*    -> Nfreq, number of harmonic degrees of freedom.                  *
*    -> Freq is array of size Nfreq containing the harmonic frequencies*
*       in units of cm**-1.                                            *
*    -> DeltaE the energy grain size for the method in cm**-1.         *
*       Good choices are usually between 1 and 100 cm**-1 depending on *
*       desired accuracy and speed.                                    *
*                                                                      *
* On output:                                                           *
*    -> EGrid is an array of size NEmax that will contain the energy   *
*       grid in kcal/mol.                                              *
*    -> SoS is an array containing the sum of states for the system    *
*       at each energy.                                                *
*    -> DoS is an array containing the density of states for the       *
*       system at each energy ( in units of (kcal/mol)**-1) ).         * 
************************************************************************
      IMPLICIT NONE 
      INTEGER NEmax, Nfreq, i, j, k, NMAX, NRdof
      PARAMETER(NMAX=20000)     ! Maximum number of quantum of 1 DOF.
      DIMENSION NoS(1:NEmax), Freq(1:Nfreq), Rdof(1:NMAX)
      DIMENSION SoS(1:NEmax), DoS(1:NEmax), EGrid(1:NEmax)
      INTEGER Rdof
      DOUBLE PRECISION Rtest, Rrem, NoS, En, kcal2har
      DOUBLE PRECISION Emax, DeltaE, SoS, Freq, cmi2har
      DOUBLE PRECISION DoS, EGrid
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      PARAMETER(kcal2har=1.0D0/627.509469D0)
*=====================================================*
*++++++++++++PRELIMINARY UNIT CONVERSIONS+++++++++++++*
*=====================================================*
* Change the energies to hartree.
      DeltaE = DeltaE * cmi2har
      Emax = Emax * kcal2har
* First, we are recieving the frequencies in cm**-1, so 
* we convert over to hartree.
      DO i=1, Nfreq
         Freq(i) = Freq(i) * cmi2har   ! Convert HO to hartrees.
         END DO
*=====================================================*
*++++++++++++++ INITIALIZE ARRAYS ++++++++++++++++++++*
*=====================================================*
* First we initialize the number of states array, before the 
* counting begins.
      DO i=1, NEmax
         SoS(i) = 0.0D0
         IF( i .EQ. 1 ) THEN
            NoS(i) = 1.0D0
         ELSE 
            NoS(i) = 0.0D0
            END IF
         END DO
*=====================================================*
*++++++++++++++ STATE COUNTING PROCEDURE +++++++++++++*
*=====================================================*
* Now we may determine the number of states by folding in 
* one vibrational degree of freedom at a time.
      DO i=1, Nfreq
* First we prepare the degree of freedom for the count.
         NRdof = INT( Emax / Freq(i) )     ! Determine max quanta needed.
         IF( NRdof .GT. NMAX ) STOP 'MAKE NMAX BIGGER IN COUNT!'
         DO j=1, NRdof
            Rtest = ( DBLE(j) * Freq(i) ) / DeltaE
            Rrem = Rtest - DBLE(INT(Rtest))
            IF ( Rrem .GE. 0.50D0 ) THEN   ! Determine number of quanta in
               Rdof(j) = INT( Rtest ) + 1  ! corresponding to element of
            ELSE                           ! number of states array.
               Rdof(j) = INT( Rtest ) 
               END IF
            END DO
* Now we use the Beyer-Swineheart count to fold in the degree of freedom.
         CALL BSWIN(NEmax,NRdof,NoS,Rdof)
         END DO
*=====================================================*
*++++++++++++++ SUM AND DENSITY OF STATES ++++++++++++*
*=====================================================*
* Now we find the sum of states.
      DO i=1, NEmax
         DO j=1, i
            SoS(i) = SoS(i) + NoS(j)     ! Creeat the sum of states.
            END DO
         END DO
* Final conversion of units and calculation of density of states.
      DO i=1, NEmax
         EGrid(i) = DBLE(i-1) * DeltaE / kcal2har ! Energy grid.
         DoS(i) =  NoS(i)/DeltaE   * kcal2har     ! Create density of states.
         END DO
* Undo changes in unit of frequency and Emax, in case user
* will use again.
      Emax = Emax / kcal2har
      DeltaE = DeltaE / cmi2har
      DO i=1, Nfreq
         Freq(i) = Freq(i) / cmi2har   ! Convert HO to hartrees.
         END DO
      RETURN
      END


* BELOW IS A ROUTINE USED BY ROUTINE ABOVE.
************************************************************************
* ****************** SINGLE BEYER-SWINEHART COUNT **********************
* *******************   USED IN ROUTINE ABOVE  *************************
* **********************  NO NEED DO MODIFY  ***************************
************************************************************************

      SUBROUTINE BSWIN(NMax,Ndof,DGen,Rdof)
************************************************************************
* This is the subroutine that folds the states for a particular        *
* degree of freedom into the number of state count.                    *
* The input to the program is:                                         *
*     -> NMax giving the total size of the energy grid.                *
*     -> Ndof is the number of quanta of the degrees of freedom        *
*        possible given the size of the energy grid.                   *
*     -> DGen is the number of states array to be modified.            *
*     -> Rdof is an array specifying the positions on the energy grid  *
*        that the individual degree of freedom states lie.             *
************************************************************************
      IMPLICIT NONE
      INTEGER NMax, i, j, k, Ndof
      DIMENSION DGen(1:NMax), DGTemp(1:NMax), Rdof(1:Ndof)
      INTEGER Rdof
      DOUBLE PRECISION DGen, DGTemp
* We initialize the temporary degeneracy array.
      DO i=1, NMax
         DGTemp(i) = DGen(i) 
         END DO             
* Now we fold in the states of the degrees of freedom into
* the temporary degeneracy array.
      DO k=1, Ndof
         DO i=1, Nmax-Rdof(k)
            DGTemp(Rdof(k)+i) = DGTemp(Rdof(k)+i) + DGen(i)
            END DO
         END DO
* The states for the degree of freedom are now folded in. We
* transfer the results to the permanent array.
      DO k=1, NMax
         DGen(k) = DGTemp(k)
         END DO
      RETURN
      END

