      SUBROUTINE COUNTER(Emax,NEmax,Nfreq,Freq,DeltaE,SoS,DoS)
      IMPLICIT NONE 
      INTEGER NEmax, Nfreq, i, j, k, NMAX, NRdof
      PARAMETER(NMAX=20000)
      DIMENSION NoS(1:NEmax), Freq(1:Nfreq), Rdof(1:NMAX)
      DIMENSION SoS(1:NEmax), DoS(1:NEmax)
      INTEGER Rdof
      DOUBLE PRECISION Rtest, Rrem, NoS, En, kcal2har
      DOUBLE PRECISION Emax, DeltaE, SoS, Freq, cmi2har
      DOUBLE PRECISION Energy, DoS
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      PARAMETER(kcal2har=1.0D0/627.5095D0)
* First we initialize the number of states array, before the 
* counting begins.
      DO i=1, NEmax
         SoS(i) = 0.0D0
         IF( i .EQ. 1 ) THEN
            NoS(i) = 1.0D0
         ELSE 
            NoS(i) = 0.0D0
            END IF
         END DO
* First, we are recieving the frequencies in cm**-1, so 
* we convert over to hartree.
      DO i=1, Nfreq
         Freq(i) = Freq(i) * cmi2har   ! Convert HO to hartrees.
         END DO
* Now we may determine the number of states by folding in 
* one vibrational degree of freedom at a time.
      DO i=1, Nfreq
* First we prepare the degree of freedom for the count.
         NRdof = INT( Emax / Freq(i) )     ! Determine max quanta needed.
         DO j=1, NRdof
            Rtest = ( DBLE(j) * Freq(i) ) / DeltaE
            Rrem = Rtest - DBLE(INT(Rtest))
            IF ( Rrem .GE. 0.50D0 ) THEN   ! Determine number of quanta in
               Rdof(j) = INT( Rtest ) + 1  ! corresponding to element of
            ELSE                           ! number of states array.
               Rdof(j) = INT( Rtest ) 
               END IF
            END DO
* Now we use the Beyer-Swineheart count to fold in the degree of freedom.
         CALL BSWIN(NEmax,NRdof,NoS,Rdof)
         END DO
* Now we find the sum of states.
      DO i=1, NEmax
         DO j=1, i
            SoS(i) = SoS(i) + NoS(j)     ! Creeat the sum of states.
            END DO
         END DO
* Last, we print our results.
      DO i=1, NEmax
         Energy(i) = DBLE(i-1) * DeltaE  ! Energy grid.
         DoS(i) =  NoS(i)/DeltaE         ! Create density of states.
         END DO
      RETURN
      END

