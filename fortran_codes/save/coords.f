      FUNCTION RBONDL(Natom,XX,At1,At2)
**************************************************************************
* This function calculates the bond length between two atoms specified by*
* indice numbers 'At1' and 'At2'.  The input is the number of atoms in   *
* the molecule, 'Natom', the cartesian coordinates array 'XX(atom,XYZ)', *
* and the indices of the atoms in the array.                             *
**************************************************************************
      IMPLICIT NONE
      INTEGER i, Natom, At1, At2
      DIMENSION XX(1:Natom,1:3)
      DOUBLE PRECISION XX, RBONDL
      RBONDL=0.0D0
      RBONDL=DSQRT( (XX(At2,1)-XX(At1,1))**2 +(XX(At2,2)-XX(At1,2))**2+
     &              (XX(At2,3)-XX(At1,3))**2 ) 
      RETURN 
      END 
 
      FUNCTION VBONDL(Natom,XX,VV,At1,At2)
************************************************************************
* This routine calculates the velocity in the bond stretch coordinate. *
* The input for this routine is:                                       *
*       - Natom is the number of atoms in the molecule.                *
*       - XX and VV are the (atom,XYZ) arrays containing the cartesian *
*         positions and velocities of the atoms in the molecule.       *
*       - At1 and At2 are the indices of the atoms in the arrays of the*
*         atoms in the bond of interest.                               *
************************************************************************
      IMPLICIT NONE
      INTEGER i, Natom, At1, At2
      DIMENSION XX(1:Natom,1:3), VV(1:Natom,1:3), vvec(1:3)
      DIMENSION runit(1:3)
      DOUBLE PRECISION XX, VV, VBONDL, runit, DotProd, rmag
      DOUBLE PRECISION vvec
      rmag=0.0D0
      DO i=1, 3
         rmag = rmag + ( XX(At2,i)-XX(At1,i) ) **2
         runit(i) = XX(At2,i)-XX(At1,i)
         vvec(i) = VV(At2,i) - VV(At1,i)
         END DO
      rmag = DSQRT( rmag )
      DotProd = 0.0D0
      DO i=1, 3
         runit(i) = runit(i) / rmag
         DotProd = DotProd + runit(i) * vvec(i)
         END DO
      VBONDL = DotProd
      RETURN
      END 

* All angles are output in radians.  In the following two routines.
      FUNCTION BANGLE(Natom,XX,At1,At2,At3)
**************************************************************************
* This routine calculates the angle between two bonds that share a common*
* atom (bond angle).  The angle is between At1-At2 and At3-At2.          *
* The input for the function is:                                         *
*      - Natom is the number of atoms in the molecule.                   *
*      - XX is the cartesian coordinate array (atom,XYZ) for the atoms   *
*        in the molecule.                                                *
*      - At1, At2, and At3 are the indices of the atoms in the geometry  *
*        arrays involved in bond angle, At2 is the vertice atom.         *
**************************************************************************
      IMPLICIT NONE
      INTEGER i, Natom, At1, At2, At3
      DIMENSION XX(1:Natom,1:3), runit12(1:3), runit32(1:3)
      DOUBLE PRECISION BANGLE, runit12, runit32, rmag12, rmag32
      DOUBLE PRECISION DotProd, XX
      rmag12=0.0D0
      rmag32=0.0D0
      DO i=1, 3
         rmag12 = rmag12 + (XX(At1,i)-XX(At2,i))**2
         rmag32 = rmag32 + (XX(At3,i)-XX(At2,i))**2
         runit12(i) = XX(At1,i)-XX(At2,i)
         runit32(i) = XX(At3,i)-XX(At2,i)
         END DO
      rmag12=DSQRT(rmag12)
      rmag32=DSQRT(rmag32)
      DotProd=0.0D0
      DO i=1, 3
         runit12(i) = runit12(i) / rmag12
         runit32(i) = runit32(i) / rmag32
         DotProd = DotProd + runit12(i) * runit32(i)
         END DO
      BANGLE = DACOS(DotProd)
      RETURN 
      END

      FUNCTION DANGLE(Natom,XX,At1,At2,At3,At4)
**************************************************************************
* This routine calculates the dihedral angle formed between two bonds    *
* separated by a third bond. It calculates in the system A-B----C-D, the *
* angle formed between bond B-A and C-D looking down bond B-C.           *
*  The input to the function is:                                         *
*       - Natom is the number of atoms in the molecule.                  *
*       - XX is the cartesian coordinates of the atoms (atom,XYZ).       *
*       - At1-At2----At3-At4 are the indices of the atoms in geometry    *
*         array for those atoms which form the dihedral angle.  The      *
*         terminal atoms are the first and last of the At's.             * 
**************************************************************************
      IMPLICIT NONE
      INTEGER i, Natom, At1, At2, At3, At4
      DIMENSION XX(1:Natom,1:3), un12(1:3), un23(1:3), un34(1:3)
      DIMENSION crss1223(1:3), crss2334(1:3)
      DOUBLE PRECISION XX, un12, un23, un34, crss1223, crss2334
      DOUBLE PRECISION DANGLE, rmag12, rmag23, rmag34
      DOUBLE PRECISION Dot1, Dot2, Pi
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      rmag12=0.0D0
      rmag23=0.0D0
      rmag34=0.0D0
      DO i=1, 3
         rmag12 = rmag12 + ( XX(At2,i)-XX(At1,i) ) **2
         rmag23 = rmag23 + ( XX(At3,i)-XX(At2,i) ) **2
         rmag34 = rmag34 + ( XX(At4,i)-XX(At3,i) ) **2
         un12(i) = XX(At2,i)-XX(At1,i)
         un23(i) = XX(At3,i)-XX(At2,i)
         un34(i) = XX(At4,i)-XX(At3,i)
         END DO
      rmag12 = DSQRT(rmag12)
      rmag23 = DSQRT(rmag23)
      rmag34 = DSQRT(rmag34)
      DO i=1, 3
         un12(i) = un12(i) / rmag12
         un23(i) = un23(i) / rmag23
         un34(i) = un34(i) / rmag34
         END DO
      CALL CRSSPRD(un12,un23,crss1223)
      CALL CRSSPRD(un23,un34,crss2334)
      Dot1=0.0D0
      Dot2=0.0D0
      DO i=1, 3
         Dot1 = Dot1 + un12(i) * crss2334(i)
         Dot2 = Dot2 + crss1223(i) * crss2334(i)
         END DO
      DANGLE = ATAN2(Dot1,Dot2) 
      IF( DANGLE .LT. 0.0D0) DANGLE = DANGLE + 2.0D0*Pi
      RETURN
      END 

      SUBROUTINE CRSSPRD(VEC1,VEC2,CRSS)
**************************************************************************
* This routine finds the cross products of two 3D vectors, VEC1 and VEC2,*
* and returns the result as the vector CRSS.                             *
**************************************************************************
      IMPLICIT NONE
      DIMENSION VEC1(1:3), VEC2(1:3), CRSS(1:3)
      DOUBLE PRECISION VEC1, VEC2, CRSS
      CRSS(1) = ( VEC1(2)*VEC2(3)-VEC1(3)*VEC2(2) )
      CRSS(2) = ( VEC1(3)*VEC2(1)-VEC2(3)*VEC1(1) )
      CRSS(3) = ( VEC1(1)*VEC2(2)-VEC1(2)*VEC2(1) )
      RETURN 
      END
      
