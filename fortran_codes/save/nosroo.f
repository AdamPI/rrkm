      SUBROUTINE NOSROO(ROO,ROH1,ROH2,AHOO1,AHOO2,Emin,Emax,DelE,
     &                  NEmax,NTAU)
************************************************************************
* The purpose of this routine is to determine the necessary parameters *
* and reduced sum of states at each value of the torsion angle in H2O2.*
* The reduced sum of states accounts for all degrees of freedom but    *
* torsion and the ROO stretch.                                         *
* The input to this routine is:                                        *
*        -> ROO is the O--O bond distance in bohr.                     *
*        -> NTAU is the number of torsion angle points to be used in   *
*           the grid.  The torsion angle will be allowed to vary       *
*           between 0 and 2*Pi, and actual points will be at Gauss-    *
*           Legendre points.                                           *
*        -> Emin is the absolute energy of the reactants minimum       *
*           energy geometry input in hartrees.                         *
*    The following quantities should be input as guesses, but will be  *
*    optimized.                                                        *
*        -> ROH1, ROH2 are the OH bond lengths in bohr                 *
*        -> AHOO1 and AHOO2 are the angles between the OH bonds and    *
*           the axis defined by the O3--->O4 bond length in radians.   *
*    The following parameters are parameters for the state count.      *
*        -> DelE is the energy grain in cm**-1 (between 1 and 100      *
*           cm**-1 is best choice.                                     *
*        -> Emax is the maximum energy at which state count shall be   *
*           performed (should be in kcal mol**-1).                     *
*        -> NEmax is the number of points in the energy grid and       *
*           should be determined by INT( Emax/DelE) + 1, where Emax    *
*           and DelE have been put in the same units.                  *
************************************************************************    
      IMPLICIT NONE
      INTEGER i, j, NEmax, NTAU, Tester
      DIMENSION XX(1:4,1:3), TAUpt(1:NTAU), TAUwght(1:NTAU)
      DIMENSION FreqIn(1:12), FrqOut(1:4), II(1:3), mass(1:4)
      DIMENSION hessian(1:4,1:3,1:4,1:3) 
      DIMENSION DoS(1:NEmax), SoS(1:NEmax), Egrid(1:NEmax)
      DOUBLE PRECISION XX, EOpt, Emin, TAUpt, TAUwght, mass
      DOUBLE PRECISION ROO, ROH1, ROH2, AHOO1, AHOO2, TAU
      DOUBLE PRECISION II, IITAU, hessian, HesStep, DoS, SoS
      DOUBLE PRECISION TAUmin, TAUmax, Pi, FreqIn, FrqOut
      DOUBLE PRECISION DelE, Emax, TOL, Egrid, ZPE
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      DOUBLE PRECISION kcal2har
      PARAMETER(kcal2har=1.0D0/627.509469D0)
      DOUBLE PRECISION cmi2har
      CHARACTER*75 FileNam
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
      INCLUDE './incfile.inc'
* We set tolerance for the optimization.
      TOL = 1.0D-13
* We open the files that we will write the data to, in order to
* save dynamic memory.
      WRITE(FileNam,999)
      OPEN(UNIT=1,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,998)
      OPEN(UNIT=2,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,997)
      OPEN(UNIT=3,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,996)
      OPEN(UNIT=4,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,995)
      OPEN(UNIT=5,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,994)
      OPEN(UNIT=6,STATUS='NEW',FILE=FileNam)
      WRITE(FileNam,993)
      OPEN(UNIT=7,STATUS='NEW',FILE=FileNam)
* We shall use Legendre quadrature for the integration.
      TAUmin = 0.0D0
      TAUmax = 2.0D0 * Pi
      CALL gauleg(TAUmin,TAUmax,TAUpt,TAUwght,NTAU)
* Now we loop through these points to obtain necessary parameters.
      DO i=1, NTAU
* Minimize the geometry at frozen ROO and TAU.
         CALL ROOTAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAUpt(i),EOpt,TOL)
* Record the energy relative to the reactant minimum ( V(TAU) value).
         EOpt = EOpt - Emin    ! Find energy relative to reactant minimum.
         WRITE(1,899) EOpt
* Find the torsion moment of inertia for this value of ROO and TAU.
         IITAU = mass(1) * (ROH1*DSIN(AHOO1))**2 + 
     &           mass(2) * (ROH2*DSIN(AHOO2))**2   ! Torsion inertia.
         WRITE(3,899) IITAU
* Determine the external principle moments of intertia for this value of TAU.
         CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAUpt(i),XX)
         CALL INERT(XX,II,1,1)                     ! External inertia.
         WRITE(2,897) II(1), II(2), II(3)
* Now we must find the density of states for the modes NOT included in ROO
* and TAU.
* Generate the HESSIAN matrix.
         HesStep = 0.010D0     ! Hessian step size 0.010 bohr for divided diff.
         CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAUpt(i),HesStep,XX,
     &             hessian,II)
* Now perform projection.
         CALL PROJECT(XX,hessian,II,1,1,FreqIn)
* Put into little array to keep things tidy.
         ZPE = 0.0D0
         DO j=1, 4
            FrqOut(j) = FreqIn(j)
            ZPE = ZPE + 0.50D0 * FreqIn(j)
            END DO
         ZPE = ZPE * cmi2har     ! Print oscillator ZPE in hartree.
         WRITE(5,898) NINT(FreqIn(1)), NINT(FreqIn(2)), 
     &                NINT(FreqIn(3)), NINT(FreqIn(4))
         WRITE(7,899) ZPE
* Now we do the sum of states.
* The input NEmax must satisfy NEmax = INT( Emax/DelE ) + 1.
         Tester = INT((Emax/DelE) * (kcal2har/cmi2har))+1-NEmax
         IF( NEmax .NE. 0 ) STOP 'NEmax in NOSROO not adding up!'
         CALL BSCOUNTER(Emax,NEmax,4,FrqOut,DelE,EGrid,SoS,DoS) 
         DO j=1, NEmax
            WRITE(4,899) SoS(j)
            END DO
         END DO
      DO i=1, NEmax
         WRITE(6,899) Egrid(j) * kcal2har  ! Write in hartree.
         END DO
      CLOSE(UNIT=1,STATUS='KEEP')
      CLOSE(UNIT=2,STATUS='KEEP')
      CLOSE(UNIT=3,STATUS='KEEP')
      CLOSE(UNIT=4,STATUS='KEEP')
      CLOSE(UNIT=5,STATUS='KEEP')
      CLOSE(UNIT=6,STATUS='KEEP')
      CLOSE(UNIT=7,STATUS='KEEP')
      RETURN
      END 
