      SUBROUTINE ROO2T(XX,TANG)
      IMPLICIT NONE
      INTEGER i, j
      DIMENSION XX(1:4,1:3), TANG(1:4,1:3)
      DIMENSION UV(1:3)
      DOUBLE PRECISION XX, TANG, UV, ROO
      ROO = 0.0D0
      DO i=1, 3
         UV(i) = XX(4,i) - XX(3,i)
         ROO = ROO + UV(i)**2
         END DO
      ROO = DSQRT(ROO)
      DO i=1, 3
         UV(i) = UV(i) / ROO
         END DO
      DO i=1, 4
         DO j=1, 3
            TANG(i,j) = 0.0D0
            END DO 
         END DO
      DO i=1, 3
         TANG(3,i) = -UV(i)
         TANG(4,i) =  UV(i)
         END DO
      RETURN
      END 
      
