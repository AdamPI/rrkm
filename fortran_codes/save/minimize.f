      SUBROUTINE FULLMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,EOpt,TOL)
************************************************************************
* The purpose of this routine is to minimize the H2O2 molecule with    *
* the amoeba routine.  Some liberties have been taken to design the    *
* initial simplex points for the ameoba (images have been generated    *
* by taking the initial guess and perturbing each of the coordinates   *
* by an amount specified by the Step array.                            *
* All input and output should be in radians and atomic units!!!        *
* The input into this routine is:                                      *
*     -> ROH1 is the first OH bond length in H2O2.                     *
*     -> ROH2 is the second OH bond length in H2O2.                    *
*     -> AHOO1 is the angle between the first OH bond and the OO bond. *
*     -> AHOO2 is the angle between the second OH bond and the OO bond.*
*     -> TAU is the H2O2 dihedral angle.                               *
*     **** The coordinates above are output as their optimized values. *
*     -> TOL is the tolerance for the energy change that is used to    *
*        determine if the potential is optimized.                      *
* The output from this routine:                                        *
*     -> The coordinates are returned in their optimized values.       *
*     -> EOpt is the potential energy of the output structure.         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag
      DIMENSION IC(1:7,1:6), PEn(1:7), XX(1:4,1:3), Step(1:6)
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU, Step
      DOUBLE PRECISION IC, TOL, XX, PEn, PottyOut, EOpt
      DOUBLE PRECISION cmi2har, deg2rad, Pi, FULLFUNK
      EXTERNAL FULLFUNK
      INTEGER Ncoor, Nimage, Niter, Ndim
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(deg2rad=Pi/180.0D0)
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we define initial step-sizes for each coordinate.
      Step(1) = 0.10D0 
      Step(2) = 0.12D0
      Step(3) = 0.15D0
      Step(4) = 3.0D0 * deg2rad
      Step(5) = 3.2D0 * deg2rad
      Step(6) = 5.0D0 * deg2rad
* First, we put the initial coordinates into the optimation array.
      IC(1,1) = ROH1
      IC(1,2) = ROH2
      IC(1,3) = ROO
      IC(1,4) = AHOO1
      IC(1,5) = AHOO2
      IC(1,6) = TAU
* We find the energy of this geometry.
      iflag=0
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      PEn(1) = PottyOut * cmi2har
* Now, we generate six other images needed by alterning each coordinate,
* by a step-size.
      DO i=1, 6
         DO j=1, 6
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),IC(i+1,3),
     &                 IC(i+1,4),IC(i+1,5),IC(i+1,6),XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 7
      Ncoor = 6
      Ndim = 6
      CALL amoeba(IC,PEn,Nimage,Ncoor,Ndim,TOL,FULLFUNK,Niter)
* Let's do restart from the minimum geometry just to make sure
* our amoeba is behaving.
      DO i=1, 6
         DO j=1, 6
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),IC(i+1,3),
     &                 IC(i+1,4),IC(i+1,5),IC(i+1,6),XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 7
      Ncoor = 6
      Ndim = 6
      CALL amoeba(IC,PEn,Nimage,Ncoor,Ndim,TOL,FULLFUNK,Niter)
* Now, we retrieve the optimized geometry and energy.
      ROH1 = IC(1,1)
      ROH2 = IC(1,2)
      ROO  = IC(1,3)
      AHOO1= IC(1,4)
      AHOO2= IC(1,5)
      TAU  = IC(1,6)
      EOpt = PEn(1)
      RETURN
      END 

* Frozen O--O bond length.
      SUBROUTINE ROOMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,EOpt,TOL)
************************************************************************
* The purpose of this routine is to minimize the H2O2 molecule with    *
* the amoeba routine.  Some liberties have been taken to design the    *
* initial simplex points for the ameoba (images have been generated    *
* by taking the initial guess and perturbing each of the coordinates   *
* by an amount specified by the Step array.                            *
* In this routine the ROO, the bond length between the two oxygens, is *
* held constant in the optimization while all other coordinates are    *
* optimized.  At the end, the ROO is the same as its input value!.     *
* All input and output should be in radians and atomic units!!!        *
* The input into this routine is:                                      *
*     -> ROH1 is the first OH bond length in H2O2.                     *
*     -> ROH2 is the second OH bond length in H2O2.                    *
*     -> AHOO1 is the angle between the first OH bond and the OO bond. *
*     -> AHOO2 is the angle between the second OH bond and the OO bond.*
*     -> TAU is the H2O2 dihedral angle.                               *
*     **** The coordinates above are output as their optimized values. *
*     -> TOL is the tolerance for the energy change that is used to    *
*        determine if the potential is optimized.                      *
* The output from this routine:                                        *
*     -> The coordinates are returned in their optimized values.       *
*        except for the ROO, which is a frozen coordinate.             *
*     -> EOpt is the potential energy of the output structure.         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag
      DIMENSION IC(1:6,1:5), PEn(1:6), XX(1:4,1:3), Step(1:5)
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU, Step
      DOUBLE PRECISION IC, TOL,  XX, PEn, PottyOut, EOpt
      DOUBLE PRECISION cmi2har, deg2rad, Pi, ROOFUNK
      EXTERNAL ROOFUNK
      INTEGER Ncoor, Nimage, Niter, Ndim
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(deg2rad=Pi/180.0D0)
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we define initial step-sizes for each coordinate.
      Step(1) = 0.10D0 
      Step(2) = 0.12D0
      Step(3) = 2.0D0 * deg2rad
      Step(4) = 2.2D0 * deg2rad
      Step(5) = 2.0D0 * deg2rad
* First, we put the initial coordinates into the optimation array.
      IC(1,1) = ROH1
      IC(1,2) = ROH2
      IC(1,3) = AHOO1
      IC(1,4) = AHOO2
      IC(1,5) = TAU
* We find the energy of this geometry.
      iflag=0
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      PEn(1) = PottyOut * cmi2har
* Now, we generate six other images needed by alterning each coordinate,
* by a step-size.
      DO i=1, 5 
         DO j=1, 5
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,
     &                 IC(i+1,3),IC(i+1,4),IC(i+1,5),XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 6
      Ncoor = 5
      Ndim = 5
      CALL amoebar(IC,PEn,Nimage,Ncoor,Ndim,TOL,ROOFUNK,ROO,Niter)
* Let's do a restart from the minimum geometry just to make
* sure our amoeba was well behaved.
      DO i=1, 5 
         DO j=1, 5
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,
     &                 IC(i+1,3),IC(i+1,4),IC(i+1,5),XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 6
      Ncoor = 5
      Ndim = 5
      CALL amoebar(IC,PEn,Nimage,Ncoor,Ndim,TOL,ROOFUNK,ROO,Niter)
* Now, we retrieve the optimized geometry and energy.
      ROH1 = IC(1,1)
      ROH2 = IC(1,2)
      AHOO1= IC(1,3)
      AHOO2= IC(1,4)
      TAU  = IC(1,5)
      EOpt = PEn(1)
      RETURN
      END 


* Frozen dihedral angle optimization.
      SUBROUTINE TAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,EOpt,TOL)
************************************************************************
* The purpose of this routine is to minimize the H2O2 molecule with    *
* the amoeba routine.  Some liberties have been taken to design the    *
* initial simplex points for the ameoba (images have been generated    *
* by taking the initial guess and perturbing each of the coordinates   *
* by an amount specified by the Step array.                            *
* This routine DOES NOT OPTIMIZE the dihedral angle TAU, which is left *
* as the value it is input (TAU is a frozen coordinate).               *
* All input and output should be in radians and atomic units!!!        *
* The input into this routine is:                                      *
*     -> ROH1 is the first OH bond length in H2O2.                     *
*     -> ROH2 is the second OH bond length in H2O2.                    *
*     -> AHOO1 is the angle between the first OH bond and the OO bond. *
*     -> AHOO2 is the angle between the second OH bond and the OO bond.*
*     -> TAU is the H2O2 dihedral angle.                               *
*     **** The coordinates above are output as their optimized values. *
*     -> TOL is the tolerance for the energy change that is used to    *
*        determine if the potential is optimized.                      *
* The output from this routine:                                        *
*     -> The coordinates are returned in their optimized values except *
*        TAU which is frozen in place.                                 *
*     -> EOpt is the potential energy of the output structure.         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag
      DIMENSION IC(1:6,1:5), PEn(1:6), XX(1:4,1:3), Step(1:5)
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU, Step
      DOUBLE PRECISION IC, TOL,  XX, PEn, PottyOut, EOpt
      DOUBLE PRECISION cmi2har, deg2rad, Pi, TAUFUNK
      EXTERNAL TAUFUNK
      INTEGER Ncoor, Nimage, Niter, Ndim
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(deg2rad=Pi/180.0D0)
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we define initial step-sizes for each coordinate.
      Step(1) = 0.10D0 
      Step(2) = 0.12D0
      Step(3) = 0.13D0
      Step(4) = 3.2D0 * deg2rad
      Step(5) = 3.0D0 * deg2rad
* First, we put the initial coordinates into the optimation array.
      IC(1,1) = ROH1
      IC(1,2) = ROH2
      IC(1,3) = ROO
      IC(1,4) = AHOO1
      IC(1,5) = AHOO2
* We find the energy of this geometry.
      iflag=0
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      PEn(1) = PottyOut * cmi2har
* Now, we generate six other images needed by alterning each coordinate,
* by a step-size.
      DO i=1, 5
         DO j=1, 5
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),IC(i+1,3)
     &                 ,IC(i+1,4),IC(i+1,5),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 6
      Ncoor = 5
      Ndim = 5
      CALL amoebat(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &             TAUFUNK,TAU,Niter)
* Let's do a restart from the minimum just to make sure our
* amoeba is behaving!
      DO i=1, 5
         DO j=1, 5
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),IC(i+1,3)
     &                 ,IC(i+1,4),IC(i+1,5),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 6
      Ncoor = 5
      Ndim = 5
      CALL amoebat(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &             TAUFUNK,TAU,Niter)
* Now, we retrieve the optimized geometry and energy.
      ROH1 = IC(1,1)
      ROH2 = IC(1,2)
      ROO  = IC(1,3)
      AHOO1= IC(1,4)
      AHOO2= IC(1,5)
      EOpt = PEn(1)
      RETURN
      END 



* Frozen dihedral angle AND O--O bond length.
      SUBROUTINE ROOTAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,
     &                     TAU,EOpt,TOL)
************************************************************************
* The purpose of this routine is to minimize the H2O2 molecule with    *
* the amoeba routine.  Some liberties have been taken to design the    *
* initial simplex points for the ameoba (images have been generated    *
* by taking the initial guess and perturbing each of the coordinates   *
* by an amount specified by the Step array.                            *
* All input and output should be in radians and atomic units!!!        *
* This routine DOES NOT OPTIMIZE the O-O bond length ROO and the       *
* dihedral angle TAU which are left as the values input and are frozen.*
* The input into this routine is:                                      *
*     -> ROH1 is the first OH bond length in H2O2.                     *
*     -> ROH2 is the second OH bond length in H2O2.                    *
*     -> AHOO1 is the angle between the first OH bond and the OO bond. *
*     -> AHOO2 is the angle between the second OH bond and the OO bond.*
*     -> TAU is the H2O2 dihedral angle.                               *
*     **** The coordinates above are output as their optimized values. *
*     -> TOL is the tolerance for the energy change that is used to    *
*        determine if the potential is optimized.                      *
* The output from this routine:                                        *
*     -> The coordinates are returned in their optimized values,       *
*        except TAU and ROO which are frozen.                          *
*     -> EOpt is the potential energy of the output structure.         *
************************************************************************
      IMPLICIT NONE
      INTEGER i, j, iflag
      DIMENSION IC(1:5,1:4), PEn(1:5), XX(1:4,1:3), Step(1:4)
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU, Step
      DOUBLE PRECISION IC, TOL, XX, PEn, PottyOut, EOpt
      DOUBLE PRECISION cmi2har, deg2rad, Pi, ROOTAUFUNK
      EXTERNAL ROOTAUFUNK
      INTEGER Ncoor, Nimage, Niter, Ndim
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(deg2rad=Pi/180.0D0)
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* First, we define initial step-sizes for each coordinate.
      Step(1) = 0.150D0 
      Step(2) = 0.13D0
      Step(3) = 3.2D0 * deg2rad
      Step(4) = 3.0D0 * deg2rad
* First, we put the initial coordinates into the optimation array.
      IC(1,1) = ROH1
      IC(1,2) = ROH2
      IC(1,3) = AHOO1
      IC(1,4) = AHOO2
* We find the energy of this geometry.
      iflag=0
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      PEn(1) = PottyOut * cmi2har
* Now, we generate six other images needed by alterning each coordinate,
* by a step-size.
      DO i=1, 4
         DO j=1, 4
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,IC(i+1,3),
     &                 IC(i+1,4),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 5
      Ncoor = 4
      Ndim = 4
      CALL amoebart(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &              ROOTAUFUNK,ROO,TAU,Niter)
* We restart the optimization in order to make sure
* the amoeba didn't behave badly.
      DO i=1, 4
         DO j=1, 4
            IF( i .EQ. j ) THEN
               IC(i+1,j) = IC(1,j) + Step(j)
            ELSE
               IC(i+1,j) = IC(1,j)
               END IF
            END DO
         CALL INT2CART(IC(i+1,1),IC(i+1,2),ROO,IC(i+1,3),
     &                 IC(i+1,4),TAU,XX)
         CALL hoohpot(XX,PottyOut,iflag)
         PEn(i+1) = PottyOut * cmi2har
         END DO
* Now, we optimize the coordinates on the PES using the amoeba.
      Niter = 0
      Nimage = 5
      Ncoor = 4
      Ndim = 4
      CALL amoebart(IC,PEn,Nimage,Ncoor,Ndim,TOL,
     &              ROOTAUFUNK,ROO,TAU,Niter)
* Now, we retrieve the optimized geometry and energy.
      ROH1 = IC(1,1)
      ROH2 = IC(1,2)
      AHOO1= IC(1,3)
      AHOO2= IC(1,4)
      EOpt = PEn(1)
      RETURN
      END 
