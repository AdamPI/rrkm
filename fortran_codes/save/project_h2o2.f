      SUBROUTINE PROJECT(XX,HESS,Inert,flagROO,flagTAU,FrqVal)
************************************************************************
* The purpose of this program is to project out the translation,       *
* rotation, O--O stretch (optional), and torsion (optional) degrees of *
* freedom from the Hessian according to the scheme of Miller, Handy,   *
* and Adams, for H2O2.                                                 *
*                             H1-O3----O4-H2                           *
* ALL INPUTS SHOULD BE IN ATOMIC UNITS!!!!                             *
* The input into this program is:                                      *
*     -> XX is an array containing the cartesian geometry of the       *
*        molecule arranged as (atom#,XYZ#) with atom labels given      *
*        above.                                                        *
*     -> HESS is the hessian matrix organized in a corresponding       *
*        pattern to XX, (atom#,XYZ#,atom#,XYZ#).  This is the CARTESIAN*
*        hessian matrix (no mass weighting on input).                  *
*     -> Inert is an array containing the principle moments of inertia.*
*     -> If flagROO = 1 the O---O stretch is projected out of the      *
*        hessian prior to diagonalization.                             *
*     -> If flagTAU = 1 the torsional mode is projected out prior to   *
*        diagonalization.                                              *
* THe output from this program is:                                     *
*     -> An array (of length 12) FrqVal containing the N projected     *
*        vibrational frequencies in cm**-1.                            *
************************************************************************
      IMPLICIT NONE
      INTEGER Natm, i, j, jj, k, ll, ii, EPSILON, flagROO, ModeNum
      PARAMETER(Natm=4)                               ! Defined for H2O2.
      INTEGER iii, jjj, kk, lll, Modkeep, flagTAU
      INTEGER IndTau, IndRoo
      DIMENSION XXmw(1:Natm,1:3), mass(1:Natm), Inert(1:3)
      DIMENSION TANG(1:Natm,1:3)
      DIMENSION XX(1:Natm,1:3), L(1:Natm,1:3,1:8)
      DIMENSION Pmat(1:Natm,1:3,1:Natm,1:3), Case(1:6)
      DIMENSION Qmat(1:3*Natm,1:3*Natm), Eval(1:3*Natm)
      DIMENSION Ivec(1:3*Natm,1:3*Natm), HssHold2(1:Natm*3,1:Natm*3)
      DIMENSION Hess(1:Natm,1:3,1:Natm,1:3), HssHold(1:Natm*3,1:Natm*3)
      DIMENSION HssRed(1:Natm*3,1:Natm*3), KptMod(1:Natm*3,1:Natm*3)
      DIMENSION EigVec(1:Natm*3,1:Natm*3), FrqVal(1:3*Natm)
      DOUBLE PRECISION Pmat, XXmw, mass, Inert, XX, L, Mtot
      DOUBLE PRECISION Qmat, Eval, Ivec, Hess, HssRed
      DOUBLE PRECISION Normy, Mag, eps, TOL, TANG
      DOUBLE PRECISION HssHold, EigVec, FrqVal, CrapVal, KptMod
      INTEGER IErr, MatZ, Ndim1, Ndim2
      DIMENSION Crap1(1:3*Natm), Crap2(1:3*Natm)
      DOUBLE PRECISION Crap1, Crap2, HssHold2
      PARAMETER(TOL=1.0D-6)
      LOGICAL Case
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
************************************************************************
*====================MASS WEIGHTING & PRELIMINARIES====================*
************************************************************************
* We start by mass weighting the input Hessian and cartesians.
      CALL HESSMW(Hess)
      CALL XMW(XX,XXmw)
* We begin by determining the total mass of the system.
      Mtot = 0.0D0
      DO i=1, Natm
         Mtot = Mtot + mass(i)
         END DO
* Now the coordinates have been redefined (and re-mass weighted).
* We now create the projector.
************************************************************************
*=======================3 TRANSLATION MODE VECTORS=====================*
************************************************************************
* First, we make the translation mode vector.
      DO i=1, 3
         DO j=1, Natm
            DO k=1, 3
               IF ( i .EQ. k ) THEN
                  L(j,i,k) = DSQRT( mass(j)/Mtot )
               ELSE
                  L(j,i,k) = 0.0D0
                  END IF
               END DO
             END DO
          END DO
************************************************************************
*=====================3 ROTATIONAL MODE VECTORS========================*
************************************************************************
* Second, we make the rotational modes.
      DO i=1, 3
         DO j=1, Natm
            DO k=4, 6
               ll = k -3
               L(j,i,k) = 0.0D0
               DO ii=1, 3
                  Case(1) = (ll .EQ. 1) .AND. 
     &                      ((ii .EQ. 2) .AND. (i .EQ. 3))
                  Case(2) = (ll .EQ. 3) .AND. 
     &                      ((ii .EQ. 1) .AND. (i .EQ. 2))
                  Case(3) = (ll .EQ. 2) .AND. 
     &                      ((ii .EQ. 3) .AND. (i .EQ. 1))
                  Case(4) = (ll .EQ. 3) .AND. 
     &                      ((ii .EQ. 2) .AND. (i .EQ. 1))
                  Case(5) = (ll .EQ. 1) .AND. 
     &                      ((ii .EQ. 3) .AND. (i .EQ. 2))
                  Case(6) = (ll .EQ. 2) .AND. 
     &                      ((ii .EQ. 1) .AND. (i .EQ. 3))
                  IF ( Case(1) .OR. Case(2) .OR. Case(3) ) THEN
                     eps = 1.0D0
                  ELSE IF ( Case(4) .OR. Case(5) .OR. Case(6) ) THEN
                     eps = -1.0D0
                  ELSE
                     eps = 0.0D0
                     END IF
                  L(j,i,k) = L(j,i,k) + eps * XXmw(j,ii) /
     &                       DSQRT(Inert(ll))
                  END DO
               END DO
            END DO
         END DO
********************************************************************
*====================CREATING TANGENT MODES TO=====================*
*================DIHEDRAL AND FRAGMENT STRETCH FOR=================*
*===============================H2O2===============================*
********************************************************************
* We use the flags in order to determine which modes to project out.
* If flagROO is set to 1, we project out the H-O~~~~O-H stretch.
* If flagTAU is set to 1, we project out the torsion angle.
      IF ( (flagROO .NE. 1 ) .AND. (flagTAU .NE. 1) ) THEN
         ModeNum = 6
      ELSE IF ( ( flagROO .NE. 1 ) .AND. (flagTAU .EQ. 1) ) THEN
         ModeNum = 7
         IndTau = 7
      ELSE IF ( ( flagROO .EQ. 1 ) .AND. (flagTAU .NE. 1) ) THEN
         ModeNum = 7
         IndRoo = 7
      ELSE IF ( (flagROO .EQ. 1 ) .AND. (flagTAU .EQ. 1) ) THEN
         ModeNum = 8
         IndTau = 7
         IndRoo = 8
      END IF
* Now, if desired we project out the dihedral coordinate.
      IF ( flagTAU .EQ. 1 ) THEN
         CALL TAUTANG(1,3,4,2,Natm,XX,TANG)       ! Create Torsion tangent.
         CALL TANMW(Natm,mass,TANG)               ! Convert to MW'ed coords.
* Now we place this in our mode vector.
         DO i=1, Natm
            DO j=1, 3
               L(i,j,IndTau) = TANG(i,j)
               END DO
            END DO
      END IF
* Now, if desired, we project out the (HO)---(OH) stretch.
      IF (flagROO .EQ. 1 ) THEN
C          CALL ROOTAN(XX,TANG)             ! Create fragment stretch tangent.
          CALL ROO2T(XX,TANG)
          CALL TANMW(Natm,mass,TANG)       ! Convert to MW'ed coords.
* Now we place this in our mode vector.
         DO i=1, Natm
            DO j=1, 3
               L(i,j,IndRoo) = TANG(i,j)
               END DO
            END DO
      END IF
************************************************************************
*================CONSTRUCTION OF PROJECTOR MATRIX======================*
************************************************************************
* We can now construct the projector matrix.
      DO i=1, Natm
          DO j=1, 3
             DO k=1, Natm
                DO ll=1, 3
                   Pmat(k,ll,i,j) = 0.0D0
                   DO ii=1, ModeNum
                      Pmat(k,ll,i,j) = Pmat(k,ll,i,j) + 
     &                                 L(i,j,ii) * L(k,ll,ii)
                      END DO
                   END DO
                END DO
             END DO
          END DO
* We now subtract the projector matrix from the identity matrix.
* This removes the undesired modes from the coordinate description of
* the system (i.e. set of coordinates without the translation, rotation, or
* gradient).
      DO i=1, Natm
         DO j=1, 3
            DO k=1, Natm
               DO ll=1, 3
                  ii = Natm * (j-1) + i
                  jj = Natm * (ll-1) + k
                  IF( ii .EQ. jj ) THEN
                     Qmat(jj,ii) = 1.0D0 - Pmat(k,ll,i,j)
                  ELSE
                     Qmat(jj,ii) = - Pmat(k,ll,i,j)
                     END IF
                  END DO
               END DO
            END DO
         END DO 
CCC I have commented out an older projection routine, that works just
CCC fine.  I use the newer routine because it automatically discards
CCC the ~zero eigenvalues.
C************************************************************************
C*====================PROJECTION OF HESSIAN MATRIX======================*
C************************************************************************
C* Now we use the Q projection matrix to project out the 
C* undesired modes from the Hessian.
C* First, right multiplication of Q.
C       DO i=1, Natm
C          DO j=1, 3
C             DO k=1, Natm
C                DO ll=1, 3
C                   ii = Natm * (j-1) + i
C                   jj = Natm * (ll-1) + k
C                   HssHold(jj,ii) = 0.0D0
C                   DO kk=1, Natm
C                      DO lll=1, 3
C                         iii = Natm * (lll-1) + kk
C                         HssHold(jj,ii) = HssHold(jj,ii) + 
C     &                        Hess(k,ll,kk,lll) * Qmat(iii,ii)
C                         END DO
C                      END DO
C                   END DO
C                END DO
C             END DO
C          END DO
C* Now, left multiplication of Q.
C      DO i=1, Natm*3
C         DO j=1, Natm*3
C            HssRed(j,i) = 0.0D0
C            DO k=1, Natm*3
C               HssRed(j,i) = HssRed(j,i) + Qmat(j,k) * 
C     &                       HssHold(k,i)
C               END DO
C            END DO
C         END DO
C************************************************************************
C*=================DIAGONALIZE PROJECTED HESSIAN MATRIX=================*
C************************************************************************
C* Now we diagonalize the Hessian matrix.
C      MatZ = 1
C      IErr = 0
C      Ndim1 = 3*Natm
C      Ndim2 = 3*Natm
C      DO i=1, 3*Natm
C         Crap1(i) = 0.0D0
C         Crap2(i) = 0.0D0
C         FrqVal(i) = 0.0D0
C         DO j=1, 3*Natm
C            EigVec(i,j) = 0.0D0
C            END DO
C         END DO
C      CALL rs(Ndim1,Ndim2,HssRed,FrqVal,MatZ,EigVec,Crap1,Crap2,IErr)
C***
C      WRITE(*,*) ''
C      WRITE(*,*) 'The eigenvalues of the Hessian matrix are:'
C      DO i=1, 3*Natm
C            IF ( FrqVal(i) .GT. 0.0D0) THEN
C               CrapVal = DSQRT(FrqVal(i)) 
C            ELSE 
C               CrapVal = -DSQRT(-FrqVal(i))
C               END IF
C               CrapVal = CrapVal / cmi2har
C               WRITE(*,*) CrapVal 
C         END DO
C      WRITE(*,*) ''
C      WRITE(*,*) 'The eigenvectors are:'
C      DO i=1, 9
C         WRITE(*,999) EigVec(i,1), EigVec(i,2), EigVec(i,3),
C     &                EigVec(i,4), EigVec(i,5), EigVec(i,6),
C     &                EigVec(i,7), EigVec(i,8), EigVec(i,9)
C         END DO
*****
***** HERE WE DIAGONALIZE THE PROJECTION MATRIX
*****
* We now diagonalize this matrix to separate the missing modes from the 
* important modes. 
************************************************************************
*=================DIAGONALIZE PROJECTED HESSIAN MATRIX=================*
************************************************************************
      Ndim1 = 3*Natm
      Ndim2 = 3*Natm
      MatZ = 1
      IErr = 0
      DO i=1, 3*Natm
         Crap1(i) = 0.0D0
         Crap2(i) = 0.0D0
         EVal(i) = 0.0D0
         DO j=1, 3*Natm
            Ivec(i,j) = 0.0D0
            END DO
         END DO
      CALL rs(Ndim1,Ndim2,Qmat,EVal,MatZ,IVec,Crap1,Crap2,IErr)
C*****
C999   FORMAT(9(F9.3,2X))
C      WRITE(*,*) 'EIGENVALUES OF PROJECTION MATRIX Q ARE:'
C      DO i=1, 3*Natm
C         WRITE(*,*) Eval(i)
C         END DO
C      WRITE(*,*) ''
*****   
***** HERE WE PROJECT THE HESSIAN MATRIX ONTO THE EIGENVECTORS OF THE PROJECTION MATRIX.
*****
* We throw away the smallest eigenvectors.
      DO i=1, 3*Natm-ModeNum
         DO j=1, 3*Natm
            KptMod(j,i) = Ivec(j,i+ModeNum)
             END DO
         END DO
      ModKeep = 3*Natm-ModeNum
* We now use convert the Hessian to the basis of the non-zero eigenvalue
* modes. First, let's reorganize the Hessian.
       DO i=1, Natm
          DO j=1, 3
             ii = Natm * (j-1) + i
             DO k=1, Natm
                DO ll=1, 3
                   jj = Natm * (ll-1) + k
                   HssHold2(jj,ii) = Hess(k,ll,i,j)
                   END DO
                END DO
             END DO      
          END DO
* Left multiplication.
      DO i=1, ModKeep
         DO j=1,3*Natm 
            HssHold(j,i) = 0.0D0
            DO k=1, 3*Natm
               HssHold(j,i) = HssHold(j,i) + HssHold2(j,k) * KptMod(k,i)
               END DO
            END DO
         END DO
      DO i=1, Modkeep
         DO j=1, Modkeep
            HssRed(j,i) = 0.0D0
            DO k=1, 3*Natm
               HssRed(j,i) = HssRed(j,i) + KptMod(k,j) * HssHold(k,i)       
               END DO
            END DO
         END DO
**** WRITING OF THE RESULTS.
* Now we diagonalize the Hessian matrix.
      MatZ = 1
      IErr = 0
      Ndim1 = 3*Natm
      Ndim2 = ModKeep
      DO i=1, 3*Natm
         Crap1(i) = 0.0D0
         Crap2(i) = 0.0D0
         FrqVal(i) = 0.0D0
         DO j=1, 3*Natm
            EigVec(i,j) = 0.0D0
            END DO
         END DO
      CALL rs(Ndim1,Ndim2,HssRed,FrqVal,MatZ,EigVec,Crap1,Crap2,IErr)
      WRITE(*,*) ''
      WRITE(*,*) 'Here are the frequencies of the diagonalized'
      WRITE(*,*) 'PROJECTED Hessian matrix:'
      DO i=1, Modkeep
         IF( FrqVal(i) .LT. 0.0D0) THEN
            STOP 'NEGATIVE FREQ IN PROJECTION SCHEME'
            END IF
         FrqVal(i) = DSQRT(FrqVal(i)) / cmi2har
         END DO
      WRITE(*,*) ''
      RETURN
      END  
