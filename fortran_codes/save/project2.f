      SUBROUTINE PROJECT(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Natm,Step,
     &                   EigVec,flagROO,flagTAU)
************************************************************************
* The purpose of this program is to project out the translation,       *
* rotation, and gradient (OPTIONAL) degrees of freedom from the system.*
* The input of this program is the mass weighted coordinates (centered *
* at the center of mass and with respect to the principle axes of      *
* rotation).  The inertia with respect to the principle axes are input *
* in the array Inert (see subroutines CMMW and INERTIA in coord4.f).   *
* This routine also uses hss.f.                                        *
************************************************************************
      IMPLICIT NONE
      INTEGER Natm, i, j, jj, k, ll, ii, EPSILON, flagROO, ModeNum
      INTEGER iii, jjj, kk, lll, Modkeep, flagTAU
      INTEGER IndTau, IndRoo
      DIMENSION Xmw(1:Natm,1:3), mass(1:Natm), Inert(1:3)
      DIMENSION PrinAx(1:3,1:3), Del(1:Natm,1:3)
      DIMENSION X(1:Natm,1:3), L(1:Natm,1:3,1:8)
      DIMENSION Pmat(1:Natm,1:3,1:Natm,1:3), Case(1:6)
      DIMENSION Qmat(1:3*Natm,1:3*Natm), Eval(1:3*Natm)
      DIMENSION Ivec(1:3*Natm,1:3*Natm), OffD(1:3*Natm), sum(1:3*Natm)
      DIMENSION Hess(1:Natm,1:3,1:Natm,1:3), HssHold(1:Natm*3,1:Natm*3)
      DIMENSION HssRed(1:Natm*3,1:Natm*3), KptMod(1:Natm*3,1:Natm*3)
      DIMENSION EigVec(1:Natm*3,1:Natm*3), FrqVal(1:3*Natm)
      DOUBLE PRECISION Pmat, Xmw, mass, Inert, Del, X, L, Mtot
      DOUBLE PRECISION Qmat, Eval, Ivec, OffD, sum, Hess, HssRed
      DOUBLE PRECISION Normy, PrinAx, Mag, eps, TOL, Step
      DOUBLE PRECISION HssHold, EigVec, FrqVal, CrapVal, KptMod
      PARAMETER(TOL=1.0D-6)
      LOGICAL Case
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
* We begin by determining the total mass of the system.
      Mtot = 0.0D0
      DO i=1, Natm
         Mtot = Mtot + mass(i)
         END DO
* Now the coordinates have been redefined (and re-mass weighted).
* We now create the projector.
* First, we make the translation mode vector.
      DO i=1, 3
         DO j=1, Natm
            DO k=1, 3
               IF ( i .EQ. k ) THEN
                  L(j,i,k) = DSQRT( mass(j)/Mtot )
               ELSE
                  L(j,i,k) = 0.0D0
                  END IF
               END DO
             END DO
          END DO
* Second, we make the rotational modes.
      DO i=1, 3
         DO j=1, Natm
            DO k=4, 6
               ll = k -3
               L(j,i,k) = 0.0D0
               DO ii=1, 3
                  Case(1) = (ll .EQ. 1) .AND. 
     &                      ((ii .EQ. 2) .AND. (i .EQ. 3))
                  Case(2) = (ll .EQ. 3) .AND. 
     &                      ((ii .EQ. 1) .AND. (i .EQ. 2))
                  Case(3) = (ll .EQ. 2) .AND. 
     &                      ((ii .EQ. 3) .AND. (i .EQ. 1))
                  Case(4) = (ll .EQ. 3) .AND. 
     &                      ((ii .EQ. 2) .AND. (i .EQ. 1))
                  Case(5) = (ll .EQ. 1) .AND. 
     &                      ((ii .EQ. 3) .AND. (i .EQ. 2))
                  Case(6) = (ll .EQ. 2) .AND. 
     &                      ((ii .EQ. 1) .AND. (i .EQ. 3))
                  IF ( Case(1) .OR. Case(2) .OR. Case(3) ) THEN
                     eps = 1.0D0
                  ELSE IF ( Case(4) .OR. Case(5) .OR. Case(6) ) THEN
                     eps = -1.0D0
                  ELSE
                     eps = 0.0D0
                     END IF
                  L(j,i,k) = L(j,i,k) + eps * Xmw(j,ii) /
     &                       DSQRT(Inert(ll))
                  END DO
               END DO
            END DO
         END DO
******
998   FORMAT(F20.10)
      DO k=1, 6
         WRITE(*,*) 'Mode ', k
         Mag = 0.0D0
         DO j=1, Natm
            DO i=1, 3
               Mag = Mag + L(j,i,k)**2
               WRITE(*,998) L(j,i,k)
               END DO
            END DO
         WRITE(*,*) 'Vector magnitude ', Mag
         END DO
* We use the flags in order to determine which modes to project out.
* If flagROO is set to 1, we project out the H-O~~~~O-H stretch.
* If flagTAU is set to 1, we project out the torsion angle.
      IF ( (flagROO .NE. 1 ) .AND. (flagTAU .NE. 1) ) THEN
         ModeNum = 6
      ELSE IF ( ( flagROO .NE. 1 ) .AND. (flagTAU .EQ. 1) ) THEN
         ModeNum = 7
         IndTau = 7
      ELSE IF ( ( flagROO .EQ. 1 ) .AND. (flagTAU .NE. 1) ) THEN
         ModeNum = 7
         IndRoo = 7
      ELSE IF ( (flagROO .EQ. 1 ) .AND. (flagTAU .EQ. 1) ) THEN
         ModeNum = 8
         IndTau = 7
         IndRoo = 8
      END IF
* Now, if desired we project out the dihedral coordinate.
      IF ( flagTAU .EQ. 1 ) THEN
         
      END IF
* Last, we make the gradient mode.  We will need to normalize
* this mode.
      CALL GRAD(Natm,X,Del,mass)
      Normy = 0.0D0
      DO i=1, Natm
         DO j=1, 3
            Normy = Normy + Del(i,j)**2
            END DO 
         END DO
      Normy = DSQRT(Normy)
      DO i=1, Natm
         DO j=1, 3
            L(i,j,7) = Del(i,j) / Normy
            END DO
         END DO
* We can now construct the projector matrix.
10    DO i=1, Natm
          DO j=1, 3
             DO k=1, Natm
                DO ll=1, 3
                   Pmat(k,ll,i,j) = 0.0D0
                   DO ii=1, ModeNum
                      Pmat(k,ll,i,j) = Pmat(k,ll,i,j) + 
     &                                 L(i,j,ii) * L(k,ll,ii)
                      END DO
                   END DO
                END DO
             END DO
          END DO
* We now subtract the projector matrix from the identity matrix.
* This removes the 7 undesired modes from the coordinate description of
* the system (i.e. set of coordinates without the translation, rotation, or
* gradient).
      DO i=1, Natm
         DO j=1, 3
            DO k=1, Natm
               DO ll=1, 3
                  ii = Natm * (j-1) + i
                  jj = Natm * (ll-1) + k
                  IF( ii .EQ. jj ) THEN
                     Qmat(jj,ii) = 1.0D0 - Pmat(k,ll,i,j)
                  ELSE
                     Qmat(jj,ii) = - Pmat(k,ll,i,j)
                     END IF
                  END DO
               END DO
            END DO
         END DO 
*****
***** HERE WE MAKE HESSIAN MATRIX
*****
* First we create the Hessian matrix.
       CALL HSS(Natm,Xmw,Step,GRAD,mass,Hess)      
* Now we use the Q projection matrix to project out the 
* undesired modes from the Hessian.
       DO i=1, Natm
          DO j=1, 3
             DO k=1, Natm
                DO ll=1, 3
                   ii = Natm * (j-1) + i
                   jj = Natm * (ll-1) + k
                   HssHold(jj,ii) = 0.0D0
                   DO kk=1, Natm
                      DO lll=1, 3
                         iii = Natm * (lll-1) + kk
                         HssHold(jj,ii) = HssHold(jj,ii) + 
     &                        Hess(k,ll,kk,lll) * Qmat(iii,ii)
                         END DO
                      END DO
                   END DO
                END DO
             END DO
          END DO
      DO i=1, Natm*3
         DO j=1, Natm*3
            HssRed(j,i) = 0.0D0
            DO k=1, Natm*3
               HssRed(j,i) = HssRed(j,i) + Qmat(j,k) * 
     &                       HssHold(k,i)
               END DO
            END DO
         END DO
* Now we diagonalize the matrix.
      CALL tred2II(HssRed,3*Natm,3*Natm,FrqVal,OffD)
* Now we must initialize the matrix that will contain the eigenvectors.
      DO i=1, 3*Natm
         DO j=1, 3*Natm
            EigVec(j,i) = HssRed(j,i)
            END DO
         END DO
* Now we perform the diagonlization of the tri-diagonal matrix.
      CALL tqliII(FrqVal,OffD,3*Natm,3*Natm,EigVec)          
* Here we normalize the eigenvectors.
      DO i=1, 3*Natm
         Normy = 0.0D0
         DO j=1, 3* Natm
            Normy = Normy + EigVec(j,i)**2
            END DO
         Normy = DSQRT(Normy)
         DO j=1, 3*Natm
            EigVec(j,i) = EigVec(j,i) / Normy
            END DO
         END DO
***
      WRITE(*,*) ''
      WRITE(*,*) 'The eigenvalues of the Hessian matrix are:'
      DO i=1, 3*Natm
            IF ( FrqVal(i) .GT. 0.0D0) THEN
               CrapVal = DSQRT(FrqVal(i)) 
            ELSE 
               CrapVal = -DSQRT(-FrqVal(i))
               END IF
               CrapVal = CrapVal * 219470.0D0
               WRITE(*,*) CrapVal 
         END DO
      WRITE(*,*) ''
      WRITE(*,*) 'The eigenvectors are:'
      DO i=1, 9
         WRITE(*,999) EigVec(i,1), EigVec(i,2), EigVec(i,3),
     &                EigVec(i,4), EigVec(i,5), EigVec(i,6),
     &                EigVec(i,7), EigVec(i,8), EigVec(i,9)
         END DO
*****
***** HERE WE DIAGONALIZE THE PROJECTION MATRIX
*****
* We now diagonalize this matrix to separate the missing modes from the 
* important modes.  We need the diagonalization routines from the 
* coord.f file.
      CALL tred2II(Qmat,3*Natm,3*Natm,Eval,OffD)
      DO i=1, 3*Natm
         DO j=1, 3*Natm
            Ivec(j,i) = Qmat(j,i)
            END DO
         END DO
      CALL tqliII(Eval,OffD,3*Natm,3*Natm,Ivec)
      DO i=1, 3*Natm
         Normy = 0.0D0
         DO j=1, 3*Natm
            Normy = Normy + Ivec(j,i)**2
            END DO
         Normy = DSQRT(Normy)
         DO j=1, 3*Natm
            Ivec(j,i) = Ivec(j,i) / Normy
            END DO 
         END DO
*****
999   FORMAT(9(F9.3,2X))
      DO i=1, 3*Natm
         WRITE(*,*) Eval(i)
         END DO
      WRITE(*,*) ''
      DO i=1, 9
         WRITE(*,999) Ivec(i,1), Ivec(i,2), Ivec(i,3), Ivec(i,4), 
     &                Ivec(i,5), Ivec(i,6), Ivec(i,7), Ivec(i,8)
     &               , Ivec(i,9)
         END DO
*****   
***** HERE WE PROJECT THE HESSIAN MATRIX ONTO THE EIGENVECTORS OF THE PROJECTION MATRIX.
*****
* We can now select separate the modes by eigenvalue.
      Modkeep = 0
      DO i=1, 3*Natm
         IF( ABS(Eval(i)) .GT. TOL ) THEN
            Modkeep = Modkeep + 1
            DO j=1, 3*Natm
               KptMod(j,Modkeep) = Ivec(j,i)
               END DO
            END IF
         END DO
* Now we construct the Hessian matrix.
      CALL HSS(Natm,Xmw,Step,GRAD,mass,Hess)
* We now use convert the Hessian to the basis of the non-zero eigenvalue
* modes.
      DO i=1, Modkeep
            DO k=1, 3
               DO ll=1, Natm
                  iii = Natm * (k-1) + ll
                  HssHold(jj,i) = 0.0D0
                  DO ii=1, 3
                     DO jj=1, Natm
                        jjj = Natm * (ii-1) + jj
                        HssHold(iii,i) = HssHold(iii,i) + 
     &                        Hess(ll,k,jj,ii) * KptMod(jjj,i)
                        END DO
                     END DO
                  END DO
             END DO
         END DO
      DO i=1, Modkeep
         DO j=1, Modkeep
            HssRed(j,i) = 0.0D0
            DO k=1, 3*Natm
               HssRed(j,i) = HssRed(j,i) + KptMod(k,j) * HssHold(k,i)       
               END DO
            END DO
         END DO
      DO i=1, 3
        WRITE(*,995) HssRed(i,1), HssRed(i,2), HssRed(i,3)
        END DO
      CALL tred2II(HssRed,Modkeep,Natm*3,FrqVal,OffD)
      DO i=1, Modkeep
         DO j=1, Modkeep
            EigVec(j,i) = HssRed(j,i)
            END DO
         END DO
      CALL tqliII(FrqVal,OffD,Modkeep,3*Natm,EigVec) 
**** WRITING OF THE RESULTS.
995   FORMAT(3(F15.8,2X))
      WRITE(*,*) ''
      WRITE(*,*) 'Here are the frequencies of the diagonalized'
      WRITE(*,*) 'PROJECTED Hessian matrix:'
      DO i=1, Modkeep
         WRITE(*,*) FrqVal(i) * 214970D0
         END DO
      WRITE(*,*) ''
      WRITE(*,*) 'Here are the eigenvectors of the system.'
      DO i=1, Modkeep
         WRITE(*,995) EigVec(i,1), EigVec(i,2), EigVec(i,3)
         END DO
* The modes with near zero eigenvalues have been discarded.
* STOP HERE FOR TESTING.
* Seems to work up to this point.
      RETURN
      END  
