      SUBROUTINE INTMAP(NumNums,NQmax,NQ,MasterN,wichway)
************************************************************************
* The purpose of this routine is to map a set of integers to a single  *
* integer, or the routine maps one integer to a set of integers.       *
* We shall assume the last integer runs the fastest in the set (i.e.   *
* the set must be ordered that the last integer increases fastest).    *
*                                                                      *
*  {1,1,...,1,1}  -> 1                                                 *
*  {1,1,...,1,2}  -> 2                                                 *
*      ...       ... ...                                               *
*  {1,1,...,1,NQmax(NumNums)} -> NQmax(NumNums)                        *
*  {1,1,...,2,1}              -> NQmax(NumNums) + 1                    *
*      ...                   ...        ...                            *
*                                                                      *
* > NumNums is the number of numbers in the set of integers.           *
* > NQmax is an array containing the maximum possible value of each    *
*   integer in the set (the minimum is assumed to be 1 and the maximums*
*   are assumed to correspond in order to the elements in NQ).         *
* > NQ is the array containing the integers which must vary between 1  *
*   and the corresponding max value in the corresponding element in    * 
*   NQmax.                                                             *
* > MasterN is the number that we map to the set NQ, or that the set   *
*   NQ is mapped to.                                                   *
* > wichway is an integer. If positive the routine takes in the set NQ *
*   and calculates MasterN.  If zero or negative, the routine takes    *
*   MasterN and calculates the set NQ.                                 *
************************************************************************
      IMPLICIT NONE
      INTEGER NumNums, MasterN, wichway, i, k, jold, jnew, valprod
      DIMENSION NQ(1:NumNums), NQmax(1:NumNums)
      INTEGER NQ, NQmax

************************************************************************
*====================WRAPPING QUANTUM NUMBERS==========================*
************************************************************************

* We first suppose that wichway is positive so NQ -> MasterN.
      IF(wichway .GT. 0) THEN
         jold = NQ(NumNums)

* If there is only one quantum number, then the above is all we need, 
* and so is a special case..
         IF(NumNums .EQ. 1) THEN
            MasterN = jold
            RETURN
            END IF

* If it is not the special case, we may proceed with the calculation.
         jold = NQ(NumNums)
         DO 10, k=1, NumNums-1
            valprod=1
            DO 11, i=0, k-1
               valprod = valprod * NQmax(NumNums-i)
11             CONTINUE
            jnew = (NQ(NumNums-k)-1) * valprod + jold
            jold = jnew
10          CONTINUE
 
         MasterN = jold

************************************************************************
*===================UNWRAPPING THE QUANTUM NUMBERS=====================*
************************************************************************

* Now we consider the opposite mapping MasterN -> NQ.
      ELSE

* We consider the trivial case.
         IF(NumNums .EQ. 1) THEN
            NQ(NumNums) = MasterN
            RETURN
            END IF 
         
         jnew = MasterN
         DO 13, k=1, NumNums
            IF(k .LT. NumNums) THEN
               valprod=1
               DO 14, i=NumNums, k+1, -1
                  valprod=valprod*NQmax(i)
14                CONTINUE
               NQ(k) = jnew / valprod + 1
               jold = MOD(jnew,valprod)
               IF(jold .EQ. 0) THEN
                  NQ(k) = NQ(k) - 1
                  jnew = MOD(jnew-1,valprod)+1
               ELSE
                  jnew = jold
                  END IF
            ELSE 
               NQ(NumNums) = jnew
               END IF
13          CONTINUE
         END IF
      RETURN
      END 
