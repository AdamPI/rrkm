* Minimization at fixed ROO.
      SUBROUTINE ROOTORSZPE(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,TOL,ZPE)
************************************************************************
* The purpose of this routine is to estimate the zero point energy in  *
* the torsional mode of H2O2 in its equilibrium geometry AT FIXED ROO. *
* The input into this routine is:                                      *
*   -> ROH1, ROH2, ROO, AHOO1, AHOO2, TAU are estimates of the         *
*      internal coordinates of hydrogen peroxide and will be the       *
*      equilibrium structure values on output except ROO which is fixed*
*   -> Step is the step size at which to generate the Hessian matrix   *
*      probably a good guess is ~0.01 bohr.                            *
*   -> TOL is the tolerance for the geometry optimization (<10**-12).  *
*   -> ZPE will contain the zero point of the torsional mode upon out  *
*      put.                                                            *
* ALL VALUES SHOULD BE GIVEN IN ATOMIC UNITS AND IN RADIANS!           *
************************************************************************
      IMPLICIT NONE
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DIMENSION XX(1:4,1:3), TANG(1:4,1:3), III(1:3)
      DOUBLE PRECISION XX, TANG, D2VDTAU, Vmin, TOL, III
      DIMENSION Hessian(1:4,1:3,1:4,1:3), mass(1:4), Proj(1:4,1:3)
      DOUBLE PRECISION Hessian, mass, Step, Proj, HessTau, ZPE
      INTEGER i, j, ii, jj
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
* First, find minimum energy geometry.
      CALL ROOMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Vmin,TOL)
* Now get the hessian matrix at this geometry.
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,
     &                XX,HESSIAN,III)
* Now we get the tangent vector to the torsion.
      CALL TAUTANG(1,3,4,2,4,XX,TANG)
* Now we mass weight the hessian and the tangent vector.
      CALL HESSMW(Hessian)
      CALL TANMW(4,mass,TANG)
* Now we project the Hessian matrix onto the torsion.
      HessTau = 0.0D0
      DO i=1, 4
         DO j=1, 3
            Proj(i,j) = 0.0D0
            DO ii=1, 4
               DO jj=1, 3
                  Proj(i,j) = Proj(i,j)+Hessian(i,j,ii,jj)*
     &                                  TANG(ii,jj)
                  END DO
               END DO
            HessTau = HessTau + Proj(i,j) * TANG(i,j)
            END DO
         END DO
* HessTau is the square of our frequency, so the ZPE is:
      ZPE = 0.50D0 * DSQRT(HessTau)      ! In atomic units.
      RETURN
      END


* Full minimization.
      SUBROUTINE TORSZPE(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,TOL,ZPE)
************************************************************************
* The purpose of this routine is to estimate the zero point energy in  *
* the torsional mode of H2O2 in its equilibrium geometry.              *
* The input into this routine is:                                      *
*   -> ROH1, ROH2, ROO, AHOO1, AHOO2, TAU are estimates of the         *
*      internal coordinates of hydrogen peroxide and will be the       *
*      equilibrium structure values on output.                         *
*   -> Step is the step size at which to generate the Hessian matrix   *
*      probably a good guess is ~0.01 bohr.                            *
*   -> TOL is the tolerance for the geometry optimization (<10**-12).  *
*   -> ZPE will contain the zero point of the torsional mode upon out  *
*      put.                                                            *
* ALL VALUES SHOULD BE GIVEN IN ATOMIC UNITS AND IN RADIANS!           *
************************************************************************
      IMPLICIT NONE
      DOUBLE PRECISION ROH1, ROH2, ROO, AHOO1, AHOO2, TAU
      DIMENSION XX(1:4,1:3), TANG(1:4,1:3), III(1:3)
      DOUBLE PRECISION XX, TANG, D2VDTAU, Vmin, TOL, III
      DIMENSION Hessian(1:4,1:3,1:4,1:3), mass(1:4), Proj(1:4,1:3)
      DOUBLE PRECISION Hessian, mass, Step, Proj, HessTau, ZPE
      INTEGER i, j, ii, jj
      DATA mass/1837.1526509653975D0,1837.1526509653975D0,
     &          29156.94570303613D0,29156.94570303613D0/
* First, find minimum energy geometry.
      CALL FULLMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Vmin,TOL)
* Now get the hessian matrix at this geometry.
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,
     &                XX,HESSIAN,III)
* Now we get the tangent vector to the torsion.
      CALL TAUTANG(1,3,4,2,4,XX,TANG)
* Now we mass weight the hessian and the tangent vector.
      CALL HESSMW(Hessian)
      CALL TANMW(4,mass,TANG)
* Now we project the Hessian matrix onto the torsion.
      HessTau = 0.0D0
      DO i=1, 4
         DO j=1, 3
            Proj(i,j) = 0.0D0
            DO ii=1, 4
               DO jj=1, 3
                  Proj(i,j) = Proj(i,j)+Hessian(i,j,ii,jj)*
     &                                  TANG(ii,jj)
                  END DO
               END DO
            HessTau = HessTau + Proj(i,j) * TANG(i,j)
            END DO
         END DO
* HessTau is the square of our frequency, so the ZPE is:
      ZPE = 0.50D0 * DSQRT(HessTau)      ! In atomic units.
      RETURN
      END
