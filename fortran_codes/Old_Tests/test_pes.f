      PROGRAM MAIN
      IMPLICIT NONE
      INTEGER iflag, i, j
      DIMENSION XX(1:4,1:3)
      DOUBLE PRECISION XX, Deg2Rad, Pi, Bohr2A, PottyOut
      PARAMETER(Pi=3.141592653589793238462643383279502884197D0)
      PARAMETER(Deg2Rad=Pi/180.0D0)
      PARAMETER(Bohr2A=0.5291772109217D0)
      DOUBLE PRECISION ROH1, ROH2, ROO, TAU, AHOO1, AHOO2
      DIMENSION II(1:3), HESSIAN(1:4,1:3,1:4,1:3), NQmax(1:2)
      DIMENSION NQi(1:2), NQj(1:2)
      DOUBLE PRECISION II, HESSIAN, Step, TOL
      INTEGER flagCOM, flagROT, flagTAU, flagROO
      INTEGER NMAX, NumNums, NQmax, NQi, NQj, counti, countj
      DOUBLE PRECISION cmi2har
      PARAMETER(cmi2har=1.0D0/219474.631370515D0)
* For the intmap routine.
      NumNums = 2
      NMAX=12
      NQmax(1) = 4
      NQmax(2) = 3
* Set iflag to zero to initialize potential on the first call.
* The PES routine will modify iflag for us afterward, so no need
* to worry about it ever again!
      iflag=0
* Set the geometry.
      ROH1 = 1.818
      ROH2 = 1.818
      ROO = 2.745
      AHOO1 = 99.76 * Deg2Rad
      AHOO2 = 80.24 * Deg2Rad
      TAU = 114.30 * Deg2Rad
* Change to cartesian coordinates.
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
9999  FORMAT(A1,3(F8.5,3X))
      WRITE(*,*) 'THE INITIAL GEOMETRY XYZ IS:'
      WRITE(*,*) '4'
      WRITE(*,*) ''
      WRITE(*,9999) 'H', XX(1,1), XX(1,2), XX(1,3)
      WRITE(*,9999) 'H', XX(2,1), XX(2,2), XX(2,3)
      WRITE(*,9999) 'O', XX(3,1), XX(3,2), XX(3,3)
      WRITE(*,9999) 'O', XX(4,1), XX(4,2), XX(4,3)
* Call the potential.
      CALL hoohpot(XX,PottyOut,iflag)
      WRITE(*,*) 'POTENTIAL ENERGY IN FIRST FRAME:'
      WRITE(*,*) PottyOut
      CALL H2O2COM(XX)
      CALL hoohpot(XX,PottyOut,iflag)
      WRITE(*,*) 'POTENTIAL ENERGY IN COM FRAME:'
      WRITE(*,*) PottyOut
      flagCOM = 0
      flagROT = 1
      CALL INERT(XX,II,flagCOM,flagROT)
      CALL hoohpot(XX,PottyOut,iflag)
      WRITE(*,*) 'POTENTIAL ENERGY IN PA FRAME:'
      WRITE(*,*) PottyOut
      WRITE(*,*) 'MOMENTS OF INERTIA IN ATOMIC UNITS:'
      WRITE(*,*) II(1), II(2), II(3)
      WRITE(*,*) 'NOW CONSTRUCT THE HESSIAN'
      WRITE(*,*) 'THE HESSIAN IS:'
999   FORMAT(12(F10.5,2X))
      Step=0.03D0
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)
C      DO i=1, NQmax(1)
C         DO j=1, NQmax(2)
C      WRITE(*,999) HESSIAN(i,j,1,1), HESSIAN(i,j,1,2), HESSIAN(i,j,1,3),
C     &             HESSIAN(i,j,2,1), HESSIAN(i,j,2,2), HESSIAN(i,j,2,3),
C     &             HESSIAN(i,j,3,1), HESSIAN(i,j,3,2), HESSIAN(i,j,3,3),
C     &             HESSIAN(i,j,4,1), HESSIAN(i,j,4,2), HESSIAN(i,j,4,3)
C            END DO
C         END DO
C      WRITE(*,*) 'THE XYZ FILE FOR CORRESPONDING CARTESIAN COORDS:'
998   FORMAT(A1,3(F12.8,3X))
C      WRITE(*,*) '4'
C      WRITE(*,*) ''
C      WRITE(*,998) 'H', XX(1,1)*Bohr2A, XX(1,2)*Bohr2A, XX(1,3)*Bohr2A
C      WRITE(*,998) 'H', XX(2,1)*Bohr2A, XX(2,2)*Bohr2A, XX(2,3)*Bohr2A
C      WRITE(*,998) 'O', XX(3,1)*Bohr2A, XX(3,2)*Bohr2A, XX(3,3)*Bohr2A
C      WRITE(*,998) 'O', XX(4,1)*Bohr2A, XX(4,2)*Bohr2A, XX(4,3)*Bohr2A
      WRITE(*,*) 'Now let us test the minimization routines.'
      ROH1 = 1.1
      ROH2 = 0.75
      ROO = 2.0D0
      AHOO1 = 120.0D0
      AHOO2 = 100.0D0
      TAU = 80.0D0
      WRITE(*,*) 'HERE IS A WACKY GEOMETRY (in ang and degrees):'
      WRITE(*,*) 'ROH1=',ROH1
      WRITE(*,*) 'ROH2=',ROH2
      WRITE(*,*) 'ROO =',ROO
      WRITE(*,*) 'AHOO1=',AHOO1
      WRITE(*,*) 'AHOO2=',AHOO2
      WRITE(*,*) 'TAU=',TAU
      ROH1 = ROH1 / Bohr2A
      ROH2 = ROH2 / Bohr2A
      ROO = ROO / Bohr2A
      AHOO1 = AHOO1 * Deg2Rad
      AHOO2 = AHOO2 * Deg2Rad
      TAU = TAU * Deg2Rad
      CALL INT2CART(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,XX)
      CALL hoohpot(XX,PottyOut,iflag)
      WRITE(*,*) 'THE ENERGY OF THE WACKY GEOMETRY IS (in cm**-1):'
      WRITE(*,*) PottyOut
      TOL=1.0D-12
      CALL FULLMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,PottyOut,TOL)
      PottyOut = PottyOut / cmi2har
      WRITE(*,*) 'THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) 'ROH1=',ROH1 * Bohr2A
      WRITE(*,*) 'ROH2=',ROH2 * Bohr2A
      WRITE(*,*) 'ROO =',ROO * Bohr2A
      WRITE(*,*) 'AHOO1=',AHOO1 / Deg2Rad
      WRITE(*,*) 'AHOO2=',AHOO2 / Deg2Rad
      WRITE(*,*) 'TAU=',TAU / Deg2Rad
      WRITE(*,*) 'THE ENERGY OF THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) PottyOut
      WRITE(*,*) 'THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) 'ROH1=',ROH1 * Bohr2A
      WRITE(*,*) 'ROH2=',ROH2 * Bohr2A
      WRITE(*,*) 'ROO =',ROO * Bohr2A
      WRITE(*,*) 'AHOO1=',AHOO1 / Deg2Rad
      WRITE(*,*) 'AHOO2=',AHOO2 / Deg2Rad
      WRITE(*,*) 'TAU=',TAU / Deg2Rad
      WRITE(*,*) 'THE ENERGY OF THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) PottyOut
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)
      flagTAU = 0
      flagROO = 0
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS AND ROT PROJECTED OUT:'
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      flagTAU = 1
      flagROO = 0
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS,ROT,TAU PROJECTED OUT:'
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)     
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      flagTAU = 0
      flagROO = 1
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS,ROT,ROO PROJECTED OUT:'
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)     
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      flagTAU = 1
      flagROO = 1
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS,ROT,ROO,TAU PROJECTED OUT:'
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      WRITE(*,*) 'Let us do this same thing again but freeze ROO.'
      ROH1 = 1.1
      ROH2 = 0.75
      ROO = 2.0D0
      AHOO1 = 120.0D0
      AHOO2 = 100.0D0
      TAU = 80.0D0
      ROH1 = ROH1 / Bohr2A
      ROH2 = ROH2 / Bohr2A
      ROO = ROO / Bohr2A
      AHOO1 = AHOO1 * Deg2Rad
      AHOO2 = AHOO2 * Deg2Rad
      TAU = TAU * Deg2Rad
      CALL ROOMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,PottyOut,TOL)
      PottyOut = PottyOut / cmi2har
      WRITE(*,*) 'THE OPTIMIZED GEOMETRY-FROZEN ROO-IS:'
      WRITE(*,*) 'ROH1=',ROH1 * Bohr2A
      WRITE(*,*) 'ROH2=',ROH2 * Bohr2A
      WRITE(*,*) 'ROO =',ROO * Bohr2A
      WRITE(*,*) 'AHOO1=',AHOO1 / Deg2Rad
      WRITE(*,*) 'AHOO2=',AHOO2 / Deg2Rad
      WRITE(*,*) 'TAU=',TAU / Deg2Rad
      WRITE(*,*) 'THE ENERGY OF THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) PottyOut
      flagTAU = 0
      flagROO = 1
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS,ROT,ROO PROJECTED OUT:'
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)     
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      WRITE(*,*) 'Now freeze TAU and ROO.'
      ROH1 = 1.1
      ROH2 = 0.75
      ROO = 2.0D0
      AHOO1 = 120.0D0
      AHOO2 = 100.0D0
      TAU = 80.0D0
      ROH1 = ROH1 / Bohr2A
      ROH2 = ROH2 / Bohr2A
      ROO = ROO / Bohr2A
      AHOO1 = AHOO1 * Deg2Rad
      AHOO2 = AHOO2 * Deg2Rad
      TAU = TAU * Deg2Rad
      CALL ROOTAUMIN(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,PottyOut,TOL)
      PottyOut = PottyOut / cmi2har
      WRITE(*,*) 'THE OPTIMIZED GEOMETRY-FROZEN ROO & TAU-IS:'
      WRITE(*,*) 'ROH1=',ROH1 * Bohr2A
      WRITE(*,*) 'ROH2=',ROH2 * Bohr2A
      WRITE(*,*) 'ROO =',ROO * Bohr2A
      WRITE(*,*) 'AHOO1=',AHOO1 / Deg2Rad
      WRITE(*,*) 'AHOO2=',AHOO2 / Deg2Rad
      WRITE(*,*) 'TAU=',TAU / Deg2Rad
      WRITE(*,*) 'THE ENERGY OF THE OPTIMIZED GEOMETRY IS:'
      WRITE(*,*) PottyOut
      flagTAU = 1
      flagROO = 1
      WRITE(*,*) 'FREQS OF MINIMUM W/ TRANS,ROT,ROO,TAU PROJECTED OUT:'
      CALL HESS(ROH1,ROH2,ROO,AHOO1,AHOO2,TAU,Step,XX,HESSIAN,II)     
      CALL PROJECT(XX,HESSIAN,II,flagROO,flagTAU)
      END 
