************************************************************************
* ZEB-1/4/2013 -- Added cartesian input array rather than common block
*                 input.
*                 The array XXIN is given as size (1:4,1:3) where      
*                 (atom#,XYZ#).  The atom numbers are H1-O3--O4-H2.   
*                 THE VALUES OF THE COORDINATES IN THE INPUT XIN
*                 MUST BE IN ATOMIC UNITS (bohr)!!!!!
*
*                 SET iflag = 0 for the first call.
*  
*                 Also, PottyOut is a variable containing the 
*                 value of the potential energy on output.
      SUBROUTINE hoohpot(XXIN,PottyOut,iflag)
      implicit double precision (a-h,o-z)
      DIMENSION XXIN(1:4,1:3)
* END HACK ZEB-1/4/2013
C      subroutine hoohpot(iflag)
C
C potential energy surface for HOOH 
C The first call should be made with iflag=0. This causes the initialization
C of some coordinates and parameters, which need to be performed only once.
C After initialization iflag=1 is returned (see below). 
C
C for control purposes, the rounded minimum geometry and the
C corresponding energy are given together with an off-minimum
C geometry/energy ( energy zero = OH + OH, see parameter iref )
C
C Potential5  1.818 1.818 2.745  99.76 80.24 114.30  : -19039.200 cm-1
C             1.7    2.0    2.5    80.0   90.0  100.0   : -6444.674 cm-1
C
C all quantities are expressed in bohr(=ao),cm-1 and rad 
C energy outputs are in cm-1
C
C length: 1ao = 52.9177249 pm
C energy: 1Eh = 219474.63067 cm-1
C m(H)/m(O)   = 0.063009091  = 1.007825/15.99491 (CRC Handbook)
C
C on 64bit computers, all `implicit double precision' statements
C should be removed and all strings  ***d*0*** and ***d*-*0*** occuring
C in the source code should be replaced by ***e*0*** and ***e*-*0***,
C respectively (do not read * in this text, it protects the text
C from the suggested change)
C
C      implicit double precision (a-h,o-z)
      parameter (n=1, ma=135) 
C n=number of simultaneous potential calls
C   if n is large, the time-consuming potential evaluation is vectorizable
C   to a very large extent, reducing the CPU time by typically one order
C   of magnitude. 
C  CAVEAT:  n must have the same value in the parameter statements in the
C           following subroutines hoohpot, hf2p, intcor, intcon
C ma=number of parameters
      parameter (ipotty=6)
C potential flag:  4=morse 5=er 6=rkr  
* ZEB 1/4/2013 -- Changed iext from 1 to 0.  Modified routine to 
*                 except cartesian input array rather than common block.
* ZEB 1/4/2013 -- Changed iref from 1 to 0.  Potential references from
*                 the minimum geometry. 
      parameter(icoor=0,iext=0,iref=1)
C coordinate flag  0=cartesian  
C iext: flag for inputs: if 1, input is via common block
C iref: if 1 then V(Rab->infinity)=0.00cm-1, if 0 then Vmin=0.00cm-1
      parameter (pi=3.1415926535898d0, phicon=0.01745329252d0)
      common/coordi/ x(n,3,4)
      common/intern/ rm1(n),rm2(n),rab(n),th1(n),th2(n),tau(n),
     1               r12(n),r14(n),r23(n),r34(n)
      common/energy/ e(n)
      common/helg/ dip0,dip1,alpa0,alpa1,alper0,alper1,
     *             rmix1,rmix2,rmix3,rmix4,
     *             gpt1,gpt2,gpt3,gpt4,gpt5
      dimension wellde(-20:20)
      dimension vcoup(-20:20),sumr(-20:20),etemp(n),rstemp(n),rs(n)
      data wellde/19*0.d0,2231.044d0,1559.307d0,1545.25d0,
     1            1544.21d0,1544.242d0,17*0.d0/
      data vcoup/30*20000.d0,97000.d0,0.d0,0.d0,20000.d0,7*20000/
      data sumr/30*15.d0,12.0d0,12.d0,12.d0,17.d0,7*15.e0/
* ZEB-1/4/2013 -- Put the input cartesians into the array used 
*                 by the PES routine.
      x(1,1,1) = XXIN(1,1)
      x(1,2,1) = XXIN(1,2)
      x(1,3,1) = XXIN(1,3)
      x(1,1,2) = XXIN(2,1)
      x(1,2,2) = XXIN(2,2)
      x(1,3,2) = XXIN(2,3)
      x(1,1,3) = XXIN(3,1)
      x(1,2,3) = XXIN(3,2)
      x(1,3,3) = XXIN(3,3)
      x(1,1,4) = XXIN(4,1)
      x(1,2,4) = XXIN(4,2)
      x(1,3,4) = XXIN(4,3)
* END OF CODE HACK -- ZEB 1/4/2013
C
C subroutine hf2pp establishes parameters and coordinate
C independent elements of the potential. This call has to be
C made only once for an arbitrary number of potential calls!
C
      if (iflag.eq.0) then 
        if(ipotty.ge.0.or.ipotty.eq.-2)   call hf2pp(ipotty) 
        iflag=1
      endif
C
C
C subroutine intcor calculates all necessary internal
C coordinates, namely r1,r2,r,th1,th2,tau,r12,r23,r13,r14
C and scales them for the potential, where appropriate.
C This is done for n sets of input coordinates at a time.
C
      call intcor(ipotty)
C
C subroutine hf2p calculates the potential energy e as a function
C of properly scaled internal coordinates r1...r14 for n
C sets of such coordinates.
C
      if(ipotty.ge.0)then
        call hf2p(ipotty)
* ZEB - 1/4/2013 -- The calcualted energy is put in the new output
*                   variable PottyOut for output.
        PottyOut = e(1)
* END HACK ZEB 1/4/2013
      endif
C
cb      do 901 i=1,n
C      write(*,*) e(i),etemp(i)
cb  901 continue
      return
      end
C
C *** subroutine intcon ***  converts internal coordinates to cartesians
C
      subroutine intcon(ipotty)
      implicit double precision (a-h,o-z)
      parameter (n=1)
      parameter (zero=0.0d0, one=1.0d0, two=2.0d0, three=3.0d0) 
      parameter (ten=10.0d0, tenth=0.1d0, half=0.5d0)
C mass fraction H/(O+H) for OF center of gravity : romh
      common/coordi/ x(n,3,4)
      common/energy/ e(n)
      common/intern/ rm1(n),rm2(n),rab(n),th1(n),th2(n),tau(n),
     1               r12(n),r14(n),r23(n),r34(n)
      romh=0.059274273d0
      if(ipotty.eq.-1.or.ipotty.eq.-11.or.ipotty.gt.3) romh=zero
      do 510 i=1,n

      x(i,1,3)=zero-rm1(i)*cos(th1(i))*romh
      x(i,2,3)=zero-rm1(i)*sin(th1(i))*romh
      x(i,3,3)=zero

      x(i,1,1)=zero+rm1(i)*cos(th1(i))*(one -romh)
      x(i,2,1)=zero+rm1(i)*sin(th1(i))*(one -romh)
      x(i,3,1)=zero

      x(i,1,4)=rab(i)-rm2(i)*cos(th2(i))*romh
      x(i,2,4)=zero  -rm2(i)*sin(th2(i))*cos(tau(i))*romh
      x(i,3,4)=zero  -rm2(i)*sin(th2(i))*sin(tau(i))*romh

      x(i,1,2)=rab(i)+rm2(i)*cos(th2(i))*(one -romh)
      x(i,2,2)=zero  +rm2(i)*sin(th2(i))*cos(tau(i))*(one -romh)
      x(i,3,2)=zero  +rm2(i)*sin(th2(i))*sin(tau(i))*(one -romh)

  510 continue
      return
      end
C
C *** subroutine intcor ***  computes internal coordinates
C
      subroutine intcor(ipotty)
      implicit double precision (a-h,o-z)
      parameter (n=1)
      parameter (zero=0.0d0, one=1.0d0, two=2.0d0, three=3.0d0) 
      parameter (ten=10.0d0, tenth=0.1d0, half=0.5d0)
      parameter (pi=3.1415926535898d0, phicon=0.01745329252d0)
      parameter (avf=0.9999999999d0)
C helps to avoid acos failures due to round off errors
C set to about 0.999999d0 for 32bit accuracy
C mass fraction H/(O+H) for OF center of gravity 
      common/coordi/ x(n,3,4)
      common/energy/ e(n)
      common/intern/ rm1(n),rm2(n),rab(n),th1(n),th2(n),tau(n),
     1               r12(n),r14(n),r23(n),r34(n)
      common/morcon/ r0,alpha,diss,scal,balo,coup,shi1,shi2
C kintern special for mod(ipotty,10)=4,5,6,7
      common/kintern/ ca(n),th1p(n),th2p(n),taup(n)
      dimension v1(n,3),v2(n,3),va(n,3),vb(n,3),dw(n)
      dimension vab(n,3),vsa(n,3),vsb(n,3),dra(n),drb(n)
      nat=4
      nact1=1
      nact2=2
      romh=0.059274273d0
      if(ipotty.eq.-1.or.ipotty.eq.-11.or.ipotty.gt.3) romh=zero
      if(ipotty.lt.0.or.ipotty.gt.3)then
      shi1=zero
      shi2=zero
      endif
C build angles ...p from H-viewpoint
      if(mod(ipotty,10).gt.3)then
      do 410 i=1,n
      r14(i)=sqrt((x(i,1,nact1)-x(i,1,nat/2+nact2))**2+
     1            (x(i,2,nact1)-x(i,2,nat/2+nact2))**2+
     1            (x(i,3,nact1)-x(i,3,nat/2+nact2))**2)
      r23(i)=sqrt((x(i,1,nact2)-x(i,1,nat/2+nact1))**2+
     1            (x(i,2,nact2)-x(i,2,nat/2+nact1))**2+
     1            (x(i,3,nact2)-x(i,3,nat/2+nact1))**2)
      v1(i,1)=x(i,1,nat/2+nact1)-x(i,1,nact1)
      v1(i,2)=x(i,2,nat/2+nact1)-x(i,2,nact1)
      v1(i,3)=x(i,3,nat/2+nact1)-x(i,3,nact1)
      v2(i,1)=x(i,1,nat/2+nact2)-x(i,1,nact2)
      v2(i,2)=x(i,2,nat/2+nact2)-x(i,2,nact2)
      v2(i,3)=x(i,3,nat/2+nact2)-x(i,3,nact2)
      vab(i,1)=x(i,1,nact2)-x(i,1,nact1)
      vab(i,2)=x(i,2,nact2)-x(i,2,nact1)
      vab(i,3)=x(i,3,nact2)-x(i,3,nact1)
      r34(i)=sqrt(vab(i,1)**2+vab(i,2)**2+
     1            vab(i,3)**2)
      rm1(i)=sqrt(v1(i,1)**2+v1(i,2)**2+v1(i,3)**2)
      rm2(i)=sqrt(v2(i,1)**2+v2(i,2)**2+v2(i,3)**2)
      th1p(i)=acos(avf*((v1(i,1)*vab(i,1)+v1(i,2)*vab(i,2)
     1       +v1(i,3)*vab(i,3))/
     1       (r34(i)*rm1(i))))
      th2p(i)=acos(-avf*((v2(i,1)*vab(i,1)+v2(i,2)*vab(i,2)
     1       +v2(i,3)*vab(i,3))/
     1       (r34(i)*rm2(i))))
      th2p(i)=pi-th2p(i)
      vsa(i,1)=v1(i,3)*vab(i,2)-v1(i,2)*vab(i,3)
      vsa(i,2)=v1(i,1)*vab(i,3)-v1(i,3)*vab(i,1)
      vsa(i,3)=v1(i,2)*vab(i,1)-v1(i,1)*vab(i,2)
      vsb(i,1)=v2(i,3)*vab(i,2)-v2(i,2)*vab(i,3)
      vsb(i,2)=v2(i,1)*vab(i,3)-v2(i,3)*vab(i,1)
      vsb(i,3)=v2(i,2)*vab(i,1)-v2(i,1)*vab(i,2)
      dra(i)=sqrt(vsa(i,1)**2+vsa(i,2)**2+vsa(i,3)**2)
      drb(i)=sqrt(vsb(i,1)**2+vsb(i,2)**2+vsb(i,3)**2)
      dw(i)= v2(i,1)*(vab(i,2)*v1(i,3)-vab(i,3)*v1(i,2))+
     1       v2(i,2)*(vab(i,3)*v1(i,1)-vab(i,1)*v1(i,3))+
     2       v2(i,3)*(vab(i,1)*v1(i,2)-vab(i,2)*v1(i,1)) 
      if(dra(i).eq.zero.or.drb(i).eq.zero) then
      taup(i)=zero
      else
      taup(i)=acos(avf*((vsa(i,1)*vsb(i,1)+vsa(i,2)*vsb(i,2)
     1             +vsa(i,3)*vsb(i,3))/
     1             (dra(i)*drb(i))))
      endif
C build normal angles viewed from F
      v1(i,1)=x(i,1,nact1)-x(i,1,nat/2+nact1)
      v1(i,2)=x(i,2,nact1)-x(i,2,nat/2+nact1)
      v1(i,3)=x(i,3,nact1)-x(i,3,nat/2+nact1)
      v2(i,1)=x(i,1,nact2)-x(i,1,nat/2+nact2)
      v2(i,2)=x(i,2,nact2)-x(i,2,nat/2+nact2)
      v2(i,3)=x(i,3,nact2)-x(i,3,nat/2+nact2)
      vab(i,1)=x(i,1,nat/2+nact2)-x(i,1,nat/2+nact1)
      vab(i,2)=x(i,2,nat/2+nact2)-x(i,2,nat/2+nact1)
      vab(i,3)=x(i,3,nat/2+nact2)-x(i,3,nat/2+nact1)
      r34(i)=sqrt(vab(i,1)**2+vab(i,2)**2+
     1            vab(i,3)**2)
      rab(i)=r34(i)
      r12(i)=sqrt((x(i,1,nact1)-x(i,1,nact2))**2+
     1            (x(i,2,nact1)-x(i,2,nact2))**2+
     1            (x(i,3,nact1)-x(i,3,nact2))**2)
      rm1(i)=sqrt(v1(i,1)**2+v1(i,2)**2+v1(i,3)**2)
      rm2(i)=sqrt(v2(i,1)**2+v2(i,2)**2+v2(i,3)**2)
      ca(i)=(v1(i,1)*v2(i,1)+v1(i,2)*v2(i,2)+v1(i,3)*v2(i,3))/
     1 (rm1(i)*rm2(i))
      th1(i)=acos(avf*((v1(i,1)*vab(i,1)+v1(i,2)*vab(i,2)
     1       +v1(i,3)*vab(i,3))/
     1       (r34(i)*rm1(i))))
      th2(i)=acos(-avf*((v2(i,1)*vab(i,1)+v2(i,2)*vab(i,2)
     1       +v2(i,3)*vab(i,3))/
     1       (r34(i)*rm2(i))))
      th2(i)=pi-th2(i)
      vsa(i,1)=v1(i,3)*vab(i,2)-v1(i,2)*vab(i,3)
      vsa(i,2)=v1(i,1)*vab(i,3)-v1(i,3)*vab(i,1)
      vsa(i,3)=v1(i,2)*vab(i,1)-v1(i,1)*vab(i,2)
      vsb(i,1)=v2(i,3)*vab(i,2)-v2(i,2)*vab(i,3)
      vsb(i,2)=v2(i,1)*vab(i,3)-v2(i,3)*vab(i,1)
      vsb(i,3)=v2(i,2)*vab(i,1)-v2(i,1)*vab(i,2)
      dra(i)=sqrt(vsa(i,1)**2+vsa(i,2)**2+vsa(i,3)**2)
      drb(i)=sqrt(vsb(i,1)**2+vsb(i,2)**2+vsb(i,3)**2)
      dw(i)= v2(i,1)*(vab(i,2)*v1(i,3)-vab(i,3)*v1(i,2))+
     1       v2(i,2)*(vab(i,3)*v1(i,1)-vab(i,1)*v1(i,3))+
     2       v2(i,3)*(vab(i,1)*v1(i,2)-vab(i,2)*v1(i,1)) 
      if(dra(i).eq.zero.or.drb(i).eq.zero) then
      tau(i)=zero
      else
      tau(i)=acos(avf*((vsa(i,1)*vsb(i,1)+vsa(i,2)*vsb(i,2)
     1             +vsa(i,3)*vsb(i,3))/
     1             (dra(i)*drb(i))))
      endif
      tau(i)=sign(tau(i),dw(i))
 410  continue
      else
      do 210 i=1,n
      v1(i,1)=x(i,1,nact1)-x(i,1,nat/2+nact1)
      v1(i,2)=x(i,2,nact1)-x(i,2,nat/2+nact1)
      v1(i,3)=x(i,3,nact1)-x(i,3,nat/2+nact1)
      v2(i,1)=x(i,1,nact2)-x(i,1,nat/2+nact2)
      v2(i,2)=x(i,2,nact2)-x(i,2,nat/2+nact2)
      v2(i,3)=x(i,3,nact2)-x(i,3,nat/2+nact2)
      va(i,1)=x(i,1,nat/2+nact1)+romh*v1(i,1)
      va(i,2)=x(i,2,nat/2+nact1)+romh*v1(i,2)
      va(i,3)=x(i,3,nat/2+nact1)+romh*v1(i,3)
      vb(i,1)=x(i,1,nat/2+nact2)+romh*v2(i,1)
      vb(i,2)=x(i,2,nat/2+nact2)+romh*v2(i,2)
      vb(i,3)=x(i,3,nat/2+nact2)+romh*v2(i,3)
      vab(i,1)=vb(i,1)-va(i,1)
      vab(i,2)=vb(i,2)-va(i,2)
      vab(i,3)=vb(i,3)-va(i,3)
      rm1(i)=sqrt(v1(i,1)**2+v1(i,2)**2+v1(i,3)**2)
      rm2(i)=sqrt(v2(i,1)**2+v2(i,2)**2+v2(i,3)**2)
      rab(i)=sqrt(vab(i,1)**2+vab(i,2)**2+vab(i,3)**2)
      th1(i)=acos(avf*((v1(i,1)*vab(i,1)+v1(i,2)*vab(i,2)
     1       +v1(i,3)*vab(i,3))/
     1       (rab(i)*rm1(i))))
      th2(i)=acos(-avf*((v2(i,1)*vab(i,1)+v2(i,2)*vab(i,2)
     1       +v2(i,3)*vab(i,3))/
     1       (rab(i)*rm2(i))))
      th2(i)=pi-th2(i)
      vsa(i,1)=v1(i,3)*vab(i,2)-v1(i,2)*vab(i,3)
      vsa(i,2)=v1(i,1)*vab(i,3)-v1(i,3)*vab(i,1)
      vsa(i,3)=v1(i,2)*vab(i,1)-v1(i,1)*vab(i,2)
      vsb(i,1)=v2(i,3)*vab(i,2)-v2(i,2)*vab(i,3)
      vsb(i,2)=v2(i,1)*vab(i,3)-v2(i,3)*vab(i,1)
      vsb(i,3)=v2(i,2)*vab(i,1)-v2(i,1)*vab(i,2)
      dra(i)=sqrt(vsa(i,1)**2+vsa(i,2)**2+vsa(i,3)**2)
      drb(i)=sqrt(vsb(i,1)**2+vsb(i,2)**2+vsb(i,3)**2)
      tau(i)=acos(avf*((vsa(i,1)*vsb(i,1)+vsa(i,2)*vsb(i,2)
     1             +vsa(i,3)*vsb(i,3))/
     1             (dra(i)*drb(i))))
      dw(i)= v2(i,1)*(vab(i,2)*v1(i,3)-vab(i,3)*v1(i,2))+
     1       v2(i,2)*(vab(i,3)*v1(i,1)-vab(i,1)*v1(i,3))+
     2       v2(i,3)*(vab(i,1)*v1(i,2)-vab(i,2)*v1(i,1)) 
      tau(i)=sign(tau(i),dw(i))
  210 continue
      if(mod(ipotty,10).eq.0)then
      do 207 i=1,n
      r12(i)=sqrt((x(i,1,nact1)-x(i,1,nact2))**2+
     1            (x(i,2,nact1)-x(i,2,nact2))**2+
     2            (x(i,3,nact1)-x(i,3,nact2))**2)
      r14(i)=sqrt((x(i,1,nact1)-x(i,1,nat/2+nact2))**2+
     1            (x(i,2,nact1)-x(i,2,nat/2+nact2))**2+
     2            (x(i,3,nact1)-x(i,3,nat/2+nact2))**2)
      r23(i)=sqrt((x(i,1,nat/2+nact1)-x(i,1,nact2))**2+
     1            (x(i,2,nat/2+nact1)-x(i,2,nact2))**2+
     2            (x(i,3,nat/2+nact1)-x(i,3,nact2))**2)
      r34(i)=sqrt((x(i,1,nat/2+nact1)-x(i,1,nat/2+nact2))**2+
     1            (x(i,2,nat/2+nact1)-x(i,2,nat/2+nact2))**2+
     2            (x(i,3,nat/2+nact1)-x(i,3,nat/2+nact2))**2)
  207 continue
      else
      do 208 i=1,n
      dra(i)=shi1+shi2*((rm1(i)-r0)**2/(1.+(rm1(i)-r0)**2)
     1            +(rm2(i)-r0)**2/(1.+(rm2(i)-r0)**2))
      vab(i,1)=vab(i,1)*dra(i)
      vab(i,2)=vab(i,2)*dra(i)
      vab(i,3)=vab(i,3)*dra(i)
      r12(i)=sqrt((x(i,1,nact1)-x(i,1,nact2)-vab(i,1))**2+
     1            (x(i,2,nact1)-x(i,2,nact2)-vab(i,2))**2+
     2            (x(i,3,nact1)-x(i,3,nact2)-vab(i,3))**2)
      r14(i)=sqrt((x(i,1,nact1)-x(i,1,nat/2+nact2)-vab(i,1))**2+
     1            (x(i,2,nact1)-x(i,2,nat/2+nact2)-vab(i,2))**2+
     2            (x(i,3,nact1)-x(i,3,nat/2+nact2)-vab(i,3))**2)
      r23(i)=sqrt((x(i,1,nat/2+nact1)-x(i,1,nact2)-vab(i,1))**2+
     1            (x(i,2,nat/2+nact1)-x(i,2,nact2)-vab(i,2))**2+
     2            (x(i,3,nat/2+nact1)-x(i,3,nact2)-vab(i,3))**2)
      r34(i)=sqrt((x(i,1,nat/2+nact1)-x(i,1,nat/2+nact2)-vab(i,1))**2+
     1            (x(i,2,nat/2+nact1)-x(i,2,nat/2+nact2)-vab(i,2))**2+
     2            (x(i,3,nat/2+nact1)-x(i,3,nat/2+nact2)-vab(i,3))**2)
      rab(i)=rab(i)*(1.d0+dra(i))
  208 continue
      endif
      endif
      return
      end
  
C 
C *** subroutine hf2pp *** prepares computation of hooh potential
C
      subroutine hf2pp(ipotty)
      implicit double precision (a-h,o-z)
      parameter(ma=135)
      parameter (zero=0.0d0, one=1.0d0, two=2.0d0, three=3.0d0) 
      parameter (ten=10.0d0, tenth=0.1d0, half=0.5d0)
      parameter (pi=3.1415926535898d0, phicon=0.01745329252d0)
      dimension fac(0:8)
      dimension fct(31),efct(31)
      common/morcon/ r0,alpha,diss,scal,balo,coup,shi1,shi2
      common/helg/ dip0,dip1,alpa0,alpa1,alper0,alper1,
     *             rmix1,rmix2,rmix3,rmix4,
     *             gpt1,gpt2,gpt3,gpt4,gpt5
      common/ang/ g(0:4,0:4,0:8,0:4),pf(0:4,0:4)
      common/par/ a(ma),ac(3,0:4,0:4,0:8)
      common/erf/ rzero,diss_er,er1,er2,er3
C
C note: this routine supplies g(...)*pif instead of g(...)
C
      pif=half/sqrt(pi)

C
C define parameters for the surface
C
      do 750 j=1,ma
      a(j)=0.d0
  750 continue
      if(mod(ipotty,10).gt.3.and.mod(ipotty,10).lt.7)then
C ipotty=4/5/6
CC&&&&&&&&&&&&&&&&&&&&&&&insert here program segment&&&&&&&&&&&&&&&&&&&&&&&
      a(  1)=    0.196543964d+06
      a(  2)=   -0.131666442d+04
      a(  3)=    0.837044500d+05
      a(  4)=   -0.149747505d+06
      a(  5)=   -0.527050194d+05
      a(  6)=    0.350818052d+05
      a(  7)=    0.382537701d+04
      a(  8)=   -0.327620475d+06
      a(  9)=    0.490160836d+05
      a( 10)=   -0.223057825d-01
      a( 11)=    0.116565972d+00
      a( 12)=    0.211894467d+08
      a( 13)=   -0.526060821d+07
      a( 14)=   -0.206741468d+07
      a( 15)=   -0.193004413d+07
      a( 16)=    0.183000000d+01
      a( 17)=    0.160000000d+01
      a( 18)=    0.100000000d+01
      a( 19)=    0.150000000d+08
      a( 20)=    0.000000000d+00
      a( 21)=    0.461976559d+08
      a( 22)=    0.251926124d+08
      a( 23)=    0.424778009d+09
      a( 24)=    0.755095027d+06
      a( 25)=   -0.772008741d+04
      a( 26)=    0.740922868d+07
      a( 27)=   -0.369988523d+08
      a( 28)=   -0.823707657d+08
      a( 29)=   -0.540506371d+05
      a( 30)=    0.104195391d+06
      a( 31)=   -0.330210452d+05
      a( 32)=   -0.541064130d+03
      a( 33)=    0.000000000d+00
      a( 34)=    0.107410700d+06
      a( 35)=    0.000000000d+00
      a( 36)=    0.130248320d+03
      a( 37)=   -0.284361758d+04
      a( 38)=    0.179613872d+07
      a( 39)=   -0.711836973d+05
      a( 40)=    0.280395298d+05
      a( 41)=    0.000000000d+00
      a( 42)=   -0.213490376d+08
      a( 43)=    0.278614559d+05
      a( 44)=   -0.891985799d+04
      a( 45)=    0.108827116d+03
      a( 46)=   -0.803105603d+05
      a( 47)=    0.293022806d+05
      a( 48)=    0.000000000d+00
      a( 49)=    0.000000000d+00
      a( 50)=   -0.228309866d-01
      a( 51)=   -0.385861114d+06
      a( 52)=   -0.196407476d+06
      a( 53)=   -0.993602418d+02
      a( 54)=    0.650765201d-01
      a( 55)=   -0.136424502d+07
      a( 56)=    0.637064907d+06
      a( 57)=   -0.104158439d+06
      a( 58)=   -0.539453504d+05
      a( 59)=    0.521120868d+04
      a( 60)=    0.170938939d+03
      a( 61)=    0.141454431d+04
      a( 62)=    0.119841217d+04
      a( 63)=    0.000000000d+00
      a( 64)=   -0.507017882d+06
      a( 65)=   -0.248633656d+07
      a( 66)=    0.000000000d+00
      a( 67)=   -0.593190302d+02
      a( 68)=    0.000000000d+00
      a( 69)=    0.366534356d+06
      a( 70)=   -0.626395242d+05
      a( 71)=   -0.134610811d+06
      a( 72)=   -0.528694162d+06
      a( 73)=    0.000000000d+00
      a( 74)=   -0.348438076d+03
      a( 75)=   -0.592964582d+05
      a( 76)=    0.000000000d+00
      a( 77)=    0.000000000d+00
      a( 78)=   -0.795943568d+05
      a( 79)=    0.000000000d+00
      a( 80)=    0.384009990d+00
      a( 81)=   -0.138535250d+02
      a( 82)=    0.000000000d+00
      a( 83)=    0.500977319d+03
      a( 84)=    0.000000000d+00
      a( 85)=    0.000000000d+00
      a( 86)=    0.454039915d+02
      a( 87)=    0.144574472d+02
      a( 88)=   -0.218793761d+05
      a( 89)=    0.000000000d+00
      a( 90)=    0.000000000d+00
      a( 91)=   -0.492339484d+06
      a( 92)=   -0.743815089d+05
      a( 93)=    0.000000000d+00
      a( 94)=    0.000000000d+00
      a( 95)=    0.000000000d+00
      a( 96)=    0.000000000d+00
      a( 97)=    0.000000000d+00
      a( 98)=    0.000000000d+00
      a( 99)=    0.000000000d+00
      a(100)=    0.000000000d+00
      a(101)=    0.000000000d+00
      a(102)=    0.000000000d+00
      a(103)=    0.000000000d+00
      a(104)=    0.000000000d+00
      a(105)=    0.000000000d+00
      a(106)=    0.000000000d+00
      a(107)=    0.000000000d+00
      a(108)=    0.000000000d+00
      a(109)=    0.000000000d+00
      a(110)=    0.000000000d+00
      a(111)=    0.000000000d+00
      a(112)=    0.000000000d+00
      a(113)=    0.000000000d+00
      a(114)=    0.000000000d+00
      a(115)=    0.000000000d+00
      a(116)=    0.000000000d+00
      a(117)=    0.000000000d+00
      a(118)=    0.000000000d+00
      a(119)=    0.000000000d+00
      a(120)=    0.000000000d+00
      a(121)=    0.000000000d+00
      a(122)=    0.000000000d+00
      a(123)=    0.000000000d+00
      a(124)=    0.000000000d+00
      a(125)=    0.000000000d+00
      a(126)=    0.000000000d+00
      a(127)=    0.000000000d+00
      a(128)=    0.000000000d+00
      a(129)=    0.000000000d+00
      a(130)=    0.000000000d+00
      a(131)=    0.000000000d+00
      a(132)=    0.000000000d+00
      a(133)=    0.000000000d+00
      a(134)=    0.000000000d+00
      a(135)=    0.000000000d+00
      rmix1 =  0.0000000000d+00
      rmix2 =  0.0000000000d+00
      rmix3 =  0.0000000000d+00
      rmix4 =  0.0000000000d+00
      dip0  =   0.168291809d+01
      dip1  =   0.238034888d+00
      alpa0 =   0.942000000d+01
      alpa1 =   0.726000000d+01
      alper0=   0.702000000d+01
      alper1=   0.182000000d+01
      g(1,1,0,1)=   0.200000000d+01
      g(1,1,2,1)=  -0.100000000d+01
      g(1,2,1,1)=   0.173205081d+01
      g(1,2,3,1)=  -0.115470054d+01
      g(2,1,1,1)=   0.173205081d+01
      g(2,1,3,1)=  -0.115470054d+01
      g(1,3,2,1)=   0.163299316d+01
      g(1,3,4,1)=  -0.122474487d+01
      g(2,2,0,1)=   0.200000000d+01
      g(2,2,0,2)=   0.200000000d+01
      g(2,2,2,1)=   0.100000000d+01
      g(2,2,2,2)=  -0.200000000d+01
      g(2,2,4,1)=  -0.133333333d+01
      g(2,2,4,2)=   0.333333333d+00
      g(3,1,2,1)=   0.163299316d+01
      g(3,1,4,1)=  -0.122474487d+01
      g(1,4,3,1)=   0.158113883d+01
      g(1,4,5,1)=  -0.126491106d+01
      g(2,3,1,1)=   0.188561808d+01
      g(2,3,1,2)=   0.149071198d+01
      g(2,3,3,1)=   0.707106781d+00
      g(2,3,3,2)=  -0.223606798d+01
      g(2,3,5,1)=  -0.141421356d+01
      g(2,3,5,2)=   0.447213595d+00
      g(3,2,1,1)=   0.188561808d+01
      g(3,2,1,2)=   0.149071198d+01
      g(3,2,3,1)=   0.707106781d+00
      g(3,2,3,2)=  -0.223606798d+01
      g(3,2,5,1)=  -0.141421356d+01
      g(3,2,5,2)=   0.447213595d+00
      g(4,1,3,1)=   0.158113883d+01
      g(4,1,5,1)=  -0.126491106d+01
      g(2,4,2,1)=   0.182574186d+01
      g(2,4,2,2)=   0.129099445d+01
      g(2,4,4,1)=   0.547722558d+00
      g(2,4,4,2)=  -0.232379001d+01
      g(2,4,6,1)=  -0.146059349d+01
      g(2,4,6,2)=   0.516397779d+00
      g(3,3,0,1)=   0.200000000d+01
      g(3,3,0,2)=   0.200000000d+01
      g(3,3,0,3)=   0.200000000d+01
      g(3,3,2,1)=   0.150000000d+01
      g(3,3,2,2)=   0.000000000d+00
      g(3,3,2,3)=  -0.250000000d+01
      g(3,3,4,1)=   0.333333333d+00
      g(3,3,4,2)=  -0.233333333d+01
      g(3,3,4,3)=   0.100000000d+01
      g(3,3,6,1)=  -0.150000000d+01
      g(3,3,6,2)=   0.600000000d+00
      g(3,3,6,3)=  -0.100000000d+00
      g(4,2,2,1)=   0.182574186d+01
      g(4,2,2,2)=   0.129099445d+01
      g(4,2,4,1)=   0.547722558d+00
      g(4,2,4,2)=  -0.232379001d+01
      g(4,2,6,1)=  -0.146059349d+01
      g(4,2,6,2)=   0.516397779d+00
      g(3,4,1,1)=   0.193649167d+01
      g(3,4,1,2)=   0.173205081d+01
      g(3,4,1,3)=   0.132287566d+01
      g(3,4,3,1)=   0.129099445d+01
      g(3,4,3,2)=  -0.577350269d+00
      g(3,4,3,3)=  -0.264575131d+01
      g(3,4,5,1)=   0.129099445d+00
      g(3,4,5,2)=  -0.230940108d+01
      g(3,4,5,3)=   0.132287566d+01
      g(3,4,7,1)=  -0.154919334d+01
      g(3,4,7,2)=   0.692820323d+00
      g(3,4,7,3)=  -0.151185789d+00
      g(4,3,1,1)=   0.193649167d+01
      g(4,3,1,2)=   0.173205081d+01
      g(4,3,1,3)=   0.132287566d+01
      g(4,3,3,1)=   0.129099445d+01
      g(4,3,3,2)=  -0.577350269d+00
      g(4,3,3,3)=  -0.264575131d+01
      g(4,3,5,1)=   0.129099445d+00
      g(4,3,5,2)=  -0.230940108d+01
      g(4,3,5,3)=   0.132287566d+01
      g(4,3,7,1)=  -0.154919334d+01
      g(4,3,7,2)=   0.692820323d+00
      g(4,3,7,3)=  -0.151185789d+00
      g(4,4,0,1)=   0.200000000d+01
      g(4,4,0,2)=   0.200000000d+01
      g(4,4,0,3)=   0.200000000d+01
      g(4,4,0,4)=   0.200000000d+01
      g(4,4,2,1)=   0.170000000d+01
      g(4,4,2,2)=   0.800000000d+00
      g(4,4,2,3)=  -0.700000000d+00
      g(4,4,2,4)=  -0.280000000d+01
      g(4,4,4,1)=   0.100000000d+01
      g(4,4,4,2)=  -0.122222222d+01
      g(4,4,4,3)=  -0.233333333d+01
      g(4,4,4,4)=   0.155555556d+01
      g(4,4,6,1)=  -0.100000000d+00
      g(4,4,6,2)=  -0.220000000d+01
      g(4,4,6,3)=   0.170000000d+01
      g(4,4,6,4)=  -0.400000000d+00
      g(4,4,8,1)=  -0.160000000d+01
      g(4,4,8,2)=   0.800000000d+00
      g(4,4,8,3)=  -0.228571429d+00
      g(4,4,8,4)=   0.285714286d-01
      pf(0,0)=   0.282094792d+00
      pf(1,0)=   0.488602512d+00
      pf(1,1)=  -0.345494149d+00
      pf(2,0)=   0.630783131d+00
      pf(2,1)=  -0.257516135d+00
      pf(2,2)=   0.128758067d+00
      pf(3,0)=   0.746352665d+00
      pf(3,1)=  -0.215453456d+00
      pf(3,2)=   0.681323651d-01
      pf(3,3)=  -0.278149216d-01
      pf(4,0)=   0.846284375d+00
      pf(4,1)=  -0.189234939d+00
      pf(4,2)=   0.446031029d-01
      pf(4,3)=  -0.119206807d-01
      pf(4,4)=   0.421459707d-02
C&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
C empirischer Morse fuer OH
       r0=1.832d0
       alpha=1.1419d0
       diss=42124.73d0
       gpt1=0.5847305d0
       gpt2=1.28d0**2
       gpt3=953320.3d0
       gpt4=164766.75d0
       gpt5=49656.989942d0
C empirische Extended Rydberg Funktion fuer OH
       rzero=1.832d0
       diss_er=37269.0d0
       er1=2.385d0
       er2=1.368d0
       er3=0.562d0
C GPT MP2
C       r0=1.73916d0
C       alpha=1.16937d0
C       diss=49652.7d0
C       gpt1=0.581d0
C       gpt2=1.286d0**2
C       gpt3=949700.d0
C       gpt4=159771.d0
C       gpt5=49947.015d0
C GPT R12
C       r0=1.7370d0
C       alpha=1.16937d0
C       diss=49652.7d0
C       gpt1=0.5793d0
C       gpt2=1.28d0**2
C       gpt3=951322.5d0
C       gpt4=160317.d0
C       gpt5=50442.629d0
       scal=1.d0
       balo=9.5d0
       coup=50000.d0
       shi1=zero
       shi2=zero
      endif
      return
      end
C
C *** subroutine hf2p *** computes the potential energy for n configurations
C
      subroutine hf2p(ipotty)
      implicit double precision (a-h,o-z)
      parameter(n=1,ma=135,nat=4)
C nat is the number of atoms 
C this is needed in cofact to correct the one body term
      parameter (zero=0.0d0,one=1.0d0,two=2.0d0,three=3.0d0,oha=1.5d0)
      parameter (ten=10.0d0,tenth=0.1d0,half=0.5d0,third=1.d0/3.d0)
      parameter (six=6.0d0)
      parameter (pi=3.1415926535898d0,phicon=0.01745329252d0)
      dimension p1(n,0:4,0:4),p2(n,0:4,0:4),cs(n,4)
      dimension ar(n,0:26),ar4(n,0:4,0:4,0:8)
      dimension vp(n,0:26),vp4(n,0:4,0:4,0:8)
      dimension s(n),s1(n),s2(n),sq(n),s3(n),s4(n)
      dimension c(n),c1(n),c2(n),cq(n),c3(n),c4(n),cl1(n),cl2(n)
      dimension c1p(n),c2p(n),ctp(n),s1p(n),s2p(n)
      dimension f014(n)
      dimension f023(n)
      dimension f0h(n)
      dimension f0f(n),f1f(n),f2f(n),f3f(n)
      dimension f4f(n),f5f(n),f6f(n),f7f(n),f8f(n),f12f(n)
      dimension rff3(n),rff6(n)
      dimension tdr1(n),tdr2(n)
      dimension qu1(n),qu2(n),ad1(n),ad2(n)
      dimension ap1(n),ap2(n),amu1(n),amu2(n)
      dimension dab1(n),dab2(n),dab3(n)
      dimension du(n),dv(n),dup(n),dvp(n),ds(n)
      dimension rr3(n),rr4(n),rr5(n),rr6(n),rr8(n),rr10(n)
      dimension r1m(n),r2m(n)
      common/morcon/ r0,alpha,diss,scal,balo,coup,shi1,shi2
      common/energy/ e(n)
      common/intern/  r1(n), r2(n),  r(n),th1(n),th2(n),tau(n),
     1               r12(n),r14(n),r23(n),r34(n)
C kintern special for mod(ipotty,10)=4,5,6,7
      common/kintern/ ca(n),th1p(n),th2p(n),taup(n)
      common/helg/ dip0,dip1,alpa0,alpa1,alper0,alper1,
     *             rmix1,rmix2,rmix3,rmix4,
     *             gpt1,gpt2,gpt3,gpt4,gpt5
      common/ang/ g(0:4,0:4,0:8,0:4),pf(0:4,0:4)
      common/par/ a(ma),ac(3,0:4,0:4,0:8)
      common/erf/ rzero,diss_er,er1,er2,er3
      pif=half/sqrt(pi)
CCCC
      if(mod(ipotty,10).eq.4.or.mod(ipotty,10).eq.5.or.
     *         mod(ipotty,10).eq.6)  then
      shdd=100.d0
      thirdo=third
      iso1=1
      iso2=0
      endif
C calculate legendre polynomials
      do 4100 id=1,n
      s1(id)=sin(th1(id))
      c1(id)=cos(th1(id))
      sq(id)=s1(id)*s1(id)
      s3(id)=sq(id)*s1(id)
      s4(id)=s3(id)*s1(id)
      cq(id)=c1(id)*c1(id)
      c3(id)=cq(id)*c1(id)
      c4(id)=c3(id)*c1(id)
      p1(id,0,0)=pf(0,0)
      p1(id,1,0)=pf(1,0)*c1(id)
      p1(id,2,0)=pf(2,0)*0.5d0*(3*cq(id)-1)
      p1(id,3,0)=pf(3,0)*0.5d0*(5*c3(id)-3*c1(id))
      p1(id,4,0)=pf(4,0)*0.125d0*(35*c4(id)-30*cq(id)+3)
      p1(id,1,1)=pf(1,1)*s1(id)
      p1(id,2,1)=pf(2,1)*3*s1(id)*c1(id)
      p1(id,3,1)=pf(3,1)*1.5d0*s1(id)*(5*cq(id)-1)
      p1(id,4,1)=pf(4,1)*2.5d0*s1(id)*(7*c3(id)-3*c1(id))
      p1(id,2,2)=pf(2,2)*3*sq(id)
      p1(id,3,2)=pf(3,2)*15*sq(id)*c1(id)
      p1(id,4,2)=pf(4,2)*7.5d0*sq(id)*(7*cq(id)-1)
      p1(id,3,3)=pf(3,3)*15*s3(id)
      p1(id,4,3)=pf(4,3)*105*s3(id)*c1(id)
      p1(id,4,4)=pf(4,4)*105*s4(id)
      s2(id)=sin(th2(id))
      c2(id)=cos(th2(id))
      sq(id)=s2(id)*s2(id)
      s3(id)=sq(id)*s2(id)
      s4(id)=s3(id)*s2(id)
      cq(id)=c2(id)*c2(id)
      c3(id)=cq(id)*c2(id)
      c4(id)=c3(id)*c2(id)
      p2(id,0,0)=pf(0,0)
      p2(id,1,0)=pf(1,0)*c2(id)
      p2(id,2,0)=pf(2,0)*0.5d0*(3*cq(id)-1)
      p2(id,3,0)=pf(3,0)*0.5d0*(5*c3(id)-3*c2(id))
      p2(id,4,0)=pf(4,0)*0.125d0*(35*c4(id)-30*cq(id)+3)
      p2(id,1,1)=pf(1,1)*s2(id)
      p2(id,2,1)=pf(2,1)*3*s2(id)*c2(id)
      p2(id,3,1)=pf(3,1)*1.5d0*s2(id)*(5*cq(id)-1)
      p2(id,4,1)=pf(4,1)*2.5d0*s2(id)*(7*c3(id)-3*c2(id))
      p2(id,2,2)=pf(2,2)*3*sq(id)
      p2(id,3,2)=pf(3,2)*15*sq(id)*c2(id)
      p2(id,4,2)=pf(4,2)*7.5d0*sq(id)*(7*cq(id)-1)
      p2(id,3,3)=pf(3,3)*15*s3(id)
      p2(id,4,3)=pf(4,3)*105*s3(id)*c2(id)
      p2(id,4,4)=pf(4,4)*105*s4(id)
      do j=1,4
        cs(id,j)=cos(j*tau(id))
      enddo

      c1p(id)=cos(th1p(id))
      c2p(id)=cos(th2p(id))
      ctp(id)=cos(taup(id))
      s1p(id)=sin(th1p(id))
      s2p(id)=sin(th2p(id))
      f014(id)=exp(-two*r14(id))
      f023(id)=exp(-two*r23(id))
      f0h(id)=exp(-two*r12(id))
      f0f(id)=exp(-two*r34(id))
      f1f(id)=f0f(id)*r34(id)
      f2f(id)=f1f(id)*r34(id)
      f3f(id)=f2f(id)*r34(id)
      f4f(id)=f3f(id)*r34(id)
      f5f(id)=f4f(id)*r34(id)
      f6f(id)=f5f(id)*r34(id)
      f7f(id)=f6f(id)*r34(id)
      f8f(id)=f7f(id)*r34(id)
      f12f(id)=f8f(id)*r34(id)**4
      rff3(id)=r34(id)**3
      rff6(id)=rff3(id)**2
      tdr1(id)=tanh(r1(id)-1.82d0)
      tdr2(id)=tanh(r2(id)-1.82d0)
C dipoles
      amu1(id)=a(18)*(dip0+dip1*tdr1(id))
      amu2(id)=a(18)*(dip0+dip1*tdr2(id))
c quadrupoles
      qu1(id)=a(16)+a(17)*tdr1(id)
      qu2(id)=a(16)+a(17)*tdr2(id)
c perpendicular polarizability
cb      ap1(id)=alper0+tdr1(id)*alper1
cb      ap2(id)=alper0+tdr2(id)*alper1
c parallel-perpendicular polarizability
cb      ad1(id)=(alpa0+tdr1(id)*alpa1-ap1(id))
cb      ad2(id)=(alpa0+tdr2(id)*alpa1-ap2(id))
c HH OO OH terms:
      y=0.d0
c --------------
      y=y+a(20)
     *  +a(1)*f0h(id)
     *  +a(2)*f0h(id)*r12(id)**5*(tdr1(id)-tdr2(id))**2
     *  +a(3)*f0h(id)*r12(id)*
     *   (((3*c2p(id)**2-1)*c1p(id)-2*s2p(id)*c2p(id)*s1p(id)*ctp(id))
     *   -((3*c1p(id)**2-1)*c2p(id)-2*s1p(id)*c1p(id)*s2p(id)*ctp(id)))
     *  +f0h(id)*r12(id)**2*(a(4)+a(5)*(tdr1(id)+tdr2(id)))*
     *   (c1p(id)*c2p(id)-half*s1p(id)*s2p(id)*ctp(id))
      y=y+f3f(id)*(a(6)+a(7)*(tdr1(id)+tdr2(id))**2)*
     *   (c1p(id)*c2p(id)-half*s1p(id)*s2p(id)*ctp(id))
      y=y+f2f(id)*(a(8)+a(9)*(tdr1(id)+tdr2(id)))
      y=y+a(10)*f12f(id)
      y=y+a(11)*f8f(id)**three
      y=y+f0f(id)**oha*(a(12)+a(13)*(tdr1(id)+tdr2(id))
     *  +a(14)*(tdr1(id)+tdr2(id))**2+a(15)*(tdr1(id)-tdr2(id))**2)
      y=y+f3f(id)**4*(a(21)+a(22)*(tdr1(id)+tdr2(id)))
      y=y+a(23)*f0f(id)**3
      y=y+a(24)*f014(id)
      y=y+a(24)*f023(id)
      y=y+a(25)*(f014(id)*r14(id)**5)**3
      y=y+a(25)*(f023(id)*r23(id)**5)**3
      y=y-187.d0*(1-tanh(5.d0*(r34(id)-4.d0)))  
c physical terms:
c --------------
      y=y+
c D-D:
     *   (-6.79437d04*amu1(id)*amu2(id)/(rff3(id)+105))
     *   *(c1(id)*c2(id)-half*s1(id)*s2(id)*cs(id,1))
c D-Q:
     *   +9.62963d04/(r34(id)**4+498)*
     *    (amu1(id)*qu2(id)*( 
     *    (3*c2(id)**2-1)*c1(id)-2*s2(id)*c2(id)*s1(id)*cs(id,1))+
     *     qu1(id)*amu2(id)*(
     *   -(3*c1(id)**2-1)*c2(id)+2*s1(id)*c1(id)*s2(id)*cs(id,1)))
c Q-Q:
     *   +9.09868d04*qu1(id)*qu2(id)/(r34(id)**5+516)*
     *    (1-5*c1(id)**2-5*c2(id)**2-15*c1(id)**2*c2(id)**2+
     *     2*(4*c1(id)*c2(id)-s1(id)*s2(id)*cs(id,1))**2)
c polaris isotropic:
cb     *   -half*33972.d0/(rff6(id)+10**5)*
cb     *    (amu1(id)**2*ap2(id)*(3*c1(id)**2+1)
cb     *    +amu2(id)**2*ap1(id)*(3*c2(id)**2+1))
c polaris anisotropic:
cb     *   -half*33972.d0/(rff6(id)+10**5)*
cb     *    (amu1(id)**2*ad2(id)+amu2(id)**2*ad1(id))
cb     *    *(ca(id)+3*c2(id)*c1(id))**2
c dispersion:
     *   -a(19)/(rff6(id)+11100)*(1+(tdr1(id)+tdr2(id)))
      e(id)=y
c angular radial expansion terms:
c ------------------------------
      ar4(id,0,0,0)=zero
      ar4(id,1,4,3)=zero
      ar4(id,4,1,3)=zero
      ar4(id,1,4,5)=zero
      ar4(id,4,1,5)=zero
      ar4(id,1,3,2)=zero
      ar4(id,3,1,2)=zero
      ar4(id,2,2,0)=zero
      ar4(id,3,3,4)=zero
      ar4(id,3,4,1)=zero
      ar4(id,4,3,1)=zero
      ar4(id,3,4,5)=zero
      ar4(id,4,3,5)=zero
      ar4(id,3,4,3)=zero
      ar4(id,4,3,3)=zero
      ar4(id,4,4,6)=zero
      ar4(id,4,4,8)=zero
C1
      ar4(id,0,1,1)=a(26)*f1f(id)*tdr2(id)
     * + f3f(id)**three*(a(27)+a(28)*tdr2(id))
     * + f5f(id)**three*a(88)*tdr2(id)**2
     * + a(91)*f2f(id)*(1+tanh(3.*(r2(id)-3.00d0)))
      ar4(id,1,0,1)=-a(26)*f1f(id)*tdr1(id)
     * - f3f(id)**three*(a(27)+a(28)*tdr1(id))
     * - f5f(id)**three*a(88)*tdr1(id)**2
     * - a(91)*f2f(id)*(1+tanh(3.*(r1(id)-3.00d0)))
C2
      ar4(id,0,2,2)=a(29)*f3f(id)*tdr1(id)
     * + f5f(id)*(a(30)+a(31)*tdr2(id))
     * + a(32)*f8f(id)
      ar4(id,2,0,2)=a(29)*f3f(id)*tdr2(id)
     * + f5f(id)*(a(30)+a(31)*tdr1(id))
     * + a(32)*f8f(id)
C3
      ar4(id,1,1,0)=a(33)*f8f(id)
C4
      ar4(id,1,1,2)=a(34)*f1f(id)*(tdr1(id)-tdr2(id))**2
     * + f8f(id)*(a(35)+a(36)*(tdr2(id)+tdr1(id)))
     * + a(37)*f3f(id)**third
C5
      ar4(id,0,3,3)=a(38)*f2f(id)
     * + f5f(id)*(a(39)+a(40)*tdr2(id))
     * + a(41)*f12f(id)
     * + a(42)*f3f(id)**three
     * + a(43)*f3f(id)**third
     * + a(80)*f8f(id)**three
      ar4(id,3,0,3)=-a(38)*f2f(id)
     * - f5f(id)*(a(39)+a(40)*tdr1(id))
     * - a(41)*f12f(id)
     * - a(42)*f3f(id)**three
     * - a(43)*f3f(id)**third
     * - a(80)*f8f(id)**three
C6
      ar4(id,1,2,1)=a(44)*f5f(id)+a(45)*f8f(id)
      ar4(id,2,1,1)=-a(44)*f5f(id)-a(45)*f8f(id)
C7
      ar4(id,1,2,3)=f3f(id)*(a(46)+a(47)*tdr1(id))
      ar4(id,2,1,3)=-f3f(id)*(a(46)+a(47)*tdr2(id))
C8
      ar4(id,0,4,4)=f1f(id)*(a(48)+a(49)*tdr2(id))
      ar4(id,4,0,4)=f1f(id)*(a(48)+a(49)*tdr1(id))
C9
      ar4(id,1,3,4)=a(50)*f12f(id)
      ar4(id,3,1,4)=a(50)*f12f(id)
C10
      ar4(id,2,2,2)=f1f(id)*(a(51)+a(52)*(tdr2(id)+tdr1(id)))
     * + a(53)*f8f(id)
     * + a(54)*f12f(id)
C13
      ar4(id,2,2,4)=a(55)*f2f(id)
     * + f3f(id)*(a(56)+a(57)*(tdr2(id)+tdr1(id)))
     * + f5f(id)*(a(58)+a(59)*(tdr1(id)-tdr2(id))**2)
     * + a(60)*f8f(id)
C14
      ar4(id,2,3,1)=a(86)*f8f(id)
      ar4(id,3,2,1)=-a(86)*f8f(id)
C15
      ar4(id,2,3,3)=f5f(id)*(a(61)+a(62)*tdr2(id))
     * + f3f(id)**three*(a(63)+a(64)*tdr1(id)
     *    +a(65)*tdr2(id)**2+a(66)*tdr1(id)**2)
     * + a(81)*f6f(id)**six*exp(-2.d0*(r12(id)-4.5d0)**2)
     * + a(92)*f2f(id)*(1+tanh(3.*(r1(id)-3.00d0)))
      ar4(id,3,2,3)=-f5f(id)*(a(61)+a(62)*tdr1(id))
     * - f3f(id)**three*(a(63)+a(64)*tdr2(id)
     *    +a(65)*tdr1(id)**2+a(66)*tdr2(id)**2)
     * - a(81)*f6f(id)**six*exp(-2.d0*(r12(id)-4.5d0)**2)
     * - a(92)*f2f(id)*(1+tanh(3.*(r2(id)-3.00d0)))
C16
      ar4(id,2,3,5)=a(67)*f8f(id)
      ar4(id,3,2,5)=-a(67)*f8f(id)
C17
      ar4(id,2,4,2)=a(68)*f5f(id)*tdr2(id)
     * + a(69)*f3f(id)**three
      ar4(id,4,2,2)=a(68)*f5f(id)*tdr1(id)
     * + a(69)*f3f(id)**three
C18
      ar4(id,2,4,4)=f1f(id)*(a(70)+a(71)*tdr2(id))
      ar4(id,4,2,4)=f1f(id)*(a(70)+a(71)*tdr1(id))
C19
      ar4(id,2,4,6)=a(72)*f3f(id)**three
      ar4(id,4,2,6)=a(72)*f3f(id)**three
C20
      ar4(id,3,3,0)=a(73)*f5f(id)
C21
      ar4(id,3,3,2)=a(74)*f3f(id)**third
C22
      ar4(id,3,3,6)=a(75)*f1f(id)+a(76)*f5f(id)
C24
      ar4(id,3,4,7)=f1f(id)*(a(77)+a(78)*tdr2(id))
     * + a(79)*f8f(id)
      ar4(id,4,3,7)=-f1f(id)*(a(77)+a(78)*tdr1(id))
     * - a(79)*f8f(id)
C25
      ar4(id,4,4,0)=a(83)*f5f(id)
C26
      ar4(id,4,4,2)=a(87)*f8f(id)
    
Cloop      ar4(id,ila(icnts,1),ila(icnts,2),ila(icnts,3))=
Cloop     *  ar4(id,ila(icnts,1),ila(icnts,2),ila(icnts,3))
Cloop     *  +a(70)*f4f(id)*(1+a(62)*tdr1(id)+a(65)*tdr2(id))
Cloop      ar4(id,ila(icnts,2),ila(icnts,1),ila(icnts,3))=
Cloop     *  ar4(id,ila(icnts,2),ila(icnts,1),ila(icnts,3))
Cloop     *  +a(70)*f4f(id)*(-1)**(ila(icnts,1)+ila(icnts,2))
Cloop     *  *(1+a(62)*tdr2(id)+a(65)*tdr1(id))
 4100 continue
      do 4102 j1j2=0,8
      do 4103 j1=max(0,j1j2-4),min(j1j2,4)
      j2=j1j2-j1
      do 4104 j3=abs(j2-j1),(j2+j1),2
      do 4101 i=1,n
      vp4(i,j1,j2,j3)=p1(i,j1,0)*p2(i,j2,0)
 4101 continue
      do 4105 m=1,min(j1,j2),1
      do 4201 i=1,n
      vp4(i,j1,j2,j3)=vp4(i,j1,j2,j3)+g(j1,j2,j3,m)*
     *            cs(i,m)*p1(i,j1,m)*p2(i,j2,m)
 4201 continue
 4105 continue
      do 4202 i=1,n
      e(i)=e(i)+vp4(i,j1,j2,j3)*ar4(i,j1,j2,j3)
 4202 continue
 4104 continue
 4103 continue
 4102 continue
      cofact=two/(nat-two)
      if(mod(ipotty,10).eq.4)then
         do 4119 k=1,n
            e(k)=e(k)
     *         +cofact*diss*(one-exp(-alpha*(r1(k)-r0)))**2
     *         +cofact*diss*(one-exp(-alpha*(r2(k)-r0)))**2
 4119    continue
      elseif(mod(ipotty,10).eq.5) then
          do 4118 k=1,n
            r1m(k)=r1(k)-rzero
            r2m(k)=r2(k)-rzero
            e(k)=e(k)
     *+cofact*(-diss_er*(1+er1*r1m(k)+er2*r1m(k)**2+er3*r1m(k)**3)
     *           *exp(-er1*r1m(k))+diss_er)
     *+cofact*(-diss_er*(1+er1*r2m(k)+er2*r2m(k)**2+er3*r2m(k)**3)
     *           *exp(-er1*r2m(k))+diss_er)
cb      do 4118 k=1,n
cb      expa=exp(-2*gpt1*r1(k))
cb      bexpa=expa*gpt2
cb      e(k)=e(k)+
cb     * cofact*(expa*(-gpt3/(1+bexpa)**2+gpt4/(1-bexpa)**2)+gpt5)
cb      expa=exp(-2*gpt1*r2(k))
cb      bexpa=expa*gpt2
cb      e(k)=e(k)+
cb     * cofact*(expa*(-gpt3/(1+bexpa)**2+gpt4/(1-bexpa)**2)+gpt5)
 4118 continue
      elseif(mod(ipotty,10).eq.6) then
        do 4117  k=1,n
          if (r1(k).le.1.316503) then
            e(k)=e(k)+1.190220082d7*exp(-4.374328*r1(k))-7453.55d0
          elseif (r1(k).ge.3.496584) then
            e(k)=e(k)+37269.0*(1-exp(-1.375429*(r1(k)-1.832393)))**2
          else
            call interpol(r1(k),epot)
            e(k)=e(k)+epot
          endif
          if (r2(k).le.1.316503) then
            e(k)=e(k)+1.190220082d7*exp(-4.374328*r2(k))-7453.55d0
          elseif (r2(k).ge.3.496584) then
            e(k)=e(k)+37269.0*(1-exp(-1.375429*(r2(k)-1.832393)))**2
          else
            call interpol(r2(k),epot)
            e(k)=e(k)+epot
          endif
 4117   continue
      endif
C check:
      do 3333 k=1,n
      if(e(k).lt.-20000.d0)then
        write(*,*) 'hole-alarm:',e(k),k
        stop
      endif
 3333 continue
      return
      end
c
      subroutine interpol(xt,yt)
      implicit none
      integer n,j
      parameter (n=23)
      double precision yp1,ypn,x(n),y(n),y2(n),xt,yt
      data x/1.316503,1.329330,1.344214,1.361439,1.381451,1.404885,
     *       1.432766,1.466770,1.509954,1.569148,1.668258,1.832393,
     *       2.040735,2.226567,2.375702,2.512454,2.644168,2.774601,
     *       2.906332,3.041625,3.182922,3.333225,3.496584/
      data y/30096.044,28046.743,25809.679,23394.990,20809.509,
     *       18057.808,15142.508,12064.731,8824.370,5420.329,
     *       1850.686,0.0,1850.686,5420.329,8824.370,12064.731,
     *       15142.508,18057.808,20809.509,23394.990,25809.679,
     *       28046.743,30096.044/
      data y2/
     * 702878.589744906870, 694322.764238971470,  624276.161812587380,
     * 595914.217180232400, 539594.368574006030, 504679.350082655100,
     * 456479.004570033250, 403801.967805763780, 347278.995511627490,
     * 280983.175894571760, 195932.646359160450, 106282.817678989440,
     * 46148.445916442193,  18468.966607999011,   5020.095140576329,
     * -2962.597996145571,  -8078.475397419184, -11280.753577874355,
     * -13697.511880608731, -13823.097865293526, -18576.674900124430,
     * -2977.273098522354, -57381.030785979509/
      data yp1/-164254.258d0/ypn/9339.1395d0/

      call splint(x,y,y2,n,xt,yt)
      return
      end

      subroutine splint(xa,ya,y2a,n,x,y)
      implicit none
      integer n
      double precision x,y,xa(n),ya(n),y2a(n)
      integer k,khi,klo
      double precision a,b,h
      klo=1
      khi=n
 1    if(khi-klo.gt.1)then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
        goto 1
      endif
      h=xa(khi)-xa(klo)
      if(h.eq.0.d0) write(*,*) 'bad xa input in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     * ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      return
      end

