      SUBROUTINE TAUTANG(N1,N2,N3,N4,Natm,XX,TANG)
************************************************************************
* The purpose of this routine is to calculate the tangent vector to    *
* the torsional motion of atoms N1-N2---N3-N4 in cartesian coordinates.*
* The input to this program is:                                        *
*      -> XX is a double array containing the cartesian coordinates of *
*         the molecule arranged as (atom#,XYZ#).                       *
*      -> Natm is the number of atoms in the molecule.                 *
*      -> N1 and N4 are the indices of the terminal atoms in the       *
*         dihedral coordinate.  N2 and N3 are the inner atoms.         *
*      -> The connectivity is N1--N2--N3--N4.                          *
* The output of the program is:                                        *
*      -> TANG is the tangent vector in cartesian coordinates of the   *
*         same dimension as XX.                                        *
* The torsion cartesian displacement formula was taken from the book   *
* MOLECULAR VIBRATIONS by Wilson, Decius, and Cross pg. 61.            *
************************************************************************
      IMPLICIT NONE 
      INTEGER N1, N2, N3, N4, Natm, i, j
      DIMENSION XX(1:Natm,1:3), TANG(1:Natm,1:3)
      DIMENSION UV12(1:3), UV23(1:3), UV43(1:3), UV32(1:3)
      DIMENSION CRP1223(1:3), CRP4332(1:3)
      DOUBLE PRECISION CRP1223, CRP4332
      DOUBLE PRECISION XX, TANG, Phi2, Phi3, UV12, UV23
      DOUBLE PRECISION UV43, UV32, R12, R23, DotProd1
      DOUBLE PRECISION DotProd2, R43, R32
      DOUBLE PRECISION Fac1, Fac2, Normy
* First, we define the bond lengths and start the unit vector
* calculations.
      R12 = 0.0D0
      R23 = 0.0D0
      R43 = 0.0D0
      DO i=1, 3
         UV12(i) = XX(N2,i)-XX(N1,i)
         UV23(i) = XX(N3,i)-XX(N2,i)
         UV43(i) = XX(N3,i)-XX(N4,i)
         R12 = R12 + ( XX(N2,i)-XX(N1,i) )**2
         R23 = R23 + ( XX(N3,i)-XX(N2,i) )**2
         R43 = R43 + ( XX(N3,i)-XX(N4,i) )**2
         END DO
      R12 = DSQRT( R12 )
      R23 = DSQRT( R23 )
      R43 = DSQRT( R43 )
      R32 = R23
* We determine the unit vectors in the bond direction 
* and the bond angles needed.
      DotProd1 = 0.0D0
      DotProd2 = 0.0D0
      DO i=1, 3
         UV12(i) = UV12(i) / R12
         UV23(i) = UV23(i) / R23
         UV43(i) = UV43(i) / R43
         UV32(i) = - UV23(i)
         DotProd1 = DotProd1 - UV12(i) * UV23(i)
         DotProd2 = DotProd2 - UV32(i) * UV43(i)
         END DO
      Phi2 = DACOS( DotProd1 )
      Phi3 = DACOS( DotProd2 )
* Detemining the necessary crossproducts of the unit vectors
* needed for the dihedral tangent.
      CALL CRSSPRD(UV12,UV23,CRP1223)
      CALL CRSSPRD(UV43,UV32,CRP4332)
      CALL CRSSPRD(UV43,UV32,CRP4332)
      CALL CRSSPRD(UV12,UV23,CRP1223)
* We now determine the torsion tangent.
* We initialize the tangent array.
      DO i=1, Natm
         DO j=1, 3
            TANG(i,j) = 0.0D0
            END DO
         END DO
* The first atom.
      Fac1 = - ( 1.0D0 / ( R12 * (DSIN(Phi2))**2 ) )
      DO i=1, 3
         TANG(N1,i) = Fac1 * CRP1223(i)
         END DO
* The last atom.
      Fac1 = - ( 1.0D0 / ( R43 * (DSIN(Phi3))**2 ) )
      DO i=1, 3
         TANG(N4,i) = Fac1 * CRP4332(i)
         END DO
* The second atom.
      Fac1 = ( (R23-R12*DCOS(Phi2))/(R23*R12*(DSIN(Phi2))**2) )
      Fac2 = ( DCOS(Phi3) / ( R23 * (DSIN(Phi3))**2 ) )
      DO i=1, 3
         TANG(N2,i) = Fac1 * CRP1223(i) + Fac2 * CRP4332(i)
         END DO
* The third atom.
      Fac1 = ( (R32-R43*DCOS(Phi3))/(R32*R43*(DSIN(Phi3))**2) )
      Fac2 = ( DCOS(Phi2) / ( R32 * (DSIN(Phi2))**2 ) )
      DO i=1, 3
         TANG(N3,i) = Fac1 * CRP4332(i) + Fac2 * CRP1223(i)
         END DO
* We finish by normalizing the tangent vector.
      Normy = 0.0D0
      DO i=1, Natm
         DO j=1, 3
            Normy = Normy + TANG(i,j)**2
            END DO
         END DO
      Normy = DSQRT(Normy)
      DO i=1, Natm
         DO j=1, 3
            TANG(i,j) = TANG(i,j) / Normy
            END DO
         END DO
      RETURN
      END
