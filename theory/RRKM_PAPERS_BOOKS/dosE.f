      PROGRAM DOSEHO
      IMPLICIT NONE
      INTEGER Nfreq, i, NEmax
      DOUBLE PRECISION Emax, DeltaE, cmi2har, kcal2har
      PARAMETER(kcal2har=1.0D0/627.5095D0)
      PARAMETER(cmi2har=1.0D0/219474.660D0)
      CHARACTER*80 Crap
      OPEN(UNIT=1,STATUS='OLD',FILE='DOSEHO.IN')
      DO i=1, 5
         READ(1,*) Crap
         END DO
      READ(1,*) Emax
      Emax = Emax * kcal2har       ! Change energy to atomic units.
      READ(1,*) Crap
      READ(1,*) Crap
      READ(1,*) DeltaE             ! Read energy resolution for counting.
      DeltaE = DeltaE * cmi2har    ! Change energy to atomic units.
      READ(1,*) Crap
      READ(1,*) Nfreq              ! Read number of frequencies.
      CLOSE(UNIT=1,STATUS='KEEP')
      NEmax = INT( Emax / DeltaE ) + 1  
      CALL COUNTER(Emax,NEMax,Nfreq,DeltaE)
      END
   
      SUBROUTINE COUNTER(Emax,NEmax,Nfreq,DeltaE)
      IMPLICIT NONE 
      INTEGER NEmax, Nfreq, i, j, k, NMAX, NRdof
      PARAMETER(NMAX=10000)
      DIMENSION NoS(1:NEmax), Freq(1:Nfreq), Rdof(1:NMAX)
      DIMENSION SoS(1:NEmax)
      INTEGER Rdof
      DOUBLE PRECISION Rtest, Rrem, NoS, En, kcal2har
      DOUBLE PRECISION Emax, DeltaE, SoS, Freq, cmi2har
      DOUBLE PRECISION Energy
      PARAMETER(cmi2har=1.0D0/219474.660D0)
      PARAMETER(kcal2har=1.0D0/627.5095D0)
      CHARACTER*80 Crap
* First we initialize the number of states array.
      DO i=1, NEmax
         SoS(i) = 0.0D0
         IF( i .EQ. 1 ) THEN
            NoS(i) = 1.0D0
         ELSE 
            NoS(i) = 0.0D0
            END IF
         END DO
* Now we read in the frequencies.
      OPEN(UNIT=1,STATUS='OLD',FILE='DOSEHO.IN')
      DO i=1, 12
         READ(1,*) Crap
         END DO
      DO i=1, Nfreq
         READ(1,*) Freq(i) 
         Freq(i) = Freq(i) * cmi2har   ! Convert HO to hartrees.
         END DO
      CLOSE(UNIT=1,STATUS='KEEP')
* Now we may determine the number of states by folding in 
* one vibrational degree of freedom at a time.
      DO i=1, Nfreq
* First we prepare the degree of freedom for the count.
         NRdof = INT( Emax / Freq(i) )     ! Determine max quanta needed.
         DO j=1, NRdof
            Rtest = ( DBLE(j) * Freq(i) ) / DeltaE
            Rrem = Rtest - DBLE(INT(Rtest))
            IF ( Rrem .GE. 0.50D0 ) THEN   ! Determine number of quanta in
               Rdof(j) = INT( Rtest ) + 1  ! corresponding to element of
            ELSE                           ! number of states array.
               Rdof(j) = INT( Rtest ) 
               END IF
            END DO
* Now we use the Beyer-Swineheart count to fold in the degree of freedom.
         CALL BSWIN(NEmax,NRdof,NoS,Rdof)
         END DO
* Now we find the sum of states.
      DO i=1, NEmax
         DO j=1, i
            SoS(i) = SoS(i) + NoS(j)
            END DO
         END DO
* Last, we print our results.
998   FORMAT('E(kcal/mol)',3X,'DoS(wavnum-1)',6X,'Sum of States')
997   FORMAT('-----------',3X,'-------------',6X,'-------------')
999   FORMAT(F10.5,4X,G15.9,4X,G15.9)
      OPEN(UNIT=1,STATUS='NEW',FILE='STATES.OUT')
      WRITE(1,998)
      WRITE(1,997)
      DO i=1, NEmax
         Energy = DBLE(i-1) * DeltaE / kcal2har
         WRITE(1,999) Energy, NoS(i)/(DeltaE/cmi2har), SoS(i)
         END DO
      CLOSE(UNIT=1,STATUS='KEEP')
      RETURN
      END

      SUBROUTINE BSWIN(NMax,Ndof,DGen,Rdof)
      IMPLICIT NONE
      INTEGER NMax, i, j, k, Ndof
      DIMENSION DGen(1:NMax), DGTemp(1:NMax), Rdof(1:Ndof)
      INTEGER Rdof
      DOUBLE PRECISION DGen, DGTemp
* We initialize the temporary degeneracy array.
      DO i=1, NMax
         DGTemp(i) = DGen(i) 
         END DO             
* Now we fold in the states of the degrees of freedom into
* the temporary degeneracy array.
      DO k=1, Ndof
         DO i=1, Nmax-Rdof(k)
            DGTemp(Rdof(k)+i) = DGTemp(Rdof(k)+i) + DGen(i)
            END DO
         END DO
* The states for the degree of freedom are now folded in. We
* transfer the results to the permanent array.
      DO k=1, NMax
         DGen(k) = DGTemp(k)
         END DO
      RETURN
      END

