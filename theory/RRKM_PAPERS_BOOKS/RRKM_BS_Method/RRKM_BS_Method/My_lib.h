/************************************************************************/
/* */
/************************************************************************/
#ifndef My_Lib_H_
#define My_Lib_H_
#include <iostream>
#include <cmath>

double uniform(double start, double end);
double gauss(double mean,double sigma,double min,double max);
double gauss();
double my_round(double);
#endif
