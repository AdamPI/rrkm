#ifndef My_Lib_CPP_
#define My_Lib_CPP_
#include <iostream>
#include <cmath>
#include <limits>
#include <cstdlib>
#include <cstring>


double uniform(double start, double end)//
{
	return start+(end-start)*rand()/(RAND_MAX + 1.0);
}
double gauss(double mean,double sigma,double min,double max)//
{
	double v,r1,r2,random;
	do{
		r1=uniform(0,1);
		r2=uniform(0,1);
		v=sqrt((-2*log(r1)))*cos(6.2832*r2);
		random=v*sigma + mean;
	}while(random<min || random >max);
	return random;
}
double gauss(){
	return gauss(0.0,1.0,-std::numeric_limits <double> ::max(),std::numeric_limits <double> ::max());
}

double my_round(double d){
	return floor(d+0.5);
}
#endif
