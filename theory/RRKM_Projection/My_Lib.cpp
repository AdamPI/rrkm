#ifndef My_Lib_CPP_
#define My_Lib_CPP_
#include <iostream>
#include <cmath>
#include <limits>
#include <cstdlib>
#include <cstring>
#include "itpp/itcomm.h"
#include <vector>


double uniform(double start, double end)//
{
	return start+(end-start)*rand()/(RAND_MAX + 1.0);
}
double gauss(double mean,double sigma,double min,double max)//
{
	double v,r1,r2,random;
	do{
		r1=uniform(0,1);
		r2=uniform(0,1);
		v=sqrt((-2*log(r1)))*cos(6.2832*r2);
		random=v*sigma + mean;
	}while(random<min || random >max);
	return random;
}
double gauss(){
	return gauss(0.0,1.0,-std::numeric_limits <double> ::max(),std::numeric_limits <double> ::max());
}

double my_round(double d){
	return floor(d+0.5);
}

int levi_civita_symbol(int i, int j, int k){
	if ((i==1&&j==2&&k==3)||(i==2&&j==3&&k==1)||(i==3&&j==1&&k==2))
	{
		return 1;
	}
	else if ((i==1&&j==3&&k==2)||(i==3&&j==2&&k==1)||(i==2&&j==1&&k==3))
	{
		return -1;
	}
	else 
		return 0;	
}

double dot_product(itpp::Vec<double> a, itpp::Vec<double> b){
	if (a.size()!=b.size())
	{
		return 0.0;
	} 
	else
	{
		double temp=0.0;
		for (int i=0; i<a.size(); ++i)
		{
			temp+=a(i)*b(i);
		}
		return temp;
	}
	
}

void cross_product(itpp::Vec<double> a, itpp::Vec<double> b, itpp::Vec<double> &c){
	if ((a.size()==b.size())&&(b.size()==c.size()))
	{
		c(0)=-a(2)*b(1) + a(1)*b(2);
		c(1)=a(2)*b(0) - a(0)*b(2);
		c(2)=-a(1)*b(0) + a(0)*b(1);
	}
	else
		return;
	
}

void mom_inertia(itpp::Vec<double> mass, itpp::Mat<double> &XX, itpp::Vec<double> &inertia, int flagCOM=1, int flagROT=1){//default parameter
	// Put the H2O2 molecule in its enter of mass frame, if desired.
	itpp::Vec<double> COM(3);
	if (flagCOM==1)
	{
		double mass_tot=0;
		for (int i=0; i<mass.size(); ++i)
		{
			mass_tot+=mass(i);
		}
		for (int i=0; i<3; ++i)
		{
			COM(i)=0;
			for(int j=0; j<mass.size(); ++j){
				COM(i)+=(mass(j)/mass_tot)*XX(j,i);
			}
			for (int j=0; j<mass.size(); ++j)
			{
				XX(j,i)=XX(j,i)-COM(i);
			}
		}
	}

	/* We construct the inertial matrix taking advantage of symmetry.*/
	itpp::Mat<double> I(3,3);
	I.zeros();
	for (int i=0; i<mass.size(); ++i)
	{
		I(0,0)+=mass(i)*(pow(XX(i,1),2)+pow(XX(i,2),2));
		I(0,1)+=(-mass(i)*XX(i,0)*XX(i,1));
		I(0,2)+=(-mass(i)*XX(i,0)*XX(i,2));
		I(1,1)+=mass(i)*(pow(XX(i,0),2)+pow(XX(i,2),2));
		I(1,2)+=(-mass(i)*XX(i,1)*XX(i,2));
		I(2,2)+=mass(i)*(pow(XX(i,0),2)+pow(XX(i,1),2));
	}
	I(1,0)=I(0,1);
	I(2,0)=I(0,2);
	I(2,1)=I(1,2);
	itpp::Mat<double> eigen_vector(I.rows(), I.cols());
	itpp::eig_sym(I, inertia, eigen_vector);
	//std::cout<<eigen_vector.transpose()*I*eigen_vector<<std::endl;
	/* Now we rotate the geometry into the principle axis frame, if
	* we set flagROT=1.*/
	if (flagROT==1)
	{
		itpp::Mat<double>XX_new(XX.rows(), XX.cols());
		for (int i=0; i<XX_new.rows(); ++i)
		{
			XX_new.set_row(i,eigen_vector*XX.get_row(i));
		}
		XX=XX_new;
	}
}

void hess_mw(itpp::Vec<double> mass, std::vector<std::vector<std::vector<std::vector<double> > > > &hess){
	//itpp::Vec<double> mass(4);
	//mass(0)=1837.1526509653975; mass(1)=1837.1526509653975; mass(2)=29156.94570303613; mass(3)=29156.94570303613;
	for (int i=0; i<hess.size(); ++i)
	{
		for (int j=0; j<hess[i].size(); ++j)
		{
			for (int k=0; k<hess[i][j].size(); ++k)
			{
				//std::cout<<hess[i][j].size();
				for (int l=0; l<hess[i][j][k].size(); ++l)
				{
					//std::cout<<hess[i][j][k].size();
					hess[i][j][k][l]=hess[i][j][k][l]/(sqrt(mass(i)*mass(k)));
				}
				
			}
			
		}
		
	}
	
}

void x_mw(itpp::Vec<double> mass, itpp::Mat<double> xx, itpp::Mat<double> &xx_mw){
	//itpp::Vec<double> mass(4);
	//mass(0)=1837.1526509653975; mass(1)=1837.1526509653975; mass(2)=29156.94570303613; mass(3)=29156.94570303613;
	for (int i=0; i<xx.rows(); ++i)
	{
		for (int j=0; j<xx.cols(); ++j)
		{
			xx_mw(i,j)=xx(i,j)*sqrt(mass(i));
		}
		
	}
}


void tau_tangent(int n1, int n2, int n3, int n4, itpp::Mat<double> XX, itpp::Mat<double> &tang){
	int N_atm=XX.rows();
	double R12=0.0, R23=0.0, R32=0.0, R43=0.0;
	itpp::Vec<double> UV12(3), UV23(3), UV32(3), UV43(3);
	for (int i=0; i<UV12.size(); ++i)
	{
		UV12(i) = XX(n2-1,i)-XX(n1-1,i);//C and C++ start from 0
		UV23(i) = XX(n3-1,i)-XX(n2-1,i);
		UV43(i) = XX(n3-1,i)-XX(n4-1,i);
		R12 = R12+pow((XX(n2-1,i)-XX(n1-1,i)),2);
		R23 = R23+pow((XX(n3-1,i)-XX(n2-1,i)),2);
		R43 = R43+pow((XX(n3-1,i)-XX(n4-1,i)),2);
	}
	R12=sqrt(R12);
	R23=sqrt(R23);
	R43=sqrt(R43);
	R32=R23;

	double dot_product_1=0.0, dot_product_2=0.0;
	for (int i=0; i<UV12.size(); ++i)
	{
		UV12(i)=UV12(i)/R12;
		UV23(i)=UV23(i)/R23;
		UV43(i)=UV43(i)/R43;
		UV32(i)=-UV23(i);
		dot_product_1 = dot_product_1 - UV12(i) * UV23(i);
		dot_product_2 = dot_product_2 - UV32(i) * UV43(i);
	}
	double phi2=acos(dot_product_1);
	double phi3=acos(dot_product_2);
	// Determining the necessary crossproducts of the unit vectors
	// needed for the dihedral tangent.
	itpp::Vec<double> CRP1223(3); cross_product(UV12, UV23, CRP1223);
	itpp::Vec<double> CRP4332(3); cross_product(UV43, UV32, CRP4332);
	//We now determine the torsion tangent.
	//We initialize the tangent array.
	tang.zeros();
	//The first atom
	double fac1=-1.0/(R12*pow(sin(phi2),2));
	for (int i=0; i<tang.cols(); ++i)
	{
		tang(n1-1,i)=fac1*CRP1223(i);
	}
	//The last atom
	fac1=-1.0/(R43*pow(sin(phi3),2));
	for (int i=0; i<tang.cols(); ++i)
	{
		tang(n4-1 ,i)=fac1*CRP4332(i);
	}
	//The second atom
	fac1=(R23-R12*cos(phi2))/(R23*R12*(pow(sin(phi2),2)));
	double fac2=cos(phi3)/(R23*pow(sin(phi3),2));
	for (int i=0; i<tang.cols(); ++i)
	{
		tang(n2-1, i)=fac1*CRP1223(i)+fac2*CRP4332(i);
	}
	//The third atom
	fac1=(R32- R43*cos(phi3))/(R32*R43*pow(sin(phi3),2));
	fac2=cos(phi2)/(R32*(pow(phi2,2)));
	for (int i=0; i<tang.cols(); ++i)
	{
		tang(n3-1,i)=fac1*CRP4332(i)+fac2*CRP1223(i);
	}
}

void tangent_mw(itpp::Vec<double> mass, itpp::Mat<double> &tang){
	int Natm=mass.size();
	for (int i=0; i<tang.rows(); ++i)
	{
		for (int j=0; j<tang.cols(); ++j)
		{
			tang(i,j)=tang(i,j)/sqrt(mass(i));
		}
		
	}
	//We finish by normalizing the tangent vector
	double normy=0.0;
	for (int i=0; i<tang.rows(); ++i)
	{
		for (int j=0; j<tang.cols(); ++j)
		{
			normy+=pow(tang(i,j),2);
		}		
	}
	//treat all 12 components as a vector
	normy=sqrt(normy);
	for (int i=0; i<tang.rows(); ++i)
	{
		for (int j=0; j<tang.cols(); ++j)
		{
			tang(i,j)=tang(i,j)/normy;
		}		
	}

}

void roo2t(itpp::Mat<double> XX, itpp::Mat<double> &stretch){
	double roo=0.0;
	itpp::Vec<double> UV(3);
	for (int i=0; i<UV.length(); ++i)
	{
		UV(i)=XX(4-1,i)-XX(3-1,i);
		roo+=pow(UV(i),2);
	}
	roo=sqrt(roo);
	for (int i=0; i<UV.length(); ++i)
	{
		UV(i)=UV(i)/roo;
	}
	for (int i=0; i<stretch.rows(); ++i)
	{
		for (int j=0; j<stretch.cols(); ++j)
		{
			stretch(i,j)=0.0;
		}

	}
	for (int i=0; i<stretch.cols(); ++i)
	{
		stretch(3-1,i)=-UV(i);
		stretch(4-1,i)=UV(i);
	}

}
#endif
