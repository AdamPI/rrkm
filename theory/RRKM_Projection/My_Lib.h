/************************************************************************/
/* */
/************************************************************************/
#ifndef My_Lib_H_
#define My_Lib_H_
#include <iostream>
#include <cmath>
#include <vector>
#include "itpp/itcomm.h"


double uniform(double start, double end);
double gauss(double mean,double sigma,double min,double max);
double gauss();
double my_round(double);
int levi_civita_symbol(int, int, int);//levi civita symbol

//dot product, or I can just use a*b in itpp namespace.
double dot_product(itpp::Vec<double> a, itpp::Vec<double> b);

//cross_product in Cartesian coordinate, c=a cross b, or use outer_product(a,b) in itpp namespace
void cross_product(itpp::Vec<double> a, itpp::Vec<double> b, itpp::Vec<double> &c);

/*****************************************************************************
* The purpose of this routine is to compute the principle moments of   
* inertia, and, if desired, rotate the geometry into the frame of the  
* principle axes.                                                      
* The input for this routine is:                                       
*    -> XX contains the H2O2 geometry in an array (atom#,XYZ#).  If    
*      flagCOM is set the geometry is set relative to the center of    
*      mass.  If flagROT is set then it is set in the frame of the   
*      principle axes.                                                 
*    -> II are the principle moments of inertia, an array (1:3).       
*    -> flagCOM=1 will set the H2O2 geometry relative to the center    
*       of mass.                                                       
*    -> flagROT=1 will set the H2O2 geometry to the principle axes     
*       frame.                                                         
************************************************************************/
void mom_inertia(itpp::Vec<double> mass, itpp::Mat<double> &XX, itpp::Vec<double> &inertia, int flagCOM=1, int flagROT=1);

//mass weighting of hessian
/*****************************************************************************
* This routine takes the cartesian hessian matrix for H2O2 and converts
* it to the mass weighted hessian matrix.												
* The mass weighting is done in atomic units.										
*****************************************************************************/
void hess_mw(itpp::Vec<double> mass, std::vector<std::vector<std::vector<std::vector<double> > > > &hess);

/******************************************************************************
* This routine converts the cartesian coordinates of H2O2 to the mass	 
* weighted coordinates.  The cartesian coordinates are contained in the
* input array XX(atom#,XYZ#) and the mass weighted coordinates are   
* placed in the array XXmw(atom#,XYZ#).  The atom indexing is:           
*                          H1-O3----O4-H2														
* The mass weights are in atomic units.													 
******************************************************************************/
void x_mw(itpp::Vec<double> mass, itpp::Mat<double> xx, itpp::Mat<double> &xx_mw);

/********************************************************************************
* The purpose of this routine is to calculate the tangent vector to    
* the torsional motion of atoms N1-N2---N3-N4 in cartesian coordinates.
* The input to this program is:                                        
*      -> XX is a double array containing the cartesian coordinates of 
*         the molecule arranged as (atom#,XYZ#).                       
*      -> Natm is the number of atoms in the molecule.                 
*      -> N1 and N4 are the indices of the terminal atoms in the       
*         dihedral coordinate.  N2 and N3 are the inner atoms.         
*      -> The connectivity is N1--N2--N3--N4.                          
* The output of the program is:                                        
*      -> TANG is the tangent vector in cartesian coordinates of the   
*         same dimension as XX.                                        
* The torsion cartesian displacement formula was taken from the book
* MOLECULAR VIBRATIONS by Wilson, Decius, and Cross pg. 61.            
**********************************************************************************/
void tau_tangent(int n1, int n2, int n3, int n4, itpp::Mat<double> XX, itpp::Mat<double> &tang);

/****************************************************************************
* The purpose of this routine is to mass-weight a cartesian            
* displace tangent vector (s_ta in the notation of Wilson, Decius, and
* Cross).                                                              
* The input for the routine is:                                        
*         -> Natm is the number of atoms in the molecule.              
*         -> mass is an array containing each of the masses of the     
*            atoms (a 1D array of size Natm).                          
*         -> TANG is a double array containing the cartesian tangent   
*            vector upon input and the mass-weighted cartesian tangent 
*            vector upon output.  It is organized as (atom#,XYZ#).     
****************************************************************************/
void tangent_mw(itpp::Vec<double> mass, itpp::Mat<double> &tang);

//calculate the stretch stuff
void roo2t(itpp::Mat<double> XX, itpp::Mat<double> &stretch);

#endif
