﻿#ifndef MAIN_CPP_
#define MAIN_CPP_
#include <iostream>
#include "My_Class.h"
#include "My_Lib.h"
#include "cstdio"
#include "cstring"
#include <fstream>
#include <vector>

#include "stdafx.h"
#include "itpp/itcomm.h"

#define flagROO 1
#define flagTAU 0
#define flagROT 0 //transform the coordinates into COM or not
#define flagCOM 1 //Rotate the geometry into principle axis frame or not
#define MAX_MODE_NUM 8
#define bohr2angstrom 0.529177
#define hartree2cm 219474.631370515

using namespace itpp;
using namespace std;

/******************************************************************************
* The purpose of this program is to project out the translation,       
* rotation, O--O stretch (optional), and torsion (optional) degrees of 
* freedom from the Hessian according to the scheme of Miller, Handy,   
* and Adams, for H2O2.                                                 
*                             H1-O3----O4-H2                           
* ALL INPUTS SHOULD BE IN ATOMIC UNITS!!!!                             
* The input into this program is:                                      
*     -> XX is an array containing the cartesian geometry of the       
*        molecule arranged as (atom#,XYZ#) with atom labels given      
*        above.                                                        
*     -> HESS is the hessian matrix organized in a corresponding       
*        pattern to XX, (atom#,XYZ#,atom#,XYZ#).  This is the CARTESIAN
*        hessian matrix (no mass weighting on input).                  
*     -> Inert is an array containing the principle moments of inertia.
*     -> If flagROO = 1 the O---O stretch is projected out of the      
*        hessian prior to diagonalization.                            
*     -> If flagTAU = 1 the torsional mode is projected out prior to   
*        diagonalization.                                              
* THe output from this program is:                                     
*     -> An array (of length 12) FrqVal containing the N projected     
*        vibrational frequencies in cm**-1.                            
****************************************************************************/

/*main function*/
int main(int argc, char** argv)
{	
	//define the mass
	itpp::Vec<double> mass(4);
	mass(0)=1837.1526509653975; mass(1)=1837.1526509653975; mass(2)=29156.94570303613; mass(3)=29156.94570303613;
	int Natm=mass.size();
	//Read the XX matrix first.
	ifstream inf_XX("geo.txt");
	//ifstream inf_XX("XX.dat");
	itpp::Mat<double>XX(Natm,3);
	itpp::Mat<double>XX_mw(Natm,3);
	vector<vector<vector<vector<double> > > > hess_4D(Natm, vector<vector<vector<double> > >(3, vector<vector<double> >(Natm, vector<double>(3))));
	for (int i=0; i<XX.rows(); ++i)
	{
		for (int j=0; j<XX.cols(); ++j)
		{
			inf_XX>>XX(i,j);
		}
	}
	//Read the Hess matrix
	ifstream inf_hess("hessian.txt");
	//ifstream inf_hess("hess.dat");
	for (int i=0; i<hess_4D.size(); ++i)
	{
		for (int j=0; j<hess_4D[0].size(); ++j)
		{
			for (int k=0; k<hess_4D[0][0].size(); ++k)
			{
				for (int l=0; l<hess_4D[0][0][0].size(); ++l)
				{
					inf_hess>>hess_4D[i][j][k][l];
					//cout<<hess_4D[i][j][k][l];
				}
			}
			//cout<<endl;
		}
	}
	//cout<<XX;
	inf_hess.close();
	inf_XX.close();
	//cout<<hess_4D[0][0][0][0]<<endl;
	/**************************************************************************************************************
	*====================MASS WEIGHTING & PRELIMINARIES====================*
	***************************************************************************************************************/
	/*Get the inertia, transform the coordinates into COM and rotate the geometry into principle axis frame,
	*if we set flagCOM==1 and flagROT==1*/
	itpp::Vec<double> inertia(3);
	mom_inertia(mass, XX, inertia,flagCOM,flagROT);
	/* We start by mass weighting the input Hessian and Cartesian.*/
	hess_mw(mass, hess_4D);
	x_mw(mass, XX, XX_mw);

	/* We begin by determining the total mass of the system.*/
	double m_tot=0.0;
	for (int i=0; i<Natm; ++i)
	{
		m_tot+=mass(i);
	}
	/* Now the coordinates have been redefined (and re-mass weighted).
	* We now create the projector.*/
	/*****************************************************************************************************************
	*=======================3 TRANSLATION MODE VECTORS=====================*
	******************************************************************************************************************
	* First, we make the translation mode vector.*/
	vector<vector<vector<double> > > L(Natm, vector<vector<double> >(3, vector<double>(MAX_MODE_NUM)));
	for (int i=0; i<Natm; ++i)
	{
		for (int j=0; j<3; ++j)
		{
			for (int k=0; k<3; ++k)
			{//translation, 3 degree of freedom
				if (j==k)
				{
					L[i][j][k]=sqrt(mass(i)/m_tot);
				}
				//cout<<L[i][j][k]<<" ";
			}
			//cout<<endl;
		}
		//cout<<endl<<endl;
	}

	/*****************************************************************************************************************
	*=====================3 ROTATIONAL MODE VECTORS========================*
	******************************************************************************************************************
	* Second, we make the rotational modes.*/
	for (int i=0; i<Natm; ++i)
	{
		for (int j=0; j<3; ++j)
		{
			for (int k=(4-1); k<=(6-1); ++k)
			{//rotation, 3 degree of freedom
				L[i][j][k]=0.0;
				for (int l=0; l<3; ++l)
				{//read the paper
					L[i][j][k]+=levi_civita_symbol(k-3+1,l+1,j+1)*XX_mw(i,l)/sqrt(inertia(k-3));//index of j,k,l start from 0
				}
				//cout<<L[i][j][k]<<" ";	
			}
			//cout<<endl;
		}
		//cout<<endl<<endl;
	}


	/**********************************************************************************************************************
		*====================CREATING TANGENT MODES TO==========================
		*================DIHEDRAL AND FRAGMENT STRETCH FOR========================
		*===============================H2O2=================================
		********************************************************************************************************************
		* We use the flags in order to determine which modes to project out.
		* If flagROO is set to 1, we project out the H-O~~~~O-H stretch.
		* If flagTAU is set to 1, we project out the torsion angle.*/
	int mode_num=0;
	int index_tau=0;
	int index_roo=0;
	if ((flagROO!=1)&&(flagTAU!=1))
	{
		mode_num=6;
	} 
	else if ((flagTAU==1)&&(flagROO!=1))
	{
		mode_num=7;
		index_tau=7-1;//index start from 0
	}
	else if ((flagROO==1)&&(flagTAU!=1))
	{
		mode_num=7;
		index_roo=7-1;//index start from 0
	}
	else if ((flagTAU==1)&&(flagROO==1))
	{
		mode_num=8;
		index_tau=7-1;//index start from 0
		index_roo=8-1;//index start from 0
	}
	// Now, if desired we project out the dihedral coordinate.
	itpp::Mat<double> tang(Natm, 3);
	if (flagTAU==1)
	{
		//Create Torsion tangent.
		tau_tangent(1,3,4,2,XX,tang);
		//Convert to MW'ed coordinates.
		tangent_mw(mass, tang);
		//Now we place this in our mode vector
		for (int i=0; i<Natm; ++i)
		{
			for (int j=0; j<3; ++j)
			{
				L[i][j][index_tau]=tang(i,j);
			}
		}
	}
	//Now, if desired, we project out the (HO)---(OH) stretch.
	itpp::Mat<double> stretch(Natm, 3);
	if (flagROO==1)
	{
		//Create fragment stretch tangent.
		roo2t(XX, stretch);
		//Convert to MW'ed coordinates
		tangent_mw(mass, stretch);
		//Now we place this in our mode vector
		for (int i=0; i<Natm; ++i)
		{
			for (int j=0; j<3; ++j)
			{
				L[i][j][index_roo]=stretch(i,j);
			}
		}
		//cout<<stretch;
	}
	/****************************************************************************************************************
	*================CONSTRUCTION OF PROJECTOR MATRIX======================*
	****************************************************************************************************************/
	//We can now construct the projector matrix.
	vector<vector<vector<vector<double> > > > Pmat_4D(Natm, vector<vector<vector<double> > >(3, vector<vector<double> >(Natm, vector<double>(3))));
	for (int i=0; i<Natm; ++i)
	{
		for (int j=0; j<3; ++j)
		{
			for (int k=0; k<Natm; ++k)
			{
				for (int l=0; l<3; ++l)
				{
					Pmat_4D[i][j][k][l]=0.0;
					for (int m=0; m<mode_num; ++m)
					{
						Pmat_4D[i][j][k][l]+=L[i][j][m]*L[k][l][m];
					}
				}
			}
		}
	}
	/* We now subtract the projector matrix from the identity matrix.
	* This removes the undesired modes from the coordinate description of
	* the system (i.e. set of coordinates without the translation, rotation, or
	* gradient).*/
	itpp::Mat<double> Qmat(3*Natm, 3*Natm);
	itpp::Mat<double> hess_2D(3*Natm, 3*Natm);
	//cout<<Qmat;
	int index_temp=0, row_index=0, col_index=0;
	for (int i=0; i<Natm; ++i)
	{
		for (int j=0; j<3; ++j)
		{
			for (int k=0; k<Natm; ++k)
			{
				for (int l=0; l<3; ++l)
				{
					row_index=index_temp/(Natm*3);
					col_index=index_temp%(Natm*3);
					//Qmat(row_index, col_index)=0.0;
					if (row_index==col_index)
					{//creat Q_matrix=1-P
						Qmat(row_index, col_index)=1.0-Pmat_4D[i][j][k][l];
						//Qmat(row_index, col_index)=1.0-Pmat_4D[k][l][i][j];
					} 
					else
					{
						Qmat(row_index, col_index)=-Pmat_4D[i][j][k][l];
						//Qmat(row_index, col_index)=-Pmat_4D[k][l][i][j];
					}
					//create 2-dimensional hess matrix
					hess_2D(row_index, col_index)=hess_4D[i][j][k][l];
					index_temp++;
				}
			}
		}
	}

	//Creat the projected_hess
	//K_p=(1-P)*K*(1-P)
	itpp::Mat<double> K_P(3*Natm, 3*Natm);
	K_P=Qmat*hess_2D*Qmat;

	//Diagonalize

	itpp::cmat eigen_vector(3*Natm, 3*Natm);
	itpp::cvec eigen_value(eigen_vector.rows());
	itpp::eig(K_P, eigen_value, eigen_vector);
	itpp::Vec<double> eigen_value_real_part=sqrt(abs(itpp::real(eigen_value)))*hartree2cm;
	cout<<eigen_value_real_part;
	//itpp::Mat<double> eigen_vector(3*Natm, 3*Natm);
	//itpp::Vec<double> eigen_value(3*Natm);
	//itpp::eig_sym(K_P,eigen_value, eigen_vector);
	//eigen_value=sqrt(abs(eigen_value))*hartree2cm;
	//cout<<eigen_value;

	return 0;
}

#endif
