﻿#ifndef MAIN_CPP_
#define MAIN_CPP_
#include <iostream>
#include "My_Class.h"
#include "My_Lib.h"
#include "cstdio"
#include "cstring"
#include <fstream>
#include <vector>
#include <algorithm>


//1): Choose a suitable grain size, Delta E 
//Consider E_max= 101kcal/mol= 34975.7/cm=
//M*Delta_E=E_max
#define E_max 35325.5
#define Delta_E 1.00 //in unit of 1/cm
#define M ((int)(E_max/Delta_E+1))
#define CM2J (1.98630*10e-23)
#define PLANCK_CONST (6.626068*10e-34)
#define PRECISION 10


using namespace std;

/* 主程序 */
int main(int argc, char** argv)
{	
	cout<<my_round(1.5)<<endl;
	ifstream fin(argv[1]);
	ofstream fout(argv[2]);

	vector<double> freq;
	freq.push_back(0);
	double in_freq;

	//read in the frequencies
	while(!fin.eof()){
		fin>>in_freq;
		freq.push_back(in_freq);
	}

	//2): Initialize two arrays, T and AT, both of length M.
	vector<int> T(M), AT(M);
	T[1]=1; AT[1]=1;

	//3): For each degree of freedom, j, carry out the following.
	//Enumerate all energy levels up to E_max; call the kth level E_j_k

	//Reduce each E_j_k to an integer, R_j_k, such that R_j_k* Delta_E= E_j_k
	vector< vector<int>  > R(freq.size(), vector<int>(1) );
	for (int i=1; i<R.size(); ++i)
	{
		//Determine the max quanta needed.
		int quanta=(int)(E_max/freq[i])+1; //ignore the first index 0
		for (int j=1; j<=quanta; ++j)
		{
			//Reduce each E_j_k to an integer, R_j_k
			R[i].push_back(round(j*freq[i])/Delta_E);
		}
		//Use the Beyer-Swineheart count to fold in the degree of freedom
		for (int k=1; k<R[i].size(); ++k)
		{
			for (int m=R[i][k]+1; m<M; ++m)
			{
				AT[m]=AT[m]+T[m-R[i][k]];
			}
		}
		//For I=1 to I=M, T(I)= AT(I)
		for (int n=1; n<M; ++n)
		{
			T[n]=AT[n];
		}
	}

	//4): P(E_m) is contained in T(R_m+1), to get Sum(P(E_m)) , set T(I)=T(I)+T(I-1), for I=2 and I=M
	fout.setf(ios::fixed, ios::floatfield); //	floatfield set to fixed
	fout.precision(PRECISION);

	fout<<"array #"<<" "<<"Energy(1/cm)"<<" "<<"Energy(kcal/mol)"<<" "<<"P(E)"<<" "<<"N(E)"<<" "<<"G(E)"<<" "<<"K(1/s)"<<endl;
	for (int i=1; i<T.size(); ++i)
	{
		int temp=0;
		fout<<i;
		if (i>=2)
		{
			for (int j=1; j<i; ++j)
			{
				temp+=T[j];
			}
		}
		fout<<" "<<i*Delta_E<<" "<<i*Delta_E*0.00285911<<" "<<T[i]<<" "<<T[i]/Delta_E<<" "
			<<temp<<" "<<1.0*Delta_E*temp/T[i]*CM2J/PLANCK_CONST<<endl;
	}

	fin.close();
	fout.close();
	return 0;
}

#endif
