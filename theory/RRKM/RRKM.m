Reactant=load('reactant_level_sum.dat');
Ts=load('ts_level_sum.dat');
E_barrier=15662;%in unit of 1/cm
CM2J= (1.98630*10e-23);
PLANCK_CONST= (6.626068*10e-34);
CM2KCALMOL= 0.00285911;

NE=Reactant(:,5);
GE=Ts(:,6);
RGE=NE;
E=NE;
k=NE;
for i=1:length(NE)
    E(i)=i*CM2KCALMOL;
    if(i<=E_barrier)
        RGE(i)=0;
    else
        RGE(i)=GE(i-E_barrier);
    end 
    k(i)=log10(RGE(i)/NE(i)*CM2J/PLANCK_CONST);
end
% scatter(E,NE);
scatter(E,k);
title('Reaction~rate~of~Performic~acid','interpreter','latex','fontsize',18);
xlabel('Energy/kcal$\cdot mol^{-1}$','interpreter','latex','fontsize',18);
ylabel('Rate/$s^{-1}$','interpreter','latex','fontsize',18);
saveas(gca,'RRKM.eps','psc2');
