﻿#ifndef MAIN_CPP_
#define MAIN_CPP_
#include <iostream>
#include "My_Class.h"
#include "My_Lib.h"
#include "cstdio"
#include "cstring"
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>


#define PRECISION 10
//1): Choose a suitable grain size, Delta E 
//Consider E_max= 100kcal/mol= 34975.7/cm
//M*Delta_E=E_max
#define Delta_E 1.00 //in unit of 1/cm
#define M 40000
#define CM2J (1.98630*10e-23)
#define PLANCK_CONST (6.626068*10e-34)


using namespace std;

/*main function */
int main(int argc, char** argv)
{	
	ifstream fin(argv[1]);
	ofstream fout(argv[2]);

	vector<double> freq;
	freq.push_back(0);
	double in_freq;

	while(!fin.eof()){
		fin>>in_freq;
		freq.push_back(in_freq);
	}
	
	vector<int> R;
	R.push_back(0);
	
	//2): Reduce each fundamental frequency, v_i, to an integer, R_i, such that R_i* Delta_E= v_i
	for (int i=1; i<freq.size(); ++i)
	{
		R.push_back(my_round(freq[i]/Delta_E));
	}
	
	//3): Initialize an array
	vector<double> T(M);
	T[1]=1;

	//4):
	for (int i=1; i<freq.size(); ++i)
	{
		for (int j=R[i]+1; j<T.size(); ++j)
		{
			T[j]=T[j-R[i]]+T[j];
		}
	}
	fout.setf(ios::fixed, ios::floatfield); //	floatfield set to fixed
	fout.precision(PRECISION);

	fout<<"array #"<<" "<<"Energy(1/cm)"<<" "<<"Energy(kcal/mol)"<<" "<<"P(E)"<<" "<<"N(E)"<<" "<<"G(E)"<<" "<<"K(1/s)"<<endl;
	int temp=0;
	for (int i=1; i<T.size(); ++i)
	{
		temp=0;
		fout<<i;
		if (i>=2)
		{
			for (int j=1; j<i; ++j)
			{
				temp+=T[j];
			}
		}
		fout<<" "<<i*Delta_E<<" "<<i*Delta_E*0.00285911<<" "<<T[i]<<" "<<T[i]/Delta_E<<" "
			<<temp<<" "<<1.0*Delta_E*temp/T[i]*CM2J/PLANCK_CONST<<endl;
	}

	//for (int i=2; i<T.size(); ++i)
	//{
	//	T[i]=T[i]+T[i-1];
	//}

	//vector<int> TT(10);
	//for (int i=0; i<10; ++i)
	//{
	//	TT[i]=my_round((i+1)*10*349.757);
	//	cout<<T[TT[i]]<<endl;
	//}

	fin.close();
	fout.close();
	return 0;
}

#endif
